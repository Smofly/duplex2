<?php

// Switch menu admin
$page = '';
if (isset($_GET['page'])) {
    $page = $_GET['page'];
}
switch ($page) {
    // Users 
    case "update_users":
        $pages = "modules/users/update_users.php";
        break;
    case "admin_users":
        $pages = "modules/users/users_index.php";
        break;
    case "create_users":
        $pages = "modules/users/create_users.php";
        break;
    case "admin_news":
        $pages = "modules/news/news_index.php";
        break;
    // Nyhder nedenfor
    case "admin_create_news":
        $pages = "modules/news/create_new.php";
        break;
    case "update_news":
        $pages = "modules/news/update_news.php";
        break;
    case "delete_news":
        $pages = "modules/news/delete_news.php";
        break;
    // Produkter m. advanced søg
    case "admin_create_moebel":
        $pages = "modules/products/create_moebel.php";
        break;
    case "admin_list_moebel":
        $pages = "modules/products/list_moebel.php";
        break;
    case "delete_moebel":
        $pages = "modules/products/delete_moebel.php";
        break;
    case "update_products":
        $pages = "modules/products/update_products.php";
        break;
    // Adresse
    case "admin_change_adress":
        $pages = "modules/adress/change_adress.php";
        break;
    // Forum
    case "admin_view_forum_main":
        $pages = "modules/forum/forum_main.php";
        break;
    case "admin_view_forum_sub":
        $pages = "modules/forum/forum_sub.php";
        break;
    case "delete_forum_main":
        $pages = "modules/forum/delete_main.php";
        break;
    case "delete_forum_sub":
        $pages = "modules/forum/delete_sub.php";
        break;
    case "admin_create_maincat":
        $pages = "modules/forum/create_main.php";
        break;
    case "admin_create_subcat":
        $pages = "modules/forum/create_sub.php";
        break;
    // Gallery
    case "admin_list_gallery":
        $pages = "modules/gallery/gallery_index.php";
        break;
    case "admin_create_galcat":
        $pages = "modules/gallery/create_gallery_category.php";
        break;
    case "admin_upload_gallery":
        $pages = "modules/gallery/upload_gallery.php";
        break;
    case "admin_delete_galcat":
        $pages = "modules/gallery/delete_gallery_category.php";
        break;


    default:
}

// Switch menu front end
$frontend_page = '';
if (isset($_GET['frontend_page'])) {
    $frontend_page = $_GET['frontend_page'];
}
switch ($frontend_page) {
        // login og tilmeld
        case "front_login":
        $frontend_pages = "modules/login/login.php";
        break;
        case "front_signup":
        $frontend_pages = "modules/login/signup.php";
        break;
    // Nyheder
    case "front_news":
        $frontend_pages = "modules/news/show_news.php";
        break;
    case "front_news_one":
        $frontend_pages = "modules/news/one_news.php";
        break;
    // Profiler
    case "admin_profile":
        $frontend_pages = "modules/users/profile_page.php";
        break;
    // søg elementer
    case "front_search":
        $frontend_pages = "modules/search_advanced/search_products.php";
        break;
    // Forum
    case "front_forum":
        $frontend_pages = "modules/forum/show_forum.php";
        break;
    case "show_threads":
        $frontend_pages = "modules/forum/show_threads.php";
        break;
    case "front_forum_one":
        $frontend_pages = "modules/forum/show_single_thread.php";
        break;
    case "front_forum_create_thread":
        $frontend_pages = "modules/forum/create_thread.php";
        break;
    // Kontaktside
    case "front_contact":
        $frontend_pages = "modules/contact/show_contactpage.php";
        break;
    // Produkter m. advanced search
    case "front_one_product":
        $frontend_pages = "modules/products/one_product.php";
        break;
    // Produkter webshop
    case "front_webshop":
        $frontend_pages = "modules/shop/show_shop.php";
        break;
    case "front_allproducts_webshop":
        $frontend_pages = "modules/shop/show_basket.php";
        break;
    // Nyhedsbrev slet
    case "front_newsletter_delete":
        $frontend_pages = "modules/newsletter/newsletter_delete.php";
        break;
    // Galleri
    case "front_galleri":
        $frontend_pages = "modules/gallery/show_gallery.php";
        break;
    case "front_galleri_one":
        $frontend_pages = "modules/gallery/show_single_gallery.php";
        break;
    default:
        $frontend_pages = "home.php";
        break;
}