<?php
if(isset($_POST['logout'])){
    $logout = new Users('users');
    $logout->logout();
    $logout->redirect('index.php');
}
?>
<nav class="navbar navbar-inverse admin-nav" role="navigation">

    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">Duplex</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav navbar-right">
            <?php if(!in_array('User', $users_rank->getRolesRank())){ ?>
                <li><a href="admin/index.php"><i class="icon-cog"></i> Administrator</a></li>
            <?php } ?>
            <li class="dropdown">

                <a href="javscript:;" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="icon-user"></i>
                    Bruger
                    <b class="caret"></b>
                </a>

                <ul class="dropdown-menu">
                    <li><a href="?frontend_page=admin_profile">Profil</a></li>
                    <li class="divider"></li>
                    <form action="" method="post"><button class="btn btn-danger btn-xs" type="submit" name="logout">Log ud</button></form>
                </ul>

            </li>
        </ul>
    </div><!-- /.navbar-collapse -->

</nav>