<?php

// Vis ALLE fejl
error_reporting(E_ALL);
ini_set('display_errors', '1');


//Vis IKKE fejl
//error_reporting(0);

/**
 * ALLE JS FILER!
 */
function include_js() {
    // ***** js online ******
    echo '<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>';
    echo '<script src="//frontend.reklamor.com/fancybox/jquery.fancybox.js"></script>';
    echo '<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>';
    // Validering online
    ?>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.1.47/jquery.form-validator.min.js"></script>
    <script>
        $.validate({
            modules: 'security',
            onModulesLoaded: function() {
                var optionalConfig = {
                    fontSize: '12pt',
                    padding: '4px',
                    bad: 'Very bad',
                    weak: 'Weak',
                    good: 'Good',
                    strong: 'Strong'
                };
                $('input[name="pass_confirmation"]').displayPasswordStrength(optionalConfig);
                $('#area').restrictLength($('#maxlength'));
            }
        });
    </script>
    <?php
    // ***** js lokalt *******
    echo '<script src="js/custom.js"></script>';
    // lightbox
    echo '<script src="js/lightbox.min.js"></script>';
    // Bootstrap lokalt
    //echo '<script src="bootstrap/js/affix.js"></script>';
    //echo '<script src="bootstrap/js/alert.js"></script>';
    //echo '<script src="bootstrap/js/button.js"></script>';
    //echo '<script src="bootstrap/js/carousel.js"></script>';
    //echo '<script src="bootstrap/js/collapse.js"></script>';
    //echo '<script src="bootstrap/js/dropdown.js"></script>';
    //echo '<script src="bootstrap/js/modal.js"></script>';
    //echo '<script src="bootstrap/js/popover.js"></script>';
    //echo '<script src="bootstrap/js/scrollspy.js"></script>';
    //echo '<script src="bootstrap/js/tab.js"></script>';
    //echo '<script src="bootstrap/js/tooltip.js"></script>';
    //echo '<script src="bootstrap/js/transition.js"></script>';
    // Validering lokalt!
    ?> 
<!--    <script src="js/form-validator/jquery.form-validator.min.js" type="text/javascript"></script>
    <script>
        $.validate({
            modules: 'security',
            onModulesLoaded: function() {
                var optionalConfig = {
                    fontSize: '12pt',
                    padding: '4px',
                    bad: 'Very bad',
                    weak: 'Weak',
                    good: 'Good',
                    strong: 'Strong'
                };
                $('input[name="pass_confirmation"]').displayPasswordStrength(optionalConfig);
                $('#area').restrictLength($('#maxlength'));
            }
        });
    </script>-->
    <?php

}

/**
 * SHOP/PRODUCTS ADD OG REMOVE!
 */
function additem() {

    // Jeg bliver nødt til at bruge nogle globals for at bruge mine variabler i forskellige filer!
    global $obj;
    global $mysqli;
    global $product;
    global $results;

    //add item in shopping cart
    if (isset($_POST["type"]) && $_POST["type"] == 'add') {
        $product_code = filter_var($_POST["product_code"], FILTER_SANITIZE_STRING); //product code
        $return_url = base64_decode($_POST["return_url"]); //return url
        //MySqli query - get details of item from db using product code
        $results = $mysqli->query("SELECT products_name, products_price FROM products_has_cat_and_img WHERE products_nr='$product_code' LIMIT 1");
        $obj = $results->fetch_object();

        if ($results) { //we have the product info
            //prepare array for the session variable
            $new_product = array(array('name' => $obj->products_name, 'code' => $product_code, 'qty' => 1, 'price' => $obj->products_price));

            if (isset($_SESSION["products_id"])) { //if we have the session
                $found = false; //set found item to false

                foreach ($_SESSION["products_id"] as $cart_itm) { //loop through session array
                    if ($cart_itm["code"] == $product_code) { //the item exist in array
                        $qty = $cart_itm["qty"] + 1; //increase the quantity
                        $product[] = array('name' => $cart_itm["name"], 'code' => $cart_itm["code"], 'qty' => $qty, 'price' => $cart_itm["price"]);
                        $found = true;
                    } else {
                        //item doesn't exist in the list, just retrive old info and prepare array for session var
                        $product[] = array('name' => $cart_itm["name"], 'code' => $cart_itm["code"], 'qty' => $cart_itm["qty"], 'price' => $cart_itm["price"]);
                    }
                }

                if ($found == false) { //we didn't find item in array
                    //add new user item in array
                    $_SESSION["products_id"] = array_merge($product, $new_product);
                } else {
                    //found user item in array list, and increased the quantity
                    $_SESSION["products_id"] = $product;
                }
            } else {
                //create a new session var if does not exist
                $_SESSION["products_id"] = $new_product;
            }
        }

        //redirect back to original page
        header('Location:' . $return_url);
    }
}

function removeitem() {

    // Jeg bliver nødt til at bruge nogle globals for at bruge mine variabler i forskellige filer!
    global $product_code;
    global $product;
    global $return_url;

    //remove item from shopping cart
    if (isset($_GET["removep"]) && isset($_GET["return_url"]) && isset($_SESSION["products_id"])) {
        $product_code = $_GET["removep"]; //get the product code to remove
        $return_url = base64_decode($_GET["return_url"]); //get return url

        foreach ($_SESSION["products_id"] as $cart_itm) { //loop through session array var
            if ($cart_itm["code"] == $product_code) { //item exist in the list
                //continue only if quantity is more than 1
                //removing item that has 0 qty
                if ($cart_itm["qty"] > 1) {
                    $qty = $cart_itm["qty"] - 1; //just decrese the quantity
                    //prepare array for the products session
                    $product[] = array('name' => $cart_itm["name"], 'code' => $cart_itm["code"], 'qty' => $qty, 'price' => $cart_itm["price"]);
                }
            } else {
                $product[] = array('name' => $cart_itm["name"], 'code' => $cart_itm["code"], 'qty' => $cart_itm["qty"], 'price' => $cart_itm["price"]);
            }

            //set session with new array values
            $_SESSION["products_id"] = $product;
        }

        //redirect back to original page
        header('Location:' . $return_url);
    }
}

/**
 * REFRESH / HEADER
 */
function userRedirect($location) {

    echo "<script>window.location.href='" . $location . "'</script>";
}

function userRedirectOnTime($location, $time) {

    echo "<script>setTimeout(function (){window.location.href='" . $location . "';},$time);</script>";
}
