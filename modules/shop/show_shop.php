<?php
$products = new Products('products_has_cat_and_img');
$result_products = $products->select_products();

if (isset($_POST['submit_vare'])) {
    $vare = new $products('products_has_cat_and_img');
    $products->setCondition_parameter($parameter = array(
        array('WHERE', 'products_id', '=', $_POST['products_id'])
    ));
    $result_vare = $products->select_products();
    $row_vare = $result_vare->fetch_object();
    $total = $row_vare->products_max - $_POST['antal'];
    $kurv->putIKurv($row_vare->products_id, $_POST['antal'], $row_vare->products_name, $row_vare->products_price, $total);
    $kurv->updateAntal($row_vare->products_id, $total);
}
?>

<div class="row">
    <div class="col-xs-8">
        <h1>Produkter</h1>
        <?php
        while ($row_products = $result_products->fetch_object()) {

            // Hvad er der i kurven?
            echo '<div class="media well">';
            echo '<form method="post" action="">';
            echo '<div class="col-xs-4">';
            // husk at ændre sti til billedemappe hvis dette er nødvendigt
            echo '<div class="pull-left"><img class="media-object img-responsive" src="images/' . $row_products->products_cat_id . '/thumb_' . $row_products->products_image_name . '"></div>';
            echo '</div>';
            echo '<div class="col-xs-8">';
            echo '<div class="media-body">';
            echo '<h4 class="media-heading">' . $row_products->products_name . '</h4>';
            echo '<p>' . $row_products->products_desc . '</p>';
            echo 'Pris ' . $row_products->products_price;
            // klargører til mulighed for at skrive antal varer der skal i kurven
            echo '<br /><br />';
            // Skal tilføje så man selv kan bestemme hvor mange varer man vil ligge i kurven
            //echo '<input class="form-control" type="text" placeholder="Antal" name="custom_choice">';
            echo '<input type="hidden" name="products_id" value="' . $row_products->products_id . '"  >';
            echo '<input type="number" name="antal" min="1" max="' . $row_products->products_max . '" value="1" >';
            echo ' <br><br><input type="submit" name="submit_vare" class="btn btn-success pull-right" value="Læg i kurv" />';
            echo '</div>';
            echo '</div>';
            echo '</form>';
            echo '</div>';
        }
        ?>
    </div>

    <div class="col-xs-4">
        <h2 class="glyphicon glyphicon-shopping-cart"></h2>
        <?php
        echo 'Du har (' . $kurv->countVare() . ') vare i kurven <br />';
        echo'Samlet pris ' . $kurv->samletPris();
        echo '<br /> <a href="?frontend_page=front_allproducts_webshop" >Gå til Kurv</a>';
        ?>
    </div>
</div>

