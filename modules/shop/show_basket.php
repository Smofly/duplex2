<?php
// dette er kurven
if (isset($_POST['plus'])) {
    $kurv->plusAntal();
}
if (isset($_POST['minus'])) {
    $kurv->minusAntal();
}
if (isset($_POST['slet'])) {
    foreach ($_SESSION['kurv'] as $key => $antal) {
        $total = $antal['antal'] + $antal['antalStk'];
        $kurv->updateAntal($key, $total);
    }
    $kurv->fjernVare();
}
if (isset($_POST['dellall'])) {
    foreach ($_SESSION['kurv'] as $key => $antal) {
        $total = $antal['antal'] + $antal['antalStk'];
        $kurv->updateAntal($key, $total);
    }
    $kurv->sletKurv();
}

echo $kurv->visKurv();
echo '<br />Ialt '. $kurv->countVare();
echo '<br />Ialt '. $kurv->samletPris();
$kurv->test();
?>

<?php
if (!empty($_SESSION['kurv'])) {
    ?>
    <form action="" method="POST">
        <input type="submit" name="dellall" value="Fjern Alle Vare" />
    </form>
    <a href="kassen.php">Til Kassen</a> 
    <?php
}
?>