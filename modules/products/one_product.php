<div class="row">

    <?php
    $one_product = new Products('products_has_cat_and_img');

// hvis der søges på varenummer, selectes der ud fra varenummer
    if (isset($_POST['submit_varenummer'])) {
        $one_product->setCondition_parameter($parameter = array(
            array('WHERE', 'products_nr', '=', $_POST['varenummer'])
        ));
    }
// hvis der søges på de andre selectes der ud fra id fra url.
    if (isset($_GET['id'])) {
        $one_product->setCondition_parameter($parameter = array(
            array('WHERE', 'products_id', '=', $_GET['id'])
        ));
    }

    $result_one_product = $one_product->select_products();
    $row_one_product = $result_one_product->fetch_object();
    ?>

    <div class="col-md-12">
        <h2><?php echo $row_one_product->products_name ?></h2>
    </div>
    <div class="col-md-12">
        <img class="img-responsive" src="images/<?php echo $row_one_product->products_cat_id ?>/<?php echo $row_one_product->products_image_name ?>" alt="<?php echo $row_one_product->products_image_name ?>" />
    </div>
    <div class="col-md-12"><?php echo $row_one_product->products_desc ?>
    </div>
    <br /><br />
    <div class="col-md-12">
        <h2>Andre varianter</h2>
        <?php
        $show_thumb = new Products('products_image');
        $show_thumb->setCondition_parameter($parameter = array(
            array('WHERE', 'products_products_id', '=', $row_one_product->products_id)
        ));
        $result_show_thumb = $show_thumb->select_products();
        while ($row_show_thumb = $result_show_thumb->fetch_object()) {
            ?>
            <img style="padding-right:50px;" src="images/<?php echo $row_one_product->products_cat_id ?>/thumb_<?php echo $row_show_thumb->products_image_name ?>" alt="<?php echo $row_show_thumb->products_image_name ?>" />
            <?php
        }
        ?>
    </div>

    <div class="col-md-12">
        <br />
        <strong>Pris: </strong><?php echo $row_one_product->products_price ?><br />
        <strong>Designer: </strong><?php echo $row_one_product->products_design ?><br/>
        <strong>Møbelserie: </strong><?php echo $row_one_product->products_cat_name ?><br />
        <strong>Design år: </strong><?php echo $row_one_product->products_year ?><br />
        <strong>Varenr:: </strong><?php echo $row_one_product->products_nr ?><br />
    </div>
</div>



