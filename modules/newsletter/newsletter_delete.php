<h3>FRAMELD DIG VORES NYHEDSBREV</h3>
<p>Indtast venligst den registrerede email adresse.</p>
<?php
// Instantiat classes
$newsletter = new Newsletter('newsletter');
$validering = new Validering();
if (isset($_POST['newsletter_submit'])) {

    $email = $_POST['email'];
    $newsletter->setCondition_parameter($parameter = array(
        array('WHERE', 'newsletter_email', '=', "'$email'")
    ));

    $result_newsletter = $newsletter->select_newsletter();
     $num = mysqli_num_rows($result_newsletter);


    if (!$num == 0) {
        $newsletter->setCondition_field('newsletter_email');
        $newsletter->setCondition_operator('=');
        $newsletter->setCondition_value($_POST['email']);
        if ($newsletter->delete_newsletter() == true) {
            echo 'Du er nu Frameldt';
        }
    } else {
        $newsletter->setError('Emailen ikke gyldig');
    }
}
if (!$newsletter->getError() == false) {
    echo '<p>' . $newsletter->getError() . '</p>';
}
?>
<form class="form-inline" action="" method="POST">
    <div class="input-group">
        <input class="form-control" name="email" placeholder="Frameld nyhedsbrev" required="">
        <span class="input-group-btn">
            <button type="submit" class="btn btn-danger" name="newsletter_submit">Frameld</button>
        </span>
    </div>
</form>