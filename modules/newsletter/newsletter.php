<?php
// Instantiat classes
$newsletter = new Newsletter('newsletter');
$validering = new Validering();
if (isset($_POST['newsletter_submit'])) {
    $email = $_POST['email'];
    // Select All newsletters and check if email is in db allready
    $newsletter->setCondition_parameter($parameter = array(
        array('WHERE', 'newsletter_email', '=', "'$email'")
    ));

    $result_newsletter = $newsletter->select_newsletter();
    $num = mysqli_num_rows($result_newsletter);


    if (!$num == 0) {
        $newsletter->setError('Du er allerede tilmeldt vores Nyhedsbrev');
    } else {
        $validering->checkEmail($_POST['email'], 'Ikke gyldig email prøv igen');
        // Validering
        if ($validering->getFejl() == false) {
            // set Emailen til classen
            $newsletter->setEmail($_POST['email']);
            // insert Data
            $newsletter->tableoption(1);
            if ($newsletter->insert_newsletter() == true) {
                echo 'Du er nu tilmeldt';
            }
        } else {
            $newsletter->setError('Emailen ikke gyldig');
        }
    }
}
?>
<h3>TILMELD DIG VORES NYHEDSBREV</h3>
<?php
if (!$newsletter->getError() == false) {
    echo '<p>' . $newsletter->getError() . '</p>';
}
?>
<form class="form-inline" action="" method="POST">
    <div class="input-group">
        <input class="form-control" name="email" placeholder="Tilmeld nyhedsbrev">
        <span class="input-group-btn">
            <button type="submit" class="btn btn-success" name="newsletter_submit">Tilmeld</button>
        </span>
    </div>

    <a href="?frontend_page=front_newsletter_delete">Fjern mig fra nyhedsbrev</a>

</form>