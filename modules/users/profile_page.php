<?php if (!in_array('Guest', $users_rank->getRolesRank())) { ?>
    <div class="row">

        <div class="col-md-6">
            <p><strong>Velkommen: </strong>Brugernavn</p>


        </div>
        <div class="col-md-6">
            <img src="http://placehold.it/140x180" alt="..." class="img-thumbnail pull-right"><br /><br />
            <form action="">
                <div class="form-group">
                    <label for="exampleInputFile">Skift profilbillede</label>
                    <input type="file" id="exampleInputFile">
                    <p class="help-block">Skift dit profilbillede</p>
                </div>
            </form>
        </div>
    </div>
    <div class="tab-pane" id="tabs-side">
        <div class="tabbable tabs-left">
            <ul class="nav nav-tabs span2">
                <li class="active"><a href="#tabs2-pane1" data-toggle="tab">Velkommen</a></li>
                <li><a href="#tabs2-pane2" data-toggle="tab">Rediger din profil</a></li>
                <li><a href="#tabs2-pane3" data-toggle="tab">Tab 3</a></li>
                <li><a href="#tabs2-pane4" data-toggle="tab">Tab 4</a></li>
            </ul>
            <div class="tab-content span5">
                <div id="tabs2-pane1" class="tab-pane active">
                    <h4>Velkommen til din profil</h4>
                    <p>Ut porta rhoncus ligula, sed fringilla felis feugiat eget. In non purus quis elit iaculis tincidunt. Donec at ultrices est.</p>
                </div>
                <div id="tabs2-pane2" class="tab-pane">
                    <h4>Rediger din profil</h4>
                    <p>Ut porta rhoncus ligula, sed fringilla felis feugiat eget. In non purus quis elit iaculis tincidunt. Donec at ultrices est.</p>
                </div>
                <div id="tabs2-pane3" class="tab-pane">
                    <h4>Pane 3 Content</h4>
                    <p>Ut porta rhoncus ligula, sed fringilla felis feugiat eget. In non purus quis elit iaculis tincidunt. Donec at ultrices est.</p>
                </div>
                <div id="tabs2-pane4" class="tab-pane">
                    <h4>Pane 4 Content</h4>
                    <p>Donec semper vestibulum dapibus. Integer et sollicitudin metus. Vivamus at nisi turpis. Phasellus vel tellus id felis cursus hendrerit. Suspendisse et arcu felis, ac gravida turpis. Suspendisse potenti.</p>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
