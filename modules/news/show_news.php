<?php
// sætter class
$count_news = new News('news');
$result_count_news = $count_news->select_news();
// hvor mange rows skal der visers pr. side og hvad vil du kalde dem?
$pagination = new Paginator('3', 'pagination');
//tæller hvor mange rows der er i result
$num_row = mysqli_num_rows($result_count_news);
$pagination->set_total($num_row);
$limit = $pagination->get_limit();
// sætter class
$show_news = new News('news_has_images_and_users');
// select news fra databasse
$show_news->setCondition_limit($limit);
$show_news->setCondition_order($order = array('news_id', 'DESC'));
$result_show_news = $show_news->select_news();
// Loop News out on index.php
while ($row_show_news = $result_show_news->fetch_object()) {
    // for at bruge dato og tid
    $date = new DateTime($row_show_news->news_time);
    // begræns længden på nyheden
    $newstring = substr($row_show_news->news_text, 0, 130);
    ?>
    <br />
    <div class="row">
        <div class="col-md-1">
            <div class="well well-sm well-warning">
                <h4 class="text-center"><?php echo $date->format('j') ?></h4>
                <p class="text-center"><?php echo $date->format('M') ?></p>
            </div>
        </div>
        <div class="col-sm-3">
            <?php
            if($row_show_news->images_news_id == ''){
                echo 'ingen billede';
            }else{
                ?>
            <img src="images/news/thumb_<?php echo $row_show_news->news_images_name ?>" />
            <?php 
            }
            ?>
        </div>
        <div class="col-sm-8">
            <?php
            if ($row_show_news->news_users_id == NULL) {
                $name = 'Brugeren er slettet';
            } else {
                $name = $row_show_news->users_username;
            }
            ?>
            <h4><a href="?frontend_page=front_news_one&news_id=<?php echo $row_show_news->news_id; ?>"><?php echo $row_show_news->news_headline ?></a></h4>
            <p><?php echo $name ?></p>
            <p><?php echo $newstring ?></p>
            <a href="?frontend_page=front_news_one&news_id=<?php echo $row_show_news->news_id; ?>"><button class="btn btn-sm btn-info pull-right">Læs mere</button></a>
            <br /><hr />
        </div>
    </div>
    <?php
}
// hvis der eksempelvis bruges switch som menu. Skal der angives url som vist nedenfor
echo $pagination->page_links('?frontend_page=front_news&');
?>




















