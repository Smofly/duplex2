<?php
$id = $_GET['news_id'];
if (isset($_GET['action']) && $_GET['action'] == 'slet') {
    $delete_comment = new Comments('news_comments');
    $delete_comment->setCondition_field('com_id');
    $delete_comment->setCondition_operator('=');
    $delete_comment->setCondition_value($_GET['com_id']);
    if ($delete_comment->delete_comment() == true) {

        userRedirect("?frontend_page=front_news_one&news_id=$id&delete=succes");
    }
}
if (isset($_GET['delete']) && $_GET['delete'] == 'succes') {
    echo '<div class="alert alert-warning">Kommentaren er nu slettet</div>';
}
if (isset($_GET['commet']) && $_GET['commet'] == 'succes') {
    echo '<div class="alert alert-info">Tak for din kommentar</div>';
}
if (isset($_GET['commet']) && $_GET['commet'] == 'deny') {
    echo '<div class="alert alert-danger">noget gik galt!</div>';
}
if (isset($_GET['validate']) && $_GET['validate'] == 'deny') {
    echo '<div class="alert alert-danger">Formen er ikke udfyldt korrekt!</div>';
}

//------******* Henter alle nyheder *******-------
$one_news = new News('news');

if (isset($_GET['news_id'])) {
    $one_news->setCondition_parameter($parameter = array(
        array('WHERE', 'news_id', '=', $_GET['news_id'])
    ));
}
$result_one_news = $one_news->select_news();
$row_one_news = $result_one_news->fetch_object();
?>
<div class="row">
    <div class="col-md-12">
        <h2><?php echo $row_one_news->news_headline ?></h2>
        <p><?php echo $row_one_news->news_text ?></p>
    </div>
</div>
<!------******* Henter kommentarer *******-------> 
<?php
// instaciater class
$count_comments = new News('news_has_comments_and_users');
// skal select comments ud fra news_id og tæller rows/indhold
$count_comments->setCondition_parameter($parameter = array(
    array('WHERE', 'com_news_id', '=', $_GET['news_id'])
));

$result_count_comments = $count_comments->select_news();
//instanciater paginator classen
$pagination_comments = new Paginator('5', 'pagination');
// count/tæller resultat jeg har fået ud af min ovenstående select
$num_row_comments = mysqli_num_rows($result_count_comments);
// regner id fra limit og indhold hvor mange sider der skal være i min limit
$pagination_comments->set_total($num_row_comments);
$limit = $pagination_comments->get_limit();
// hvis der ikke er kommenteret, hvad så?
if ($num_row_comments == 0) {
    echo "<hr />";
    echo "Der er ingen kommentarer";
} else {
// hvis der er kommentarer så skal disse vises
    $one_news_comments = new News('news_comments_has_users');


    $one_news_comments->setCondition_parameter($parameter = array(
        array('WHERE', 'com_news_id', '=', $_GET['news_id'])
    ));
    $one_news_comments->setCondition_order($order = array('com_id', 'DESC'));
    $one_news_comments->setCondition_limit($limit);
    $result_one_news_comments = $one_news_comments->select_news();
    while ($row_one_news_comments = $result_one_news_comments->fetch_object()) {
        $date = new DateTime($row_one_news_comments->com_time);
        ?>
        <div class="row">
            <div class="col-md-12">
                <hr />
                <div class="col-md-3">
                    <?php
                    if ($row_one_news_comments->com_users_id == NULL) {
                        $name = 'Brugeren er slettet';
                    } else {
                        $name = $row_one_news_comments->users_username;
                    }
                    ?>
                    <img src="http://placehold.it/100x100" alt="..." class="img-circle">
                    <p><?php echo '<p><strong>Brugernavn: </strong>' . $name . '</p>' . '<p><strong>Tid: </strong>' . $date->format('d-m-Y H:i:s') ?></p>
                </div>
                <div class="col-md-8">
                    <!--det er ikke nødvendig at pakke kommentaren ind i et <p> da vores tinMCE klarer dette for os-->
                    <?php echo $row_one_news_comments->com_comment ?>
                </div> 
                <div class="col-md-1">
                    <?php
                    if (in_array('Admin', $users_rank->getRolesRank()) || in_array('Superadmin', $users_rank->getRolesRank())) {
                        ?>
                        <a class="confirm pull-right" href="?frontend_page=front_news_one&news_id=<?php echo $_GET['news_id'] ?>&action=slet&com_id=<?php echo $row_one_news_comments->com_id ?>"><i class="glyphicon glyphicon-trash"></i></a>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>

        <?php
    }
    echo $pagination_comments->page_links('?frontend_page=front_news_one&news_id=' . $_GET['news_id'] . '&');
} // slut på tjek om der er kommantarer
?>
<hr />
<?php
// tjek om der er logget ind for at give adgang til at kommentere
if (in_array('Guest', $users_rank->getRolesRank())) {
    echo "Du skal være logget ind for at kommentere";
} else {
    ?>
    <?php
    $new_comment = new Comments('news_comments');
    if (isset($_POST['submit_comment'])) {
        // Valider længden af kommentaren
        $validering = new Validering();

        $validering->checkMinLength($_POST['com_comment'], 10);
        $validering->checkMaxLength($_POST['com_comment'], 250);
        // instanciater class
        if ($fejl = $validering->getFejl() == false) {
            // instanciater class
            $_POST['com_comment'] = preg_replace('/[^A-Za-z0-9\. -]/', '', $_POST['com_comment']);
            $new_comment->setCom_comment($_POST['com_comment']);
            $new_comment->setCom_news_id($_GET['news_id']);
            $new_comment->setCom_users_id($_SESSION['users_id']);

            // tableoption sender data til classen
            $new_comment->tableoption(1);

            if ($new_comment->insert_comment() == true) {
                userRedirect("?frontend_page=front_news_one&news_id=$id&commet=succes");
            } else {
                userRedirect("?frontend_page=front_news_one&news_id=$id&commet=deny");
            }
        } else {
            userRedirect("?frontend_page=front_news_one&news_id=$id&validate=deny");
        }
    }
    ?>
    <div class="row">
        <div class="col-md-8">
            <form  role="form" method="POST" action="">

                <label for="com_comment">Ny kommentar?</label><br />
                Max (<span id="maxlength">250</span> tegn tilbage)
                <textarea id="area" class="form-control" name="com_comment" placeholder="Skriv din kommentar her" rows="6"></textarea><br />

                <input type="submit" name="submit_comment" class="btn btn-primary" value="Udgiv"/>

            </form>
        </div>
        <div class="col-md-4"></div>
    </div>
<?php } ?>