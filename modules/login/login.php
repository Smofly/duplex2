<?php
// include the language file
include 'modules/login/language.php';
// instantiate and activate the class with the tablename users!

$login = new Users('users');

$validate = new Validering();
if (isset($_POST['login_submit'])) {

    $validate->checkMinLength($_POST['username'], 3, 'Du skal minimum have 3 tegn');
    $validate->checkMinLength($_POST['password'], 6, 'Dit password skal minimum værre 6 tegn');

    if ($validateerror = $validate->getFejl()) {

        foreach ($validateerror as $value) {
            echo '<p>' . $value . '</p>';
        }
    } else {
        $login->setUsers_username($_POST['username']);
        $login->setUsers_password($_POST['password']);


        if ($login->logincheck() == false) {
            $fejl = $login->getError();
            foreach ($fejl as $value) {
                echo '<p>' . $value . '</p>';
            }
        } else {
            $login->redirect('index.php');
        }
    }
}
?>
<div class="col-md-8">
    <form action=""  method="POST">
        <div class="form-group">
            <label for="username"><?php echo $usernameLabel; ?></label>
            <input class="form-control" type="text" id="username" name="username" data-validation="length" data-validation-length="3-45" data-validation-help="Skriv dit brugernavn"  class="form-control input-lg username-field" />
        </div> <!-- /field -->
        <div class="form-group">
            <label for="password"><?php echo $password; ?></label>
            <input class="form-control" type="password" id="password" name="password"  data-validation="length" data-validation-length="min6" data-validation-help="Skriv dit koderord" class="form-control input-lg password-field"/>
        </div> <!-- /password -->
        <div class="form-group">
            <input class=" btn btn-default" type="submit" value="Log ind" name="login_submit"/>
        </div> <!-- .actions -->
</div> <!-- /login-fields -->
</form>
</div>
<div class="col-md-4"></div>

