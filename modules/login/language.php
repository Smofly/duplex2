<?php
// Login and signup language file, feel free to translate here!
$usernameLabel = "Dit brugernavn:";
$username = "Brugernavn";
$passwordLabel ="Kodeord:";
$password = "Kodeord";
$passwordConfirmationLabel ="Indtast ønskede kodeord igen:";
$passwordConfirmation = "Kodeord igen";
$spamCheckLabel ="Hvad er 4+3?";
$spamCheckPlaceholder ="Er du et menneske?";
$spamCheckResult = "7";
$userEmailLabel = 'Email';