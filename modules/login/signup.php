<?php
// include the language file
include_once 'modules/login/language.php';
$insert_users = new Users('users');
if (isset($_POST['submit_opret'])) {

    $email = $_POST["email"];
    $username = $_POST['username'];
    $insert_users->setCondition_parameter($parameter = array(
        array('WHERE', 'users_email', '=', "'$email'"),
        array('OR', 'users_username', '=', "'$username'")
    ));
    $result_insertUsers = $insert_users->select_user();
    $count_users = mysqli_num_rows($result_insertUsers);

    if (!$count_users == 0) {
        $insert_users->redirect('?frontend_page=front_signup&oprettet=deny');
    } else {
        $spamcheck = $insert_users->calculate($_POST['spamcheck']);

        if ($spamcheck == false) {
            $insert_users->redirect('?frontend_page=front_signup&spamcheck=deny');
        } else {

            $validering = new Validering();

            // Check if Rank and Active is a number. if not make a error
            $validering->checknumeric($_POST['spamcheck']);

            // Check adress and email
            $validering->checkEmail($_POST['email']);
            // Check username and password
            $validering->checkName($_POST['username']);
            $validering->checkMinLength($_POST['username'], 3);

            $validering->checkMinLength($_POST['pass_confirmation'], 6);
            $validering->checkPassword($_POST['pass_confirmation']);
            $validering->checkMatch($_POST['pass_confirmation'], $_POST['pass']);

            if ($fejl = $validering->getFejl() == false) {

                $insert_users->setUsers_firstname('Intet indtastet endnu');
                $insert_users->setUsers_lastname('Intet indtastet endnu');
                $insert_users->setUsers_adress('Intet indtastet endnu');

                $insert_users->setUsers_username($_POST['username']);
                $insert_users->setUsers_email($_POST['email']);
                $insert_users->setUsers_password($_POST['pass_confirmation']);

                $insert_users->setUsers_active(1);
                $insert_users->setRoles_roles_id(2);

                $insert_users->tableoption(1);
                if (!$insert_users->insert_user() == false) {
                    $insert_users->redirect('?frontend_page=front_signup&oprettet=succes');
                }
            } else {
                $insert_users->redirect('?frontend_page=front_signup&vali=deny');
            }
        }
    }
}
?>

<div class="col-md-8">

    <?php
    if (isset($_GET['oprettet']) && $_GET['oprettet'] == 'succes') {
        echo '<div class="alert alert-success">Brugeren er nu oprettet</div>';
    }
    if (isset($_GET['oprettet']) && $_GET['oprettet'] == 'deny') {
        echo '<div class="alert alert-danger">Der findes allerede en Bruger med valgte Brugernavn/Email</div>';
    }
    if (isset($_GET['spamcheck']) && $_GET['spamcheck'] == 'deny') {
        echo '<div class="alert alert-danger">Du har svaret forkert på spamchecket</div>';
    }
    if (isset($_GET['vali']) && $_GET['vali'] == 'deny') {
        echo '<div class="alert alert-danger">En eller flere felter var ikke udfyldt korekt</div>';
    }
    ?>

    <form action="" id="form" method="POST">
        <div class="form-group">
            <label for="username"><?php echo $usernameLabel; ?></label>
            <input type="text" class="form-control" name="username" data-validation="length alphanumeric"  data-validation-length="3-12" placeholder="<?php echo $username; ?>" data-validation-error-msg="Dit brugernavn skal minimum værre 3 max 12 tegn">
        </div>
        <div class="form-group">
            <label for="email"><?php echo $userEmailLabel; ?></label>
            <input type="email" class="form-control" name="email" data-validation="email" placeholder="<?php echo $username; ?>" data-validation-error-msg="Din Email er ikke gyldig">
        </div>
        <div class="form-group">
            <label for="password"><?php echo $passwordLabel; ?></label>
            <input type="password" class="form-control" name="pass_confirmation" data-validation="strength" data-validation-strength="2" data-validation="length" data-validation-length="min6" placeholder="<?php echo $password; ?>" data-validation-error-msg="Adgangskoden er ikke stærk nok">
        </div>
        <div class="form-group">
            <label for="passwordConfirmation"><?php echo $passwordConfirmationLabel; ?></label>
            <input type="password" class="form-control" name="pass" data-validation="confirmation" placeholder="<?php echo $passwordConfirmation; ?>" data-validation-error-msg="Din Adgangskoden matcher ikke hinanden">
        </div>
        <div class="form-group">
            <label for="spamcheck"><?php echo $spamCheckLabel; ?></label>
            <input type="text" class="form-control" name="spamcheck"  data-validation="spamcheck" data-validation-captcha="<?php echo $spamCheckResult; ?>" placeholder="<?php $spamCheckPlaceholder; ?>" data-validation-error-msg="Du har ikke udfyldt sikkerhedsspørgsmålet Korekt">
        </div>
        <input class="btn btn-default" type="submit" name="submit_opret" value="Opret bruger">
    </form>
</div>
<div class="col-md-4"></div>