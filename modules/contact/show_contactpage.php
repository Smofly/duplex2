<?php
if (isset($_POST['submit'])) {

    $validering = new Validering();

    // tjek fornavn og efternavn
    $validering->checkName($_POST['fornavn']);
    $validering->checkName($_POST['efternavn']);
    // tjek email
    $validering->checkEmail($_POST['email']);
    //tjek om postnummeret har mindst 4 cifre
    $validering->checkMinLength($_POST['postnr'], 4);
    // tjek om telefonnummer har mindst 8 cifre
    $validering->checkMinLength($_POST['tlfnr'], 8);
    // tjek beskeden er mere end 250 tegn
    $validering->checkMaxLength($_POST['message'], 250);
   $_POST['message'] = preg_replace('/[^A-Za-z0-9\. -]/', '', $_POST['message']);
    if ($fejl = $validering->getFejl() == false) {
        echo '<p class="success">Beskeden er sendt</p>';
    } else {
        echo '<p class="warning">desværre noget gik galt</p>';
    }
}
?>
<div class="col-md-6">

    <!-- her er kontakt form, tilføj nye felter, fjern eller udkommenter de felter du ikke skal bruge -->
    <form action=""  method="POST">
        <div class="field">
            <label for="fornavn">Fornavn</label>
            <input type="text" id="fornavn" name="fornavn" data-validation="length" data-validation-length="3-45" data-validation-help="Skriv dit fornavn" data-validation-error-msg="Der skal være imellem 3-45 cifre" class="form-control input-lg username-field" />
        </div> <!-- /field -->

        <div class="field">
            <label for="efternavn">Efternavn</label>
            <input type="text" id="Fornavn" name="efternavn" data-validation="length" data-validation-length="3-45" data-validation-help="Skriv dit efternavn" data-validation-error-msg="Der skal være imellem 3-45 cifre" class="form-control input-lg username-field" />
        </div> <!-- /field -->

        <div class="field">
            <label for="email">Email</label>
            <input type="text" id="email" name="email" data-validation="email" data-validation-error-msg="Du skal huske at udfylde email" class="form-control input-lg username-field" />
        </div> <!-- /field -->

        <div class="field">
            <label for="postnr">Postnummer</label>
            <input type="text" id="postnr" name="postnr" data-validation="length" data-validation-length="min4" data-validation-help="Skriv dit postnummer" data-validation-error-msg="Du skal skrive mindst 4 cifre" class="form-control input-lg username-field" />
        </div> <!-- /field -->

        <div class="field">
            <label for="tlfnr">Telefonnummer</label>
            <input type="text" id="tlfnr" name="tlfnr" data-validation="length" data-validation-length="min8" data-validation-help="Skriv dit telefonnummer" data-validation-error-msg="Du skal skrive mindst 8 cifre" class="form-control input-lg username-field" />
        </div> <!-- /field -->
        <div class="field">
            <label for="message">Din besked</label>
            Max (<span id="maxlength">250</span> tegn tilbage)
            <textarea id="area" name="message" class="form-control" rows="3"></textarea>
        </div>
        <br />
        <div class="login-actions">
            <input class="btn btn-success" type="submit" value="Send" name="submit"/>
        </div> <!-- .actions -->
    </form>
</div>
<div class="col-md-6">
    <h2>Information</h2>
    <p>Her kan man evt. placere kort, adresse eller anden information</p>
</div>

