<?php
if (isset($_GET['action']) && $_GET['action'] == 'slet') {
    $forum_id = $_GET['forum_id'];
    $delete_comment = new Forum('forum_comments');
    $delete_comment->setCondition_field('fcom_id');
    $delete_comment->setCondition_operator('=');
    $delete_comment->setCondition_value($_GET['fcom_id']);
    if ($delete_comment->delete_forum() == true) {

        userRedirect("?frontend_page=front_forum_one&forum_id=$forum_id&delete=succes");
    }
}
if (isset($_GET['delete']) && $_GET['delete'] == 'succes') {
    echo '<div class="alert alert-warning">Kommentaren er nu slettet</div>';
}
  if (isset($_GET['forum_comment']) && $_GET['forum_comment'] == 'succes') {
        echo '<div class="alert alert-info">Tak for din kommentar</div>';
    }
    if (isset($_GET['forum_comment']) && $_GET['forum_comment'] == 'deny') {
        echo '<div class="alert alert-danger">noget gik galt!</div>';
    }
    if (isset($_GET['forum_validate']) && $_GET['forum_validate'] == 'deny') {
        echo '<div class="alert alert-danger">noget gik galt!</div>';
    }

$one_thread = new Forum('forum');
$one_thread->setCondition_parameter($parameter = array(
    array('WHERE', 'forum_id', '=', $_GET['forum_id'])
));

$result_one_thread = $one_thread->select_forum();
$row_one_thread = $result_one_thread->fetch_object();
?>

<div class="row">
    <div class="col-md-12">
        <h2><?php echo $row_one_thread->forum_headline ?></h2>
        <p><?php echo $row_one_thread->forum_text ?></p>
    </div>
</div>
<hr />
<!------******* Henter kommentarer *******------->
<?php
// instaciater class
$count_comments = new Forum('forum_comments_has_users');
// skal select comments ud fra news_id og tæller rows/indhold
$count_comments->setCondition_parameter($parameter = array(
    array('WHERE', 'com_forum_id', '=', $_GET['forum_id'])
));

$result_count_comments = $count_comments->select_forum();
//instanciater paginator classen
$pagination_comments = new Paginator('5', 'pagination');
// count/tæller resultat jeg har fået ud af min ovenstående select
$num_row_comments = mysqli_num_rows($result_count_comments);
// regner id fra limit og indhold hvor mange sider der skal være i min limit
$pagination_comments->set_total($num_row_comments);
$limit = $pagination_comments->get_limit();
// hvis der ikke er kommenteret, hvad så?
if ($num_row_comments == 0) {
    
    echo "Der er ikke nogen der har svaret på indlægget";
    echo "<hr />";
} else {
// hvis der er kommentarer så skal disse vises
    $one_thread_comments = new Forum('forum_comments_has_users');

    $one_thread_comments->setCondition_parameter($parameter = array(
        array('WHERE', 'com_forum_id', '=', $_GET['forum_id'])
    ));
    // Sorter nyeste først
    $one_thread_comments->setCondition_order($order = array('fcom_id', 'DESC'));

    $one_thread_comments->setCondition_limit($limit);
    $result_one_thread_comments = $one_thread_comments->select_forum();
    while ($row_one_thread_comments = $result_one_thread_comments->fetch_object()) {
        if ($row_one_thread_comments->com_users_id == NULL) {
            $name = 'Brugeren er slettet';
        } else {
            $name = $row_one_thread_comments->users_username;
        }
        ?>            
        <div class="row">
            <div class="col-md-3">
                <img src="http://placehold.it/100x100" alt="..." class="img-circle">
                <p><?php echo '<p><strong>Brugernavn: </strong>' . $name . '</p>' . '<p><strong>Tid: </strong>' . $row_one_thread_comments->fcom_time ?></p>
            </div>
            <div class="col-md-8">
                <!--det er ikke nødvendig at pakke kommentaren ind i et <p> da vores tinMCE klarer dette for os-->
                <?php echo $row_one_thread_comments->fcom_comment ?>
            </div>        
            <?php
            if (in_array('Admin', $users_rank->getRolesRank()) || in_array('Superadmin', $users_rank->getRolesRank())) {
                ?>
                <div class="col-md-1">
                    <a class="confirm pull-right" href="?frontend_page=front_forum_one&forum_id=<?php echo $_GET['forum_id'] ?>&action=slet&fcom_id=<?php echo $row_one_thread_comments->fcom_id ?>"><i class="glyphicon glyphicon-trash"></i></a>
                </div>               
            </div>
            <hr />
            <?php
        }
        ?>              
        <?php
    }

    echo $pagination_comments->page_links('?frontend_page=front_forum_one&forum_id=' . $_GET['forum_id'] . '&');
} // slut på tjek om der er kommantarer
?>
<?php
// tjek om der er logget ind for at give adgang til at kommentere
if (in_array('Guest', $users_rank->getRolesRank())) {
    echo "Du skal være logget ind for at kommentere";
} else {
    ?>
    <?php
    $new_comment = new Forum('forum_comments');
    if (isset($_POST['submit_comment'])) {
        // Valider længden af kommentaren
        $validering = new Validering();

        $validering->checkMinLength($_POST['fcom_comment'], 10);
        $validering->checkMaxLength($_POST['fcom_comment'], 250);
        
        if ($fejl = $validering->getFejl() == false) {
            // instanciater class
            $_POST['fcom_comment']  = preg_replace('/[^A-Za-z0-9\. -]/', '', $_POST['fcom_comment'] );
            $new_comment->setFcom_comment($_POST['fcom_comment']);
            $new_comment->setCom_forum_id($_GET['forum_id']);
            $new_comment->setCom_users_id($_SESSION['users_id']);

            // tableoption sender data til classen
            $new_comment->tableoption(2);
            $forum_id = $_GET['forum_id'];
            if ($new_comment->insert_forum() == true) {
                userRedirect("?frontend_page=front_forum_one&forum_id=$forum_id&forum_comment=succes");
            } else {
                userRedirect("?frontend_page=front_forum_one&forum_id=$forum_id&forum_comment=deny");
            }
        } else {
            $forum_id = $_GET['forum_id'];
            userRedirect("?frontend_page=front_forum_one&forum_id=$forum_id&forum_validate=deny");
        }
    }
    ?>
    <div class="col-md-6">

        <form  role="form" method="POST" action="">

            <label for="fcom_comment">Din kommentar</label><br />
            Max (<span id="maxlength">250</span> tegn tilbage)
            <textarea id="area" class="form-control" name="fcom_comment" placeholder="Skriv din kommentar her" cols="50" rows="5"></textarea><br />
            
            <input type="submit" name="submit_comment" class="btn btn-primary" value="Send"/>

        </form>
    </div>
    <div class="col-md-6"></div>
<?php }
?>