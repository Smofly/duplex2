<div class="container"><div class="row">
<?php
$newsid = $_GET['newsid'];

$delete_news = new News('news');

$delete_news->setCondition_field('news_id');
$delete_news->setCondition_operator('=');
$delete_news->setCondition_value($newsid);

if($delete_news->delete_news() == true){
    echo '<div class="alert alert-success">Nyheden er slettet</div>';
}else{
    echo '<div class="alert alert-danger">Noget gik galt!</div>';
}
?>
</div></div>
