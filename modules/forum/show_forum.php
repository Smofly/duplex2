<br />
<!-- KUN hvis du er logget ind som admin kommer mulighederne redigere og slet frem til hver tråd! -->
<div class="row">
    <div class="col-md-12">
        <!-- forum kategori eks.1 -->                    
        <?php
        $forum_catgory = new Forum_category('forum_category');
        $result_forum_category = $forum_catgory->select_forum();

        $forum_subcat = new Forum_category('forum_subcat');

        foreach ($result_forum_category as $forum_category) {
            ?>
            <table class="table table-bordered table-stripped">
                <thead>
                    <tr>
                        <?php
                        echo '<h2>' . $forum_category['fcat_name'] . '</h2>';
                        ?>
                        <th class="info">Kategori</th>
                        <th class="info">Tråde</th>
                        <th class="info">Seneste tråd</th>
                        <th class="info">Dato og Forfatter</th>              
                        <th class="info" style="width: 34px;"></th>

                    </tr>
                </thead>
                <?php
                // Selecter med en where statement. hvor du tager id`ét fra den øverste foreach
                $forum_subcat->setCondition_parameter($parameter = array(array('WHERE', 'forum_category_fcat_id', '=', $forum_category['fcat_id'])));
                $result_forum_subcat = $forum_subcat->select_forum();

                // Count threads
                $count_fcat = new Forum('forum');
                $count_fcat->setCondition_parameter($parameter = array(array('WHERE', 'forum_subcat_fsubcat_id', '=', $forum_category['fcat_id'])));
                $result_count_threads = $count_fcat->select_forum();
                $count_threads = mysqli_num_rows($result_count_threads);


                foreach ($result_forum_subcat AS $fsubcats) {
                    ?>
                    <tbody>
                        <tr>
                            <td class="active">
                                <?php
                                // navnet på sub kategorien / underkatgorien
                                echo '<a href="?frontend_page=show_threads&id=' . $fsubcats['fsubcat_id'] . '">' . $fsubcats['fsubcat_name'] . '</a>';
                                ?>
                            </td>
                            <td class="active"><?php echo $count_threads ?></td>

                            <?php
                            // Instanstantiate Forum classen
                            $select_threads = new Forum('forum_has_users');
                            // Select de tråede med id tilsvarende subcat
                            $select_threads->setCondition_parameter($parameter = array(
                                array('WHERE', 'forum_subcat_fsubcat_id', '=', $fsubcats['fsubcat_id'])
                            ));
                            $select_threads->setCondition_order($order = array('forum_id', 'DESC'));
                            $select_threads->setCondition_limit(1);
                            $result_select_threads = $select_threads->select_forum();
                            // Count om der er nogen tråede oprettet (Hvis ikke smid en besked om der ikke er nogen tråede ellers loop sidste oprettet tråd ud.)
                            $count_select_threads = mysqli_num_rows($result_select_threads);
                            if ($count_select_threads == 0) {
                                echo '<td class="warning">Ingen Tråde oprettet</td>';
                                echo '<td class="warning"></td>';
                            } else {
                                while ($row_select_thread = $result_select_threads->fetch_object()) {
                                    $date = new DateTime($row_select_thread->forum_time);
                                    if ($row_select_thread->forum_users_id == NULL) {
                                        $name = 'Brugeren er slettet';
                                    }else{
                                        $name = $row_select_thread->users_username;
                                    }
                                    ?>
                                    <td class="active"><a href="?frontend_page=front_forum_one&forum_id=<?php echo $row_select_thread->forum_id ?>"><?php echo $row_select_thread->forum_headline ?></a></td>
                                    <td class="active"><i><?php echo $date->format('d-m-Y H:i') ?> af: <a href="#"><?php echo $name ?></a></td>
                                    <?php
                                }
                            }
                            ?>

                            <!-- admin del -->

                            <?php
                            if (in_array('Guest', $users_rank->getRolesRank())) {

                                echo '<td class="active"><a href="?frontend_page=show_threads&id=' . $fsubcats['fsubcat_id'] . '">' . '<span class="glyphicon glyphicon-list"></span></a></td>';
                            } else {
                                ?>
                                <!--Ny tråd i denne underkategori-->
                                <td class="active">
                                    <a href="?frontend_page=front_forum_create_thread&fsubcat_id=<?php echo $fsubcats['fsubcat_id'] ?>"<span class="glyphicon glyphicon-pencil"></span></a>
                                    <!--Se alle tråde i denne underkategori-->
                                    <?php
                                    echo '<a href="?frontend_page=show_threads&id=' . $fsubcats['fsubcat_id'] . '">' . '<span class="glyphicon glyphicon-list"></span></a>';
                                    echo "</td>";
                                    if (!in_array('User', $users_rank->getRolesRank())) {
                                        ?>
                                        <!-- Her kan indsættes hvad administratoren skal kunne -->
                                        <?php
                                    }
                                }
                                ?>
                            </td><!-- slut admin del -->
                        </tr>
                    </tbody>
                    <?php
                }
                ?>
            </table>
            <?php
        }
        ?>
    </div>
</div><!--end row -->