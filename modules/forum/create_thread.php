<?php
$new_thread = new Forum('forum');
if (isset($_POST['submit_thread'])) {
    // valider indlægget
    $validering = new Validering();
    $validering->checkMinLength($_POST['forum_headline'], 5);
    $validering->checkMaxLength($_POST['forum_headline'], 59);
    if ($fejl = $validering->getFejl() == false) {

        $new_thread->setForum_headline($_POST['forum_headline']);
        $new_thread->setForum_text($_POST['forum_text']);
        $new_thread->setForum_users_id($_SESSION['users_id']);
        $new_thread->setForum_subcat_fsubcat_id($_GET['fsubcat_id']);

        // tableoption sender data til classen
        $new_thread->tableoption(1);
        $last_insertid = $new_thread->insert_forum();
        if (!$last_insertid == '') {
            userRedirect("?frontend_page=front_forum_one&forum_id=$last_insertid");
        } else {
            echo '<div class="alert alert-danger">Det lader til noget gik galt. Prøv igen eller kontakt administratoren!</div>';
        }
    } else {
        echo '<div class="alert alert-danger">Det lader til noget gik galt. Prøv igen eller kontakt administratoren!</div>';
    }
}
?>

<div class="col-md-12">
    <form class="form-horizontal" role="form" method="POST" action="">
        <div class="form-group">
            <label for="forum_headline">Indlæggets navn</label>
            Max (<span id="maxlength">50</span> tegn tilbage)
            <input id="area" class="form-control" name="forum_headline" type="Text" size="50" maxlength="50">
        </div>

        <div class="form-group">
            <label for="forum_text">Dit indlæg</label><br />
            <textarea class="mceEditor" name="forum_text" placeholder="Din tekst" cols="50" rows="5"></textarea><br />
            <button type="submit" name="submit_thread" class="btn btn-default">Send</button>
        </div>
    </form>
</div> 
