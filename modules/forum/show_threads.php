<?php
if (isset($_GET['action']) && $_GET['action'] == 'delete') {
    $delete_thread = new Forum('forum');
    $delete_thread->setCondition_field('forum_id');
    $delete_thread->setCondition_operator('=');
    $delete_thread->setCondition_value($_GET['threadid']);
    if ($delete_thread->delete_forum() == true) {
        $id = $_GET['id'];
        userRedirect("?frontend_page=show_threads&id=$id&delete=succes");
    }
}
if (isset($_GET['delete']) && $_GET['delete'] == 'succes') {
    echo '<div class="alert alert-success">Tråden er nu slettet</div>';
}

// udtrækker subcat headline
$show_subcat = new Forum('forum_subcat');
$show_subcat->setCondition_parameter($parameter = array(
    array('WHERE', 'fsubcat_id', '=', $_GET['id'])
));
$result_show_subcat = $show_subcat->select_forum();
$row_show_subcat = $result_show_subcat->fetch_object();

// udtr�kker tr�dende der h�rer til den p�g�ldende subcat
$show_threads = new Forum('forum_has_subcats_and_users');

$show_threads->setCondition_parameter($parameter = array(
    array('WHERE', 'fsubcat_id', '=', $_GET['id'])
));
$show_threads->setCondition_order($order = array('forum_id', 'DESC'));

$result_show_threads = $show_threads->select_forum();
?>

<h1><?php echo $row_show_subcat->fsubcat_name ?></h1>
<table class="table table-bordered table-stripped">

    <?php if (in_array('Guest', $users_rank->getRolesRank())) {
        ?> 
        <!-- hvis man er gæst -->
        <?php
    } else {
        ?>
        <!-- Hvis man er bruger -->
        <a href="?frontend_page=front_forum_create_thread&fsubcat_id=<?php echo $row_show_subcat->fsubcat_id ?>"<button class="btn btn-success pull-right">Skriv ny</button></a><br /><br />

        <?php
        if (!in_array('User', $users_rank->getRolesRank())) {
            ?>
            <!--Hvis man er admin-->
            <?php
        }
    }
    ?>

    <thead>
        <tr>
            <th class="info">Tråde</th>
            <th class="info">Forfatter</th>
            <th class="info">tidspunkt</th>
            <th class="info">Kommentarer</th>

            <?php if (in_array('Guest', $users_rank->getRolesRank())) {
                ?> 
                <!-- hvis man er gæst -->
                <?php
            } else {
                ?>
                <!-- Hvis man er bruger -->
                <?php
                if (!in_array('User', $users_rank->getRolesRank())) {
                    ?>
                    <!--Hvis man er admin-->
                    <th class="info" style="width: 34px;"><span class="glyphicon glyphicon-trash"></span></th>
                    <?php
                }
            }
            ?>


        </tr>
    </thead>
    <tbody>
        <?php
        while ($row_show_threads = $result_show_threads->fetch_object()) {

            // skal tælle antal af svar på de enkelte tråde! (mangler af lave view)
            $count_answers = new Forum('forum_comments');
            $count_answers->setCondition_parameter($parameter = array(
                array('WHERE', 'com_forum_id', '=', $row_show_threads->forum_id)
            ));

            $result_count_answers = $count_answers->select_forum();
            $num_row = mysqli_num_rows($result_count_answers);

            // for at bruge dato og tid
            $date = new DateTime($row_show_threads->forum_time);

            if ($row_show_threads->forum_users_id == NULL) {
                $name = 'Brugeren er slettet';
            } else {
                $name = $row_show_threads->users_username;
            }
            ?>
            <tr>
                <td class="active"><a href="?frontend_page=front_forum_one&forum_id=<?php echo $row_show_threads->forum_id; ?>"><?php echo $row_show_threads->forum_headline ?></a></td>
                <td class="active"><a href="#"><?php echo $name; ?></a></td>
                <td class="active"><i><?php echo $date->format('d-m-Y H:i') ?></i></td>
                <td class="active"><?php echo $num_row; ?></td>


                <?php if (in_array('Guest', $users_rank->getRolesRank())) {
                    ?> 
                    <!-- hvis man er gæst -->
                    <?php
                } else {
                    ?>
                    <!-- Hvis man er bruger -->
                    <?php
                    if (!in_array('User', $users_rank->getRolesRank())) {
                        ?>
                        <!--Hvis man er admin-->
                        <td class="danger"><a class="confirm" href="?frontend_page=show_threads&id=<?php echo $_GET['id'] ?>&action=delete&threadid=<?php echo $row_show_threads->forum_id ?>" ><span class="glyphicon glyphicon-trash"></span></a></td>
                                <?php
                            }
                        }
                        ?>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>