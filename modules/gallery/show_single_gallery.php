<?php
$show_gallery = new Gallery('gallery_has_img_and_cat');

if (isset($_GET['gallery_category_id'])) {
    $show_gallery->setCondition_parameter($parameter = array(
        array('WHERE', 'gallery_category_id', '=', $_GET['gallery_category_id'])
    ));
}
$result_show_gallery = $show_gallery->select_gallery();
while ($row_show_gallery = $result_show_gallery->fetch_object()) {
?>
    <div class="image-container">
        <div class="gallery">
            <div class="gallery-item">
                <a data-lightbox="image-1" href="images/gallery/<?php echo $row_show_gallery->gallery_images_name; ?>"><img class="img-thumbnail img-responsive" src="images/gallery/thumb_<?php echo $row_show_gallery->gallery_images_name; ?>" alt="<?php echo $row_show_gallery->gallery_images_name; ?>"/></a>
            </div>
        </div>
    </div>
<?php } ?>