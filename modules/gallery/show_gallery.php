<h2>Vælg et galleri</h2>

<?php
$show_gallery = new Gallery('gallery_category');
$result_show_gallery = $show_gallery->select_gallery();
while ($row_show_gallery = $result_show_gallery->fetch_object()) {
?>
<div class="image-container">
    <div class="well">
            <a href="?frontend_page=front_galleri_one&gallery_category_id=<?php echo $row_show_gallery->gallery_category_id; ?>"><h4><?php echo $row_show_gallery->gallery_category_name ?></h4></a> 
            <p><?php echo $row_show_gallery->gallery_category_desc ?></p>    
    </div>
</div>
<?php } ?>
