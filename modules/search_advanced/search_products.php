<?php
// instanciate class og smider tabel i classen
$products = new Products('products');

// select_cat og smid i resultat
$result_products = $products->select_products();
?>

<div class="col-md-6">
    <?php
    $class_search = new Search_products('products_has_cat_and_img');

    if (isset($_POST['submit_search'])) {
        if (empty($_POST['checkbox'])) {
            $checkmenu = '';
        } else {
            $checkmenu = $_POST['checkbox'];
        }
        $design = $_POST['design'];
        $yearmin = $_POST['yearmin'];
        $yearmax = $_POST['yearmax'];
        $pricemin = $_POST['pricemin'];
        $pricemax = $_POST['pricemax'];

        $result_search = $class_search->search($checkmenu, $design, $yearmin, $yearmax, $pricemin, $pricemax);
        $count = mysqli_num_rows($result_search);


        if ($count == 0) {
            echo 'Ingen Søge resultater';
        } else {

            while ($row_search = $result_search->fetch_object()) {
                ?>
                <a href="?frontend_page=front_one_product&id=<?php echo $row_search->products_id; ?>">
                    <img  width="120px" height="90px" src="images/<?php echo $row_search->products_cat_id ?>/<?php echo $row_search->products_image_name ?> "/>
                    <?php
                    $products_desc = substr($row_search->products_desc, 0, 183);
                    echo '<h2>' . $row_search->products_name . '</h2>';
                    echo $products_desc;
                    ?>
                </a>
                <?php
            }
        }
    } else {
        ?>
        <form class="form-horizontal" action="?frontend_page=front_one_product" method="post">
            <div class="form-group">
                <label for="varenummer">Søg på et vare nr.</label> 
                <input class="form-control" type="search" id="varenummer" name="varenummer" placeholder="Indtast varenummer" required /><br />             
                <input class="btn btn-info" type="submit" name="submit_varenummer" value="Søg" />           
            </div>
        </form>
        <hr />
        <form class="form-horizontal" action="" method="POST">
            <div class="form-group">
                <strong>Vælg en varekategori</strong>
            </div>
            <div class="checkbox-inline">
                <label class="checkbox-inline">
                    <input type="checkbox" name="checkbox[]" value="sofa"> Sofa
                </label>
                <label class="checkbox-inline">
                    <input type="checkbox" name="checkbox[]" value="sofabord"> Sofabord
                </label>
                <label class="checkbox-inline">
                    <input type="checkbox" name="checkbox[]" value="spisestol"> Spisestol
                </label>
                <label class="checkbox-inline">
                    <input type="checkbox" name="checkbox[]" value="spisebord"> Spisebord
                </label>
            </div>

            <br /><br />

            <div class="form-group">
                <label for="design">Producent</label>
                <select class="form-control" name="design">
                    <option value="">Vælg en producent</option>-->
                    <?php
                    // looper kategorier i en select/option
                    while ($row_products = $result_products->fetch_object()) {
                        ?>
                        <option value="<?php echo $row_products->products_design; ?>"><?php echo $row_products->products_design; ?> </option>
                        <?php
                    }
                    ?>
                </select>
            </div>

            <div class="form-group">
                <div class="col-sm-4">
                    <strong>Produceret år</strong>
                    <p>Mellem hvilke år ønsker du at søge? <I>fx. 1996-2014</I><p>
                </div>
                <div class="col-sm-4">
                    Min <input class="form-control" type="text" name="yearmin" placeholder="Min" /> 
                </div>
                <div class="col-sm-4">
                    Max <input class="form-control" type="text" name="yearmax" placeholder="Max" />
                </div>
            </div>
            <div class="form-group">

                <div class="col-sm-4">
                    <strong>Pris:</strong>
                    <p>Hvad må produktet min eller max koste? <I>fx. 100-2000</I><p>
                </div>          
                <div class="col-sm-4">
                    Min <input class="form-control" type="text" name="pricemin" placeholder="Min" /> 
                </div>
                <div class="col-sm-4">
                    Max <input class="form-control" type="text" name="pricemax" placeholder="Max" />
                </div>
            </div>
            <input class="btn btn-info" type="submit" name="submit_search" value="SØG" />
    </div>
    </form>
<?php } ?>
</div>