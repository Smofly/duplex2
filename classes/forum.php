<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of forum
 *
 * @author martinhartmann81
 */
class Forum extends Database {

    // Tablename: Forum

    private $forum_id;
    private $forum_headline;
    private $forum_text;
    private $forum_update_time;
    private $forum_users_id;
    private $forum_subcat_fsubcat_id;
    // Tablename Comments

    private $fcom_id;
    private $fcom_comment;
    private $com_forum_id;
    private $com_users_id;
    // Classen variabler
    private $tablename;

    public function getForum_id() {
        return $this->forum_id;
    }

    public function getForum_headline() {
        return $this->forum_headline;
    }

    public function getForum_text() {
        return $this->forum_text;
    }

    public function getForum_update_time() {
        return $this->forum_update_time;
    }

    public function getForum_users_id() {
        return $this->forum_users_id;
    }

    public function getForum_subcat_fsubcat_id() {
        return $this->forum_subcat_fsubcat_id;
    }

    public function getFcom_id() {
        return $this->fcom_id;
    }

    public function getFcom_comment() {
        return $this->fcom_comment;
    }

    public function getCom_forum_id() {
        return $this->com_forum_id;
    }

    public function getCom_users_id() {
        return $this->com_users_id;
    }

    public function getTablename() {
        return $this->tablename;
    }

    public function setForum_id($forum_id) {
        $this->forum_id = $forum_id;
    }

    public function setForum_headline($forum_headline) {
        $this->forum_headline = $forum_headline;
    }

    public function setForum_text($forum_text) {
        $this->forum_text = $forum_text;
    }

    public function setForum_update_time($forum_update_time) {
        $this->forum_update_time = $forum_update_time;
    }

    public function setForum_users_id($forum_users_id) {
        $this->forum_users_id = $forum_users_id;
    }

    public function setForum_subcat_fsubcat_id($forum_subcat_fsubcat_id) {
        $this->forum_subcat_fsubcat_id = $forum_subcat_fsubcat_id;
    }

    public function setFcom_id($fcom_id) {
        $this->fcom_id = $fcom_id;
    }

    public function setFcom_comment($fcom_comment) {
        $this->fcom_comment = $fcom_comment;
    }

    public function setCom_forum_id($com_forum_id) {
        $this->com_forum_id = $com_forum_id;
    }

    public function setCom_users_id($com_users_id) {
        $this->com_users_id = $com_users_id;
    }

    public function setTablename($tablename) {
        $this->tablename = $tablename;
    }

    // Constructor 
    public function __construct($tablename) {
        $this->tablename = $tablename;
        parent::__construct();
    }

    public function tableoption($value) {
        if ($value == 1) {
            $this->setFieldnames($fieldname = array(
            'forum_headline' => $this->forum_headline,
            'forum_text' => $this->forum_text,
            'forum_update_time' => '2014-09-01 00:00:00',
            'forum_subcat_fsubcat_id' => $this->forum_subcat_fsubcat_id,
            'forum_users_id' => $this->forum_users_id
            ));
        } elseif ($value == 2) {
            $this->setFieldnames($fieldname = array(
                'fcom_comment' => $this->fcom_comment,
                'com_forum_id' => $this->com_forum_id,
                'com_users_id' => $this->com_users_id
            ));
        }
    }

// Insert Function
    public function insert_forum() {
        return parent::insert_Database($this->tablename, $this->getFieldnames());
    }

// Update Function
    public function update_forum() {
        return parent::update_Database($this->tablename, $this->getFieldnames(), $this->getCondition_field(), $this->getCondition_operator(), $this->getCondition_value());
    }

// Delete Function
    public function delete_forum() {
        return parent::delete_Database($this->tablename, $this->getCondition_field(), $this->getCondition_operator(), $this->getCondition_value());
    }

// Select Function
    public function select_forum() {
        return parent::select_All_Database($this->tablename, $this->getSelectOperator(), $this->getCondition_parameter(), $this->getGroupby_value(), $this->getCondition_order(), $this->getCondition_limit());
    }

}
