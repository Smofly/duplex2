<?php

class Newsletter extends Database {

    private $email;
    private $tablename;
    private $error = false;

// Getter
    public function getEmail() {
        return $this->email;
    }

    public function getTablename() {
        return $this->tablename;
    }

    public function getError() {
        return $this->error;
    }

// Setter
    public function setEmail($email) {
        $this->email = $email;
    }

    public function setTablename($tablename) {
        $this->tablename = $tablename;
    }

    public function setError($error) {
        $this->error = $error;
    }

    public function __construct($tablename) {
        $this->tablename = $tablename;
        parent::__construct();
    }

    /**
     * CRUD Method (Insert,Update,Select,Delete)
     */
    public function tableoption($value) {
        if ($value == 1) {
            $this->setFieldnames($fieldname = array(
                'newsletter_email' => $this->email,
            ));
        }
    }

// Insert Function
    public function insert_newsletter() {
        return parent::insert_Database($this->tablename, $this->getFieldnames());
    }

// Update Function
    public function update_newsletter() {
        return parent::update_Database($this->tablename, $this->getFieldnames(), $this->getCondition_field(), $this->getCondition_operator(), $this->getCondition_value());
    }

// Delete Function
    public function delete_newsletter() {
        return parent::delete_Database($this->tablename, $this->getCondition_field(), $this->getCondition_operator(), $this->getCondition_value());
    }

// Select Function
    public function select_newsletter() {
         return parent::select_All_Database($this->tablename, $this->getSelectOperator(), $this->getCondition_parameter(), $this->getGroupby_value(), $this->getCondition_order(), $this->getCondition_limit());
    }

}
