<?php

class Search extends Database {

    private $tablename;
    
public function __construct($tablename) {
    
    $this->tablename = $tablename;
    parent::__construct();
}
// variabel rækkefølge skal være ens men navngivningen behøver ikke
public function search($search = NULL, $select = NULL, $usersid = NULL){
    // NULL hvis form/search/select er tom 
     $sql_search = "SELECT * FROM $this->tablename where 1=1";
    // hvis select er valgt 
    if(!$select == NULL){
        $sql_search .= " AND news_events_category_cat_id = $select";
    }
    if(!$usersid == NULL){
        $sql_search .= " AND news_users_users_id = $usersid";
    }
    // hvis search feltet er skrevet i, ( LIKE Skal ligge sidst).
    if(!$search == NULL){
       $sql_search .= " AND news_headline LIKE '%$search%'";
    }
    
    return $this->objCon->query($sql_search);
    
}

} 