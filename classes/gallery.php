<?php

class Gallery extends Database {

// Tablenames from Gallery_category
    private $gallery_category_id;
    private $gallery_category_name;
    private $gallery_category_desc;
// Tablenames from gallery_images
    private $gallery_images_id;
    private $gallery_images_name;
    private $gallery_images_desc;
    private $gallery_category_gallery_category_id;
    private $tablename;

    public function getGallery_category_id() {
        return $this->gallery_category_id;
    }

    public function getGallery_category_name() {
        return $this->gallery_category_name;
    }

    public function getGallery_category_desc() {
        return $this->gallery_category_desc;
    }

    public function getGallery_images_id() {
        return $this->gallery_images_id;
    }

    public function getGallery_images_name() {
        return $this->gallery_images_name;
    }

    public function getGallery_images_desc() {
        return $this->gallery_images_desc;
    }

    public function getGallery_category_gallery_category_id() {
        return $this->gallery_category_gallery_category_id;
    }

    public function getTablename() {
        return $this->tablename;
    }

    public function setGallery_category_id($gallery_category_id) {
        $this->gallery_category_id = $gallery_category_id;
    }

    public function setGallery_category_name($gallery_category_name) {
        $this->gallery_category_name = $gallery_category_name;
    }

    public function setGallery_category_desc($gallery_category_desc) {
        $this->gallery_category_desc = $gallery_category_desc;
    }

    public function setGallery_images_id($gallery_images_id) {
        $this->gallery_images_id = $gallery_images_id;
    }

    public function setGallery_images_name($gallery_images_name) {
        $this->gallery_images_name = $gallery_images_name;
    }

    public function setGallery_images_desc($gallery_images_desc) {
        $this->gallery_images_desc = $gallery_images_desc;
    }

    public function setGallery_category_gallery_category_id($gallery_category_gallery_category_id) {
        $this->gallery_category_gallery_category_id = $gallery_category_gallery_category_id;
    }

    public function setTablename($tablename) {
        $this->tablename = $tablename;
    }

    public function __construct($tablename) {
        $this->tablename = $tablename;
        parent::__construct();
    }

    public function tableoption($value) {
        if ($value == 1) {
            $this->setFieldnames($fieldname = array(
                'gallery_category_name' => $this->gallery_category_name,
                'gallery_category_desc' => $this->gallery_images_desc
            ));
        } elseif ($value == 2) {
            $this->setFieldnames($fieldname = array(
                'gallery_images_name' => $this->gallery_images_name,
                'gallery_images_desc' => $this->gallery_images_desc,
                'gallery_category_gallery_category_id' => $this->gallery_category_gallery_category_id
            ));
        }
    }

// Insert Function
    public function insert_gallery() {
        return parent::insert_Database($this->tablename, $this->getFieldnames());
    }

// Update Function
    public function update_gallery() {
        return parent::update_Database($this->tablename, $this->getFieldnames(), $this->getCondition_field(), $this->getCondition_operator(), $this->getCondition_value());
    }

// Delete Function
    public function delete_gallery() {
        return parent::delete_Database($this->tablename, $this->getCondition_field(), $this->getCondition_operator(), $this->getCondition_value());
    }

// Select Function
    public function select_gallery() {
        return parent::select_All_Database($this->tablename, $this->getSelectOperator(), $this->getCondition_parameter(), $this->getGroupby_value(), $this->getCondition_order(), $this->getCondition_limit());
    }

}
