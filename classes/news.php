<?php

class News extends Database {

    /**
     *  Var too News tabel
     */
    private $news_id;
    private $news_headline;
    private $news_text;
    private $news_update_time;
    private $news_category_cat_id;
    private $news_users_id;


    /**
     * Var too News_comments Tabel;
     */
    private $com_id;
    private $com_comment;
    private $com_news_id;
    private $com_users_id;
    
// Var too News_images Table
    private $news_images_id;
    private $news_images_name;
    private $images_news_id;

    /**
     *  Class Var;
     */
    private $tablename;

    public function getNews_id() {
        return $this->news_id;
    }

    public function getNews_headline() {
        return $this->news_headline;
    }

    public function getNews_text() {
        return $this->news_text;
    }

    public function getNews_update_time() {
        return $this->news_update_time;
    }

    public function getNews_category_cat_id() {
        return $this->news_category_cat_id;
    }

    public function getNews_users_id() {
        return $this->news_users_id;
    }

    public function getCom_id() {
        return $this->com_id;
    }

    public function getCom_comment() {
        return $this->com_comment;
    }

    public function getCom_news_id() {
        return $this->com_news_id;
    }

    public function getCom_users_id() {
        return $this->com_users_id;
    }

    public function getNews_images_id() {
        return $this->news_images_id;
    }

    public function getNews_images_name() {
        return $this->news_images_name;
    }

    public function getImages_news_id() {
        return $this->images_news_id;
    }

    public function getTablename() {
        return $this->tablename;
    }

    public function setNews_id($news_id) {
        $this->news_id = $news_id;
    }

    public function setNews_headline($news_headline) {
        $this->news_headline = $news_headline;
    }

    public function setNews_text($news_text) {
        $this->news_text = $news_text;
    }

    public function setNews_update_time($news_update_time) {
        $this->news_update_time = $news_update_time;
    }

    public function setNews_category_cat_id($news_category_cat_id) {
        $this->news_category_cat_id = $news_category_cat_id;
    }

    public function setNews_users_id($news_users_id) {
        $this->news_users_id = $news_users_id;
    }

    public function setCom_id($com_id) {
        $this->com_id = $com_id;
    }

    public function setCom_comment($com_comment) {
        $this->com_comment = $com_comment;
    }

    public function setCom_news_id($com_news_id) {
        $this->com_news_id = $com_news_id;
    }

    public function setCom_users_id($com_users_id) {
        $this->com_users_id = $com_users_id;
    }

    public function setNews_images_id($news_images_id) {
        $this->news_images_id = $news_images_id;
    }

    public function setNews_images_name($news_images_name) {
        $this->news_images_name = $news_images_name;
    }

    public function setImages_news_id($images_news_id) {
        $this->images_news_id = $images_news_id;
    }

    public function setTablename($tablename) {
        $this->tablename = $tablename;
    }

    
    /**
     *  Constructor
     * @param type $tablename
     */
    public function __construct($tablename) {
        $this->tablename = $tablename;
        parent::__construct();
    }

    public function tableoption($value) {
        if ($value == 1) {
            $this->setFieldnames($fieldname = array(
                'news_headline' => $this->news_headline,
                'news_text' => $this->news_text,
                'news_update_time' => $this->news_update_time,
                'news_users_id' => $this->news_users_id,
                'news_category_cat_id' => $this->news_category_cat_id
            ));
        } elseif ($value == 2) {
            $this->setFieldnames($fieldname = array(
                'com_comment' => $this->com_comment,
                'com_news_id' => $this->com_news_id,
                'com_users_id' => $this->com_users_id
            ));
        } else if ($value == 3) {
            $this->setFieldnames($fieldname = array(
                'news_images_name' => $this->news_images_name,
                'images_news_id' => $this->images_news_id
            ));
        }
    }

// Insert Function
    public function insert_news() {
        return parent::insert_Database($this->tablename, $this->getFieldnames());
    }

// Update Function
    public function update_news() {
        return parent::update_Database($this->tablename, $this->getFieldnames(), $this->getCondition_field(), $this->getCondition_operator(), $this->getCondition_value());
    }

// Delete Function
    public function delete_news() {
        return parent::delete_Database($this->tablename, $this->getCondition_field(), $this->getCondition_operator(), $this->getCondition_value());
    }

// Select Function
    public function select_news() {
        return parent::select_All_Database($this->tablename, $this->getSelectOperator(), $this->getCondition_parameter(), $this->getGroupby_value(), $this->getCondition_order(), $this->getCondition_limit());
    }

}
