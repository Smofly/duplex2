<?php

class Users extends Database {

    private $tablename;
    private $error = false;

    /**
     * Tablename: users
     */
    private $users_id; // int(11)
    private $users_firstname; // varchar(45)
    private $users_lastname; // varchar (45)
    private $users_adress; // varchar (150)
    private $users_email; // varchar(45)
    private $users_signdate; // timestamp
    private $users_username; //varchar (45)
    private $users_password; // varchar (45)
    private $users_active; // int(11)
    private $roles_roles_id; // int(11)

    /**
     * Getter
     */

    public function getTablename() {
        return $this->tablename;
    }

    public function getError() {
        return $this->error;
    }

    public function getUsers_id() {
        return $this->users_id;
    }

    public function getUsers_firstname() {
        return $this->users_firstname;
    }

    public function getUsers_lastname() {
        return $this->users_lastname;
    }

    public function getUsers_adress() {
        return $this->users_adress;
    }

    public function getUsers_email() {
        return $this->users_email;
    }

    public function getUsers_signdate() {
        return $this->users_signdate;
    }

    public function getUsers_username() {
        return $this->users_username;
    }

    public function getUsers_password() {
        return $this->users_password;
    }

    public function getUsers_active() {
        return $this->users_active;
    }

    public function getRoles_roles_id() {
        return $this->roles_roles_id;
    }

    /**
     * Setter
     */
    public function setTablename($tablename) {
        $this->tablename = $tablename;
    }

    public function setError($error) {
        $this->error = $error;
    }

    public function setUsers_id($users_id) {
        $this->users_id = $users_id;
    }

    public function setUsers_firstname($users_firstname) {
        $this->users_firstname = $users_firstname;
    }

    public function setUsers_lastname($users_lastname) {
        $this->users_lastname = $users_lastname;
    }

    public function setUsers_adress($users_adress) {
        $this->users_adress = $users_adress;
    }

    public function setUsers_email($users_email) {
        $this->users_email = $users_email;
    }

    public function setUsers_signdate($users_signdate) {
        $this->users_signdate = $users_signdate;
    }

    public function setUsers_username($users_username) {
        $this->users_username = $users_username;
    }

    public function setUsers_password($users_password) {
        $this->users_password = md5($users_password);
    }

    public function setUsers_active($users_active) {
        $this->users_active = $users_active;
    }

    public function setRoles_roles_id($roles_roles_id) {
        $this->roles_roles_id = $roles_roles_id;
    }

    /**
     * Constructor
     */
    public function __construct($tablename) {
        $this->tablename = $tablename;
        parent::__construct();
    }

    public function tableoption($value) {
        if ($value == 1) {
            $this->setFieldnames($fieldname = array(
                'users_firstname' => $this->users_firstname,
                'users_lastname' => $this->users_lastname,
                'users_adress' => $this->users_adress,
                'users_email' => $this->users_email,
                'users_username' => $this->users_username,
                'users_password' => $this->users_password,
                'users_active' => $this->users_active,
                'roles_roles_id' => $this->roles_roles_id,
            ));
        } else if ($value == 2) {
            $this->setFieldnames($fieldname = array(
                'users_firstname' => $this->users_firstname,
                'users_lastname' => $this->users_lastname,
                'users_adress' => $this->users_adress,
                'users_email' => $this->users_email,
                'users_username' => $this->users_username,
                'users_active' => $this->users_active,
                'roles_roles_id' => $this->roles_roles_id,
            ));
        }
    }

// Insert Function
    public function insert_user() {
        return parent::insert_Database($this->tablename, $this->getFieldnames());
    }

// Update Function
    public function update_user() {
        return parent::update_Database($this->tablename, $this->getFieldnames(), $this->getCondition_field(), $this->getCondition_operator(), $this->getCondition_value());
    }

// Delete Function
    public function delete_user() {
        return parent::delete_Database($this->tablename, $this->getCondition_field(), $this->getCondition_operator(), $this->getCondition_value());
    }

// Select Function
    public function select_user() {
        return parent::select_All_Database($this->tablename, $this->getSelectOperator(), $this->getCondition_parameter(), $this->getGroupby_value(), $this->getCondition_order(), $this->getCondition_limit());
    }

    /**
     * Her er logincheck
     */
    public function logincheck() {
        $sql_username = "SELECT users_username FROM $this->tablename WHERE users_username = '$this->users_username'";
        $result_username = $this->objCon->query($sql_username);
        $num_username = mysqli_num_rows($result_username);
        if ($num_username == 0) {
            $this->error[] = 'brugernavn eller kodeord er forkert';
            return false;
        } else {
            $sql_userandpassword = "SELECT users_id, users_username, users_password, users_active, roles_roles_id FROM $this->tablename WHERE users_username = '$this->users_username' AND users_password = '$this->users_password'";
            $result_userandpassword = $this->objCon->query($sql_userandpassword);
            $num_userandpassword = mysqli_num_rows($result_userandpassword);
            if ($num_userandpassword == 0) {
                $this->error[] = 'brugernavn eller kodeord er forkert';

                return false;
            } else {
                $row_users = $result_userandpassword->fetch_object();

                if ($row_users->users_active != 1) {
                    $this->error[] = 'Brugeren er ikke aktiveret (Se din email indbakke for aktiverings link, husk evt. din spam indbakke)';
                    return false;
                } else {
                    $_SESSION['users_rank'] = $row_users->roles_roles_id;
                    $_SESSION['users_id'] = $row_users->users_id;
                    return true;
                }
            }
        }
    }

    public function calculate($value) {
        if ($value == 7) {
            return true;
        } else {
            return false;
        }
    }

    public function logout() {
        session_destroy();
        return true;
    }

    function redirect($loc) {
        echo "<script>window.location.href='" . $loc . "'</script>";
    }

}
