<?php

class Products extends Database {

    /**
     *  Var too products tabel
     */
    private $products_id;
    private $products_name;
    private $products_nr;
    private $products_year;
    private $products_price;
    private $products_desc;
    private $products_design;
    private $products_max;
    private $products_products_category_products_cat_id;
    private $products_products_image_products_image_id;

    /**
     *  Class Var;
     */
    private $tablename;

    /**
     * GETTERS
     */

    /**
     * @return mixed
     */
    public function getProducts_max() {
        return $this->products_max;
    }

    public function getProductsDesc() {
        return $this->products_desc;
    }

    /**
     * @return mixed
     */
    public function getProductsDesign() {
        return $this->products_design;
    }

    /**
     * @return mixed
     */
    public function getProductsId() {
        return $this->products_id;
    }

    /**
     * @return mixed
     */
    public function getProductsNr() {
        return $this->products_nr;
    }

    /**
     * @return mixed
     */
    public function getProductsPrice() {
        return $this->products_price;
    }

    /**
     * @return mixed
     */
    public function getProductsName() {
        return $this->products_name;
    }

    /**
     * @return mixed
     */
    public function getProductsProductsCategoryProductsCatId() {
        return $this->products_products_category_products_cat_id;
    }

    /**
     * @return mixed
     */
    public function getProductsProductsImageProductsImageId() {
        return $this->products_products_image_products_image_id;
    }

    /**
     * @return mixed
     */
    public function getProductsYear() {
        return $this->products_year;
    }

    /**
     * @return mixed
     */
    public function getTablename() {
        return $this->tablename;
    }

    /**
     *  SETTERS
     */

    /**
     * @param mixed $products_desc
     */
    public function setProductsDesc($products_desc) {
        $this->products_desc = $products_desc;
    }
    
    public function setProducts_max($products_max) {
        $this->products_max = $products_max;
    }

    
    /**
     * @param mixed $products_design
     */
    public function setProductsDesign($products_design) {
        $this->products_design = $products_design;
    }

    /**
     * @param mixed $products_nr
     */
    public function setProductsNr($products_nr) {
        $this->products_nr = $products_nr;
    }

    /**
     * @param mixed $products_id
     */
    public function setProductsId($products_id) {
        $this->products_id = $products_id;
    }

    /**
     * @param mixed $products_price
     */
    public function setProductsPrice($products_price) {
        $this->products_price = $products_price;
    }

    /**
     * @param mixed $products_products_category_products_cat_id
     */
    public function setProductsProductsCategoryProductsCatId($products_products_category_products_cat_id) {
        $this->products_products_category_products_cat_id = $products_products_category_products_cat_id;
    }

    /**
     * @param mixed $products_products_image_products_image_id
     */
    public function setProductsProductsImageProductsImageId($products_products_image_products_image_id) {
        $this->products_products_image_products_image_id = $products_products_image_products_image_id;
    }

    /**
     * @param mixed $products_year
     */
    public function setProductsYear($products_year) {
        $this->products_year = $products_year;
    }

    /**
     * @param mixed $tablename
     */
    public function setTablename($tablename) {
        $this->tablename = $tablename;
    }

    /**
     * @param mixed $products_name
     */
    public function setProductsName($products_name) {
        $this->products_name = $products_name;
    }

    /**
     *  Constructor
     * @param type $tablename
     */
    public function __construct($tablename) {
        $this->tablename = $tablename;
        parent::__construct();
    }

    public function tableoption($value) {
        if ($value == 1) {
            $this->setFieldnames($fieldname = array(
                'products_name' => $this->products_name,
                'products_nr' => $this->products_nr,
                'products_year' => $this->products_year,
                'products_price' => $this->products_price,
                'products_desc' => $this->products_desc,
                'products_design' => $this->products_design,
                'products_max' => $this->products_max,
                'products_category_products_cat_id' => $this->products_products_category_products_cat_id
            ));
        }
    }

// Insert Function
    public function insert_products() {
        return parent::insert_Database($this->tablename, $this->getFieldnames());
    }

// Update Function
    public function update_products() {
        return parent::update_Database($this->tablename, $this->getFieldnames(), $this->getCondition_field(), $this->getCondition_operator(), $this->getCondition_value());
    }

// Delete Function
    public function delete_products() {
        return parent::delete_Database($this->tablename, $this->getCondition_field(), $this->getCondition_operator(), $this->getCondition_value());
    }

// Select Function
    public function select_products() {
        return parent::select_All_Database($this->tablename, $this->getSelectOperator(), $this->getCondition_parameter(), $this->getGroupby_value(), $this->getCondition_order(), $this->getCondition_limit());
    }

}
