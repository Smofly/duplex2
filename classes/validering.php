<?php

class Validering {

    private $error = false;
    

    /**
     * metoden validerer heltal (int) inden for en range (ikke chars)
     * 
     * @param type $variable
     * @param int $min
     * @param int $max
     * @param string $fejlbesked
     * @return boolean/array
     */
    public function checkInt($variable, $minRange, $maxRange, $fejlbesked = 'Der skete en fejl') {
        $int_options = array("options" =>
            array("min_range" => $minRange, "max_range" => $maxRange));

        if (filter_var($variable, FILTER_VALIDATE_INT, $int_options)) {
            return $variable;
        } else {
            $this->error[] = $fejlbesked;
            return false;
        }
    }

    /**
     * Metoden validerer om Emailen er gyldig
     * 
     * @param type $variable
     * @param type $fejlbesked
     * @return boolean
     */
    public function checkEmail($variable, $fejlbesked = 'Der skete en fejl') {
        if (!filter_var($variable, FILTER_VALIDATE_EMAIL)) {
            $this->error[] = $fejlbesked;
            return false;
        } else {
            return $variable;
        }
    }

    /**
     * Metoden validerer om talet er en Decimal tal (skal konventeres før kan blive lagt i databasse)
     * 
     * @param type $variable
     * @param type $fejlbesked
     * @return boolean
     */
    public function checkFloat($variable, $fejlbesked = 'Der skete en fejl') {
        if (!filter_var($variable, FILTER_VALIDATE_FLOAT, array("options" => array('decimal' => ',')))) {
            return $variable;
        } else {
            $this->error[] = $fejlbesked;
            return false;
        }
    }

    /**
     * Metoden Validerer om variablen er støre end Maxlength
     * 
     * @param type $variable
     * @param type $maxlength
     * @param type $fejlbesked
     * @return boolean
     */
    public function checkMaxLength($variable, $maxlength, $fejlbesked = 'Der skete en fejl') {
        if (strlen($variable) < $maxlength) {
            return $variable;
        } else {
            $this->error[] = $fejlbesked;
            return false;
        }
    }

    /**
     * Metoden Validerer om variablen er mindre end minLength
     * 
     * @param type $variable
     * @param type $minlength
     * @param type $fejlbesked
     * @return boolean
     */
    public function checkMinLength($variable, $minlength, $fejlbesked = 'Der skete en fejl') {

        if (strlen($variable) >= $minlength) {
            return $variable;
        } else {
            $this->error[] = $fejlbesked;
            return false;
        }
    }

    /**
     * Metoden validerer om en url er en gyldig url
     * 
     * @param type $variable
     * @param type $fejlbesked
     * @return boolean
     */
    public function checkUrl($variable, $fejlbesked = 'Der skete en fejl') {
        if (!filter_var($variable, FILTER_VALIDATE_URL)) {
            $this->error[] = $fejlbesked;
            return false;
        } else {
            return $variable;
        }
    }

    /**
     * Metoden validerer om variablen er kun bogstaver
     * 
     * @param type $variable
     * @param type $fejlbesked
     * @return boolean
     */
    public function checkName($variable, $fejlbesked = 'Der skete en fejl') {
        if (!preg_match("/^[a-üA-Ü]+$/", $variable)) {
            $this->error[] = $fejlbesked;
            return false;
        } else {
            return $variable;
        }
    }

    public function checkPassword($variable, $fejlbesked = 'Der skete en fejl') {
        if (!preg_match("/^[a-üA-Ü0-9]+$/", $variable)) {
            $this->error[] = $fejlbesked;
            return false;
        } else {
            return $variable;
        }
    }

    /**
     * Metoden validerer om stringen er en Boolan
     * 
     * @param type $variable
     * @param type $fejlbesked
     * @return boolean
     */
    public function checkBoolan($variable, $fejlbesked = 'Der skete en fejl') {
        if (!filter_var($variable, FILTER_VALIDATE_BOOLEAN)) {
            $this->error[] = $fejlbesked;
            return false;
        } else {
            return $variable;
        }
    }

    /**
     * Metoden validerer om navn Indholder 2 strings
     * 
     * @param type $variable
     * @param type $fejlbesked
     * @return boolean
     */
    public function checkNameSurname($variable, $fejlbesked = 'Der skete en fejl') {
        if (strpos($variable, " ") == false) {
            $this->error[] = $fejlbesked;
            return false;
        } else {
            return $variable;
        }
    }

    /**
     * Metoden  Validerer om Variable1 er = med variable2
     * 
     * @param type $variable1
     * @param type $variable2
     * @param type $fejlbesked
     * @return boolean
     */
    public function checkMatch($variable1, $variable2, $fejlbesked = 'Der skete en fejl') {
        if ($variable1 == $variable2) {
            return $variable1;
        } else {
            $this->error[] = $fejlbesked;
            return false;
        }
    }

    /**
     * Metoden Validerer om Adresse indholder Adresse + Hus nummer
     * 
     * @param type $variable
     * @param type $fejlbesked
     * @return boolean
     */
    public function checkAdresse($variable, $fejlbesked = 'Der skete en fejl') {
        if (preg_match('/[" "]+[0-9]+/', $variable) == false) {
            $this->error[] = $fejlbesked;
            return false;
        } else {
            return $variable;
        }
    }

    public function checkDate($variable, $fejlbesked = 'Der skete en fejl') {
        if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $variable) == false) {
            $this->error[] = $fejlbesked;
            return false;
        } else {
            return $variable;
        }
    }

    public function checknumeric($variable, $fejlbesked = 'Der skete en fejl') {
        if (is_numeric($variable) == false) {
            $this->error[] = $fejlbesked;
            return false;
        } else {
            return $variable;
        }
    }

    /**
     * HUSK at denne returnerer true hvis der er fejl
     * @return bool (array)
     */
    public function getFejl() {
        return $this->error;
    }

}
