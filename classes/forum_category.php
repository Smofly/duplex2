<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of forum_category
 *
 * @author martinhartmann81
 */
class Forum_category extends Database {

    // Tablename Forum_category
    private $fcat_id;
    private $fcat_name;
    // Tablename Forum_subcatgory
    private $fsubcat_id;
    private $fsubcat_name;
    private $fsubcat_desc;
    private $forum_category_fcat_id;
    // Class variabler
    private $tablename;

    public function __construct($tablename) {
        $this->tablename = $tablename;
        parent::__construct();
    }

    // Getter
    public function getFcat_id() {
        return $this->fcat_id;
    }

    public function getFcat_name() {
        return $this->fcat_name;
    }

    public function getFsubcat_id() {
        return $this->fsubcat_id;
    }

    public function getFsubcat_name() {
        return $this->fsubcat_name;
    }

    public function getFsubcat_desc() {
        return $this->fsubcat_desc;
    }

    public function getForum_category_fcat_id() {
        return $this->forum_category_fcat_id;
    }

    public function getTablename() {
        return $this->tablename;
    }

    // Setter
    public function setFcat_id($fcat_id) {
        $this->fcat_id = $fcat_id;
    }

    public function setFcat_name($fcat_name) {
        $this->fcat_name = $fcat_name;
    }

    public function setFsubcat_id($fsubcat_id) {
        $this->fsubcat_id = $fsubcat_id;
    }

    public function setFsubcat_name($fsubcat_name) {
        $this->fsubcat_name = $fsubcat_name;
    }

    public function setFsubcat_desc($fsubcat_desc) {
        $this->fsubcat_desc = $fsubcat_desc;
    }

    public function setForum_category_fcat_id($forum_category_fcat_id) {
        $this->forum_category_fcat_id = $forum_category_fcat_id;
    }

    public function setTablename($tablename) {
        $this->tablename = $tablename;
    }

    public function tableoption($value) {
        if ($value == 1) {
            $this->setFieldnames($fieldname = array(
                'fcat_name' => $this->fcat_name,
            ));
        } elseif ($value == 2) {
            $this->setFieldnames($fieldname = array(
                'fsubcat_name' => $this->fsubcat_name,
                'fsubcat_desc' => $this->fsubcat_desc,
                'forum_category_fcat_id' => $this->forum_category_fcat_id
            ));
        }
    }

// Insert Function
    public function insert_forum() {
        return parent::insert_Database($this->tablename, $this->getFieldnames());
    }

// Update Function
    public function update_forum() {
        return parent::update_Database($this->tablename, $this->getFieldnames(), $this->getCondition_field(), $this->getCondition_operator(), $this->getCondition_value());
    }

// Delete Function
    public function delete_forun() {
        return parent::delete_Database($this->tablename, $this->getCondition_field(), $this->getCondition_operator(), $this->getCondition_value());
    }

// Select Function
    public function select_forum() {
        return parent::select_All_Database($this->tablename, $this->getSelectOperator(), $this->getCondition_parameter(), $this->getGroupby_value(), $this->getCondition_order(), $this->getCondition_limit());
    }

    function redirect($loc) {
        echo "<script>window.location.href='" . $loc . "'</script>";
    }

}
