<?php

/**
 * Class Roles
 */
class Roles extends Database {

    // Tablename
    private $tablename;
    // Tablename roles

    private $roles_id;
    private $roles_rank;
    private $userrank;

    /**
     * @param mixed $tablename
     */
    public function setTablename($tablename) {
        $this->tablename = $tablename;
    }

    /**
     * @return mixed
     */
    public function getTablename() {
        return $this->tablename;
    }

    /**
     * @param mixed $roles_rank
     */
    public function setRolesRank($roles_rank) {
        $this->roles_rank = $roles_rank;
    }

    /**
     * @return mixed
     */
    public function getRolesRank() {
        return $this->roles_rank;
    }

    /**
     * @param mixed $roles_id
     */
    public function setRolesId($roles_id) {
        $this->roles_id = $roles_id;
    }

    /**
     * @return mixed
     */
    public function getRolesId() {
        return $this->roles_id;
    }

    /**
     * @param $tablename
     * @param bool $userrank
     */
    public function __construct($tablename, $userrank = NULL) {
        if ($userrank == NULL) {
            $this->userrank = 1;
        } else {
            $this->userrank = $userrank;
        }
        $this->tablename = $tablename;
        parent::__construct();

        $select_roles = "SELECT * FROM $this->tablename";

        if (!$this->userrank == false AND is_numeric($this->userrank)) {
            $select_roles .= " where roles_id = $this->userrank";
        }
        $result_roles = $this->objCon->query($select_roles);
        while ($row_roles = $result_roles->fetch_assoc()) {
            $this->roles_rank[] = $row_roles['roles_rank'];
        }
    }

}
