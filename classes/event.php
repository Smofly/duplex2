<?php

class Event extends Database {

// Classes variables
    private $tablename;
// tablename: Event
    private $event_id;
    private $event_name;
    private $event_desc;
    private $event_startdate;
    private $event_enddate;
    private $event_tilmelding;
    private $event_img;
// foreign keys
    private $news_events_category_cat_id;
    private $users_users_id;

    public function __construct($tablename) {
        $this->tablename = $tablename;
        parent::__construct();
        $this->tableoption();
    }

//Getter
    public function getTablename() {
        return $this->tablename;
    }

    public function getEvent_id() {
        return $this->event_id;
    }

    public function getEvent_name() {
        return $this->event_name;
    }

    public function getEvent_desc() {
        return $this->event_desc;
    }

    public function getEvent_startdate() {
        return $this->event_startdate;
    }

    public function getEvent_enddate() {
        return $this->event_enddate;
    }

    public function getEvent_tilmelding() {
        return $this->event_tilmelding;
    }

    public function getEvent_img() {
        return $this->event_img;
    }

    public function getNews_events_category_cat_id() {
        return $this->news_events_category_cat_id;
    }

    public function getUsers_users_id() {
        return $this->users_users_id;
    }

// Setter
    public function setTablename($tablename) {
        $this->tablename = $tablename;
    }

    public function setEvent_id($event_id) {
        $this->event_id = $event_id;
    }

    public function setEvent_name($event_name) {
        $this->event_name = $event_name;
    }

    public function setEvent_desc($event_desc) {
        $this->event_desc = $event_desc;
    }

    public function setEvent_startdate($event_startdate) {
        $this->event_startdate = $event_startdate;
    }

    public function setEvent_enddate($event_enddate) {
        $this->event_enddate = $event_enddate;
    }

    public function setEvent_tilmelding($event_tilmelding) {
        $this->event_tilmelding = $event_tilmelding;
    }

    public function setEvent_img($event_img) {
        $this->event_img = $event_img;
    }

    public function setNews_events_category_cat_id($news_events_category_cat_id) {
        $this->news_events_category_cat_id = $news_events_category_cat_id;
    }

    public function setUsers_users_id($users_users_id) {
        $this->users_users_id = $users_users_id;
    }

// Table Option (to insert/update)
    public function tableoption($value) {
        if ($value == 1) {
            $this->setFieldnames($fieldname = array(
                'event_name' => $this->event_name,
                'event_desc' => $this->event_desc,
                'event_startdate' => $this->event_startdate,
                'event_enddate' => $this->event_enddate,
                'event_tilmelding' => $this->event_tilmelding,
                'event_img' => $this->event_img,
                'news_events_category_cat_id' => $this->news_events_category_cat_id,
                'users_users_id' => $this->users_users_id
            ));
        }
    }

// Insert Function
    public function insert_event() {
        return parent::insert_Database($this->tablename, $this->getFieldnames());
    }

// Update Function
    public function update_event() {
        return parent::update_Database($this->tablename, $this->getFieldnames(), $this->getCondition_field(), $this->getCondition_operator(), $this->getCondition_value());
    }

// Delete Function
    public function delete_event() {
        return parent::delete_Database($this->tablename, $this->getCondition_field(), $this->getCondition_operator(), $this->getCondition_value());
    }

// Select Function
    public function select_event() {
        return parent::select_All_Database($this->tablename, $this->getSelectOperator(), $this->getCondition_parameter(), $this->getGroupby_value(), $this->getCondition_order(), $this->getCondition_limit());
    }

}
