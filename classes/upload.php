<?php

class Upload {

    public $dir;
    public $allowed_file_type;
    public $file_size;
    public $file_name;

    public function __construct($dir, $allowed_file_type = false, $file_size = 2097152) {

        $this->dir = $dir;

        $this->file_size = $file_size;

        if ($allowed_file_type == false) {
            $this->allowed_file_type = array("image/jpg", "image/jpeg", "image/gif", "image/png");
        } else {
            $this->allowed_file_type = $allowed_file_type;
        }
    }

    /**
     * Genererer et skaleret thumbnail af et billede - bredden og hoejden styres af maxVaerdier
     *
     * @param string $navn
     * @param int $maxBredde
     * @param int $maxHoejde
     */
    public function lavProportionalThumb($navn, $maxBredde = '100', $maxHoejde = '100') {
        $this->egenskaber = @getimagesize($this->dir . $navn);
        //print_r($this->egenskaber);
        //udtrækker højde og bredde
        $width = $this->egenskaber[0];
        $height = $this->egenskaber[1];

        //finder typen jpg gif png
        $type = $this->egenskaber[2];


        if ($width >= $height) {
            $newWidth = floor($width * ($maxBredde / $width));
            $newHeight = floor($height * ($maxBredde / $width));
        } else {
            $newHeight = floor($height * ($maxHoejde / $height));
            $newWidth = floor($width * ($maxBredde / $width));
        }
        $nytNavn = $this->dir . "thumb_" . $navn;
        copy($this->dir . $navn, $nytNavn);
        $thumbnail = imagecreatetruecolor($newWidth, $newHeight);

        //gif
        if ($type == 1) {
            $billede = imagecreatefromgif($nytNavn);
            $billedstr = getimagesize($nytNavn);
            imagecopyresampled($thumbnail, $billede, 0, 0, 0, 0, $newWidth, $newHeight, $billedstr[0], $billedstr[1]);
            imagegif($thumbnail, $nytNavn);
        }

        //jpegbillede
        if ($type == 2) {
            $billede = imagecreatefromjpeg($nytNavn);
            $billedstr = getimagesize($nytNavn);
            imagecopyresampled($thumbnail, $billede, 0, 0, 0, 0, $newWidth, $newHeight, $billedstr[0], $billedstr[1]);
            imagejpeg($thumbnail, $nytNavn);
        }

        //png
        if ($type == 3) {
            $billede = imagecreatefrompng($nytNavn);
            $billedstr = getimagesize($nytNavn);
            imagecopyresampled($thumbnail, $billede, 0, 0, 0, 0, $newWidth, $newHeight, $billedstr[0], $billedstr[1]);
            imagepng($thumbnail, $nytNavn);
        }
    }

    /**
     * 
     * @param type $files
     * @param type $post
     * @return string
     */
    public function save($files, $post) {
        if (is_array($files) && is_array($post)) {

            $errors = array();

            foreach ($files['files']['tmp_name'] as $key => $tmp_name) {
                $file_name = time() . '_' . $files['files']['name'][$key];
                $file_size = $files['files']['size'][$key];
                $file_tmp = $files['files']['tmp_name'][$key];
                $file_type = $files['files']['type'][$key];

                if (!in_array($file_type, $this->allowed_file_type)) {
                    $errors[] = 'unsupported file format';
                }

                if ($file_size > $this->file_size) {
                    $errors[] = 'File size must be less than 2 MB';
                }

                if (empty($errors) == true) {

                    if (is_dir($this->dir) == false) {
                        mkdir("$this->dir", 0777);  // Create directory if it does not exist
                    }

                    if (is_dir("$this->dir/" . $file_name) == false) {
                        move_uploaded_file($file_tmp, "$this->dir/" . $file_name);
                        $this->file_name[] = $file_name;
                    }
                } else {
                    foreach ($errors as $key => $err) {
                        echo $err;
                    }
                }
            }

            return $this->file_name;
            if (empty($errors)) {
                $errors[] = "Alles klar";
            }
            return $errors;
        }
    }

    public function deleteImg($navn) {
        unlink($this->dir . $navn);
        $original = ltrim($navn, "thumb_");
        unlink($this->dir . $original);
    }

}
