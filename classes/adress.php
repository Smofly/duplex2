<?php

class Adress extends Database {

// Classes variables
    private $tablename;
// tablename: Event
    private $info_id;
    private $info_adressline_one;
    private $info_adressline_two;
    private $info_phone_one;
    private $info_phone_two;
    private $info_email;
    private $info_company;

    /**
     * @param mixed $info_adressline_one
     */
    public function setInfoAdresslineOne($info_adressline_one) {
        $this->info_adressline_one = $info_adressline_one;
    }

    /**
     * @return mixed
     */
    public function getInfoAdresslineOne() {
        return $this->info_adressline_one;
    }

    /**
     * @param mixed $info_adressline_two
     */
    public function setInfoAdresslineTwo($info_adressline_two) {
        $this->info_adressline_two = $info_adressline_two;
    }

    /**
     * @return mixed
     */
    public function getInfoAdresslineTwo() {
        return $this->info_adressline_two;
    }

    /**
     * @param mixed $info_company
     */
    public function setInfoCompany($info_company) {
        $this->info_company = $info_company;
    }

    /**
     * @return mixed
     */
    public function getInfoCompany() {
        return $this->info_company;
    }

    /**
     * @param mixed $info_email
     */
    public function setInfoEmail($info_email) {
        $this->info_email = $info_email;
    }

    /**
     * @return mixed
     */
    public function getInfoEmail() {
        return $this->info_email;
    }

    /**
     * @param mixed $info_id
     */
    public function setInfoId($info_id) {
        $this->info_id = $info_id;
    }

    /**
     * @return mixed
     */
    public function getInfoId() {
        return $this->info_id;
    }

    /**
     * @param mixed $info_phone_one
     */
    public function setInfoPhoneOne($info_phone_one) {
        $this->info_phone_one = $info_phone_one;
    }

    /**
     * @return mixed
     */
    public function getInfoPhoneOne() {
        return $this->info_phone_one;
    }

    /**
     * @param mixed $info_phone_two
     */
    public function setInfoPhoneTwo($info_phone_two) {
        $this->info_phone_two = $info_phone_two;
    }

    /**
     * @return mixed
     */
    public function getInfoPhoneTwo() {
        return $this->info_phone_two;
    }

    /**
     * @param mixed $tablename
     */
    public function setTablename($tablename) {
        $this->tablename = $tablename;
    }

    /**
     * @return mixed
     */
    public function getTablename() {
        return $this->tablename;
    }

    // Constructor
    public function __construct($tablename) {
        $this->tablename = $tablename;
        parent::__construct();
    }

    public function tableoption($value) {
        if ($value == 1) {
            $this->setFieldnames($fieldname = array(
                'info_adressline_one' => $this->info_adressline_one,
                'info_adressline_two' => $this->info_adressline_two,
                'info_phone_one' => $this->info_phone_one,
                'info_phone_two' => $this->info_phone_two,
                'info_email' => $this->info_email,
                'info_company' => $this->info_company
            ));
        }
    }

// Insert Function
    public function insert_info() {
        return parent::insert_Database($this->tablename, $this->getFieldnames());
    }

// Update Function
    public function update_info() {
        return parent::update_Database($this->tablename, $this->getFieldnames(), $this->getCondition_field(), $this->getCondition_operator(), $this->getCondition_value());
    }

// Delete Function
    public function delete_info() {
        return parent::delete_Database($this->tablename, $this->getCondition_field(), $this->getCondition_operator(), $this->getCondition_value());
    }

// Select Function
    public function select_info() {
        return parent::select_All_Database($this->tablename, $this->getSelectOperator(), $this->getCondition_parameter(), $this->getGroupby_value(), $this->getCondition_order(), $this->getCondition_limit());
    }

}
