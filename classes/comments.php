<?php

/**
 * Created by PhpStorm.
 * User: martinhartmann81
 * Date: 08/05/14
 * Time: 17.57
 */
class Comments extends Database {

    private $com_id;
    private $com_comment;
    private $com_time;
    private $com_news_id;
    private $com_users_id;
    private $tablename;

    public function getCom_id() {
        return $this->com_id;
    }

    public function getCom_comment() {
        return $this->com_comment;
    }

    public function getCom_time() {
        return $this->com_time;
    }

    public function getCom_news_id() {
        return $this->com_news_id;
    }

    public function getCom_users_id() {
        return $this->com_users_id;
    }

    public function getTablename() {
        return $this->tablename;
    }

    public function setCom_id($com_id) {
        $this->com_id = $com_id;
    }

    public function setCom_comment($com_comment) {
        $this->com_comment = $com_comment;
    }

    public function setCom_time($com_time) {
        $this->com_time = $com_time;
    }

    public function setCom_news_id($com_news_id) {
        $this->com_news_id = $com_news_id;
    }

    public function setCom_users_id($com_users_id) {
        $this->com_users_id = $com_users_id;
    }

    public function setTablename($tablename) {
        $this->tablename = $tablename;
    }

    
    /**
     *  Constructor
     * @param type $tablename
     */
    public function __construct($tablename) {
        $this->tablename = $tablename;
        parent::__construct();
    }

    public function tableoption($value) {
        if ($value == 1) {
            $this->setFieldnames($fieldname = array(
                'com_comment' => $this->com_comment,
                'com_news_id' => $this->com_news_id,
                'com_users_id' => $this->com_users_id
            ));
        }
    }

// Insert Function
    public function insert_comment() {
        return parent::insert_Database($this->tablename, $this->getFieldnames());
    }

// Update Function
    public function update_comment() {
        return parent::update_Database($this->tablename, $this->getFieldnames(), $this->getCondition_field(), $this->getCondition_operator(), $this->getCondition_value());
    }

// Delete Function
    public function delete_comment() {
        return parent::delete_Database($this->tablename, $this->getCondition_field(), $this->getCondition_operator(), $this->getCondition_value());
    }

// Select Function
    public function select_comment() {
        return parent::select_All_Database($this->tablename, $this->getSelectOperator(), $this->getCondition_parameter(), $this->getGroupby_value(), $this->getCondition_order(), $this->getCondition_limit());
    }

}
