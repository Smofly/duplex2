<?php

class Products_category extends Database {
    /**
     *  Var too products category tabel
     */
    private $products_cat_id;
    private $products_cat_name;

    /**
     *  Class Var;
     */
    private $tablename;

    /**
     * GETTERE
     */

    /**
     * @return mixed
     */
    public function getProducstCatName()
    {
        return $this->products_cat_name;
    }

    /**
     * @return mixed
     */
    public function getProductsCatId()
    {
        return $this->products_cat_id;
    }

    /**
     * @return mixed
     */
    public function getTablename()
    {
        return $this->tablename;
    }

    /**
     * SETTERE
     */

    /**
     * @param mixed $producst_cat_name
     */
    public function setProducstCatName($products_cat_name)
    {
        $this->producst_cat_name = $products_cat_name;
    }

    /**
     * @param mixed $products_cat_id
     */
    public function setProductsCatId($products_cat_id)
    {
        $this->products_cat_id = $products_cat_id;
    }

    /**
     * @param mixed $tablename
     */
    public function setTablename($tablename)
    {
        $this->tablename = $tablename;
    }

    /**
     *  Constructor
     * @param type $tablename
     */
    public function __construct($tablename) {
        $this->tablename = $tablename;
        parent::__construct();
    }

    public function tableoption($value) {
        if ($value == 1) {
            $this->setFieldnames($fieldname = array(
                'products_cat_name' => $this->products_cat_name
            ));
        }
    }

    // Insert Function
    public function insert_products_category() {
        return parent::insert_Database($this->tablename, $this->getFieldnames());
    }

// Update Function
    public function update_products_category() {
        return parent::update_Database($this->tablename, $this->getFieldnames(), $this->getCondition_field(), $this->getCondition_operator(), $this->getCondition_value());
    }

// Delete Function
    public function delete_products_category() {
        return parent::delete_Database($this->tablename, $this->getCondition_field(), $this->getCondition_operator(), $this->getCondition_value());
    }

// Select Function
    public function select_products_category() {
        return parent::select_All_Database($this->tablename, $this->getSelectOperator(), $this->getCondition_parameter(), $this->getGroupby_value(), $this->getCondition_order(), $this->getCondition_limit());
    }

}