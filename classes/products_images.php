<?php

/**
 *  Var too products images
 */

class Products_images extends Database {
    private $products_image_id;
    private $products_image_name;
    private $products_products_id;

    /**
     *  Class Var;
     */
    private $tablename;

    /**
     * GETTERS
     */

    /**
     * @return mixed
     */
    public function getProductsImageId()
    {
        return $this->products_image_id;
    }

    /**
     * @return mixed
     */
    public function getProductsImageName()
    {
        return $this->products_image_name;
    }

    /**
     * @return mixed
     */
    public function getTablename()
    {
        return $this->tablename;
    }

    /**
     * @return mixed
     */
    public function getProductsProductsId()
    {
        return $this->products_products_id;
    }


    /**
     * SETTERS
     */

    /**
     * @param mixed $products_image_id
     */
    public function setProductsImageId($products_image_id)
    {
        $this->products_image_id = $products_image_id;
    }

    /**
     * @param mixed $products_image_name
     */
    public function setProductsImageName($products_image_name)
    {
        $this->products_image_name = $products_image_name;
    }

    /**
     * @param mixed $tablename
     */
    public function setTablename($tablename)
    {
        $this->tablename = $tablename;
    }

    /**
     * @param mixed $products_products_id
     */
    public function setProductsProductsId($products_products_id)
    {
        $this->products_products_id = $products_products_id;
    }


    /**
     *  Constructor
     * @param type $tablename
     */
    public function __construct($tablename) {
        $this->tablename = $tablename;
        parent::__construct();
    }

    public function tableoption($value) {
        if ($value == 1) {
            $this->setFieldnames($fieldname = array(
                'products_image_name' => $this->products_image_name,
                'products_products_id' => $this->products_products_id
            ));
        }
    }


// Insert Function
    public function insert_products_images() {
        return parent::insert_Database($this->tablename, $this->getFieldnames());
    }

// Update Function
    public function update_products_images() {
        return parent::update_Database($this->tablename, $this->getFieldnames(), $this->getCondition_field(), $this->getCondition_operator(), $this->getCondition_value());
    }

// Delete Function
    public function delete_products_images() {
        return parent::delete_Database($this->tablename, $this->getCondition_field(), $this->getCondition_operator(), $this->getCondition_value());
    }

// Select Function
    public function select_products_images() {
        return parent::select_All_Database($this->tablename, $this->getSelectOperator(), $this->getCondition_parameter(), $this->getGroupby_value(), $this->getCondition_order(), $this->getCondition_limit());
    }

}

