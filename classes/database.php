<?php

class Database {

    protected $objCon;
    private $fieldnames;
    private $condition_parameter;
    private $selectOperator;
    private $condition_order;
    private $condition_limit;
    private $groupby_value;
    private $condition_field;
    private $condition_operator;
    private $condition_value;

// Ghetter 
    public function getObjCon() {
        return $this->objCon;
    }

    public function getFieldnames() {
        return $this->fieldnames;
    }

    public function getCondition_parameter() {
        return $this->condition_parameter;
    }

    public function getSelectOperator() {
        return $this->selectOperator;
    }

    public function getCondition_order() {
        return $this->condition_order;
    }

    public function getCondition_limit() {
        return $this->condition_limit;
    }

    public function getGroupby_value() {
        return $this->groupby_value;
    }

    public function getCondition_field() {
        return $this->condition_field;
    }

    public function getCondition_operator() {
        return $this->condition_operator;
    }

    public function getCondition_value() {
        return $this->condition_value;
    }

    // Setter
    public function setObjCon($objCon) {
        $this->objCon = $objCon;
    }

    public function setFieldnames($fieldnames) {
        $this->fieldnames = $fieldnames;
    }

    public function setCondition_parameter($condition_parameter) {
        $this->condition_parameter = $condition_parameter;
    }

    public function setSelectOperator($selectOperator) {
        $this->selectOperator = $selectOperator;
    }

    public function setCondition_order($condition_order) {
        $this->condition_order = $condition_order;
    }

    public function setCondition_limit($condition_limit) {
        $this->condition_limit = $condition_limit;
    }

    public function setGroupby_value($groupby_value) {
        $this->groupby_value = $groupby_value;
    }

    public function setCondition_field($condition_field) {
        $this->condition_field = $condition_field;
    }

    public function setCondition_operator($condition_operator) {
        $this->condition_operator = $condition_operator;
    }

    public function setCondition_value($condition_value) {
        $this->condition_value = $condition_value;
    }

// Constructer sætter forbindelsen op. samt tester om der er en forbindelse. hvis sætter den charset til utf8    
    protected function __construct() {
        $this->objCon = new mysqli("localhost", "root", "", "duplex");
        if ($this->objCon->connect_error) {
            die('kan ikke forbinde (' . $this->objCon->connect_erno . ')' . $this->objCon->connect_error);
        }
        $this->objCon->query("SET NAMES 'utf8'") or die($this->objCon->error);
    }

// Insæt i databasen
    protected function insert_Database($tablename, $fieldnames) {
        $column_name = "";
        $field_value = "";
        foreach ($fieldnames as $column_names => $field_values) {
            $field_values = mysqli_real_escape_string($this->objCon, $field_values);

            $column_name .= "$column_names, ";
            $field_value .= "'$field_values' ,";
        }
        $column_name = rtrim($column_name, ", ");
        $field_value = rtrim($field_value, ", ");
      echo  $sql_db_insert = "INSERT INTO $tablename ($column_name) VALUES ($field_value)";
        if ($this->objCon->query($sql_db_insert) == true) {
            return $this->objCon->insert_id;
        } else {
            return false;
        }
    }

// Update Databassen
    protected function update_Database($tablename, $fieldnames, $condition_field, $condition_operator, $condition_value) {
        $column_name = "";
        foreach ($fieldnames as $column_names => $field_values) {
            $field_values = mysqli_real_escape_string($this->objCon, $field_values);
            $column_name .= "$column_names = '$field_values', ";
        }
        $column_name = rtrim($column_name, ", ");

        $sql_db_update = "UPDATE $tablename SET $column_name WHERE " . $condition_field . " " . $condition_operator . " '" . $condition_value . "'";
        if ($this->objCon->query($sql_db_update) == true) {
            return true;
        } else {
            return false;
        }
    }

// Delete From Databasen
    protected function delete_Database($tablename, $condition_field, $condition_operator, $condition_value) {
        $sql_db_delete = "DELETE FROM $tablename WHERE " . $condition_field . " " . $condition_operator . " '" . $condition_value . "'";
        if ($this->objCon->query($sql_db_delete) == true) {
            return true;
        } else {
            return false;
        }
    }

// Select from Databasen
    protected function select_All_Database($tablename, $selectOperator = NULL, $condition_parameter = NULL, $groupby_value = NULL, $condition_order = NULL, $condition_limit = NULL) {
        if ($selectOperator == NULL) {
            $selectOperator = "*";
        }
        $column_name = '';
        $order = '';

        $sql_db_select = "SELECT $selectOperator FROM $tablename";

        /**
         * Select med flere parameter
         *  via F.eks 
         * $databasse->setCondition_parameter($parameter = array(array('WHERE', "newsid", '=', '"1"'),
         * array('AND', "newsid", '=', '"1"'),
         * array('AND', "newsid", '=', '"1"')));
         */
        if (!$condition_parameter == NULL) {

            foreach ($condition_parameter as $section => $value) {
                foreach ($value as $key => $value) {
                    $column_name .= " $value";
                }
            }
            $sql_db_select .= " $column_name";
        }

        if (!$groupby_value == NULL) {
            $sql_db_select .= " GROUP BY $groupby_value";
        }

        if (!$condition_order == NULL) {
            foreach ($condition_order as $order_key => $order_value) {
                $order .= " $order_value";
            }
            $sql_db_select .= " ORDER BY $order";
        }

        if (!$condition_limit == NULL) {
            $sql_db_select .= " LIMIT $condition_limit";
        }

        return $this->objCon->query($sql_db_select);
    }

}

?>