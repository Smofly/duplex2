<?php

class category extends Database {

    // Classes variables
    private $tablename;
    // Tablename: news_events_category
    private $cat_id;
    private $cat_name;
    private $cat_desc;
    private $cat_event_news;
    private $cat_small_img;
    private $cat_large_img;

    public function __construct($tablename) {
        $this->tablename = $tablename;
        parent::__construct();
    }

// tableoption (insert/update)
    public function tableoption($value) {
        if ($value == 1) {
            $this->setFieldnames($fieldname = array(
                'cat_name' => $this->cat_name,
                'cat_desc' => $this->cat_desc,
                'cat_event_news' => $this->cat_event_news,
                'cat_small_img' => $this->cat_small_img,
                'cat_large_img' => $this->cat_large_img
            ));
        }
    }

// Insert Function
    public function insert_cat() {
        return parent::insert_Database($this->tablename, $this->getFieldnames());
    }

// Update Function
    public function update_cat() {
        return parent::update_Database($this->tablename, $this->getFieldnames(), $this->getCondition_field(), $this->getCondition_operator(), $this->getCondition_value());
    }

// Delete Function
    public function delete_cat() {
        return parent::delete_Database($this->tablename, $this->getCondition_field(), $this->getCondition_operator(), $this->getCondition_value());
    }

// Select Function
    public function select_cat() {
        return parent::select_All_Database($this->tablename, $this->getSelectOperator(), $this->getCondition_parameter(), $this->getGroupby_value(), $this->getCondition_order(), $this->getCondition_limit());
    }

}
