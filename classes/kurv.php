<?php

class Kurv extends Database {

    
    
    public function __construct() {
//opretter kurv hvis der ikke er en kurv allerede
        if (!$_SESSION['kurv']) {
            $_SESSION['kurv'] = array('kurv');
            $_SESSION['kurv'] = "";
        }
        parent::__construct();
    }

//__cons

    public function putIKurv($produktID, $antal, $navn, $pris, $antalStk) {
        $nytProdukt = true;
        if (count($_SESSION['kurv']) >= 1) {
            if (!empty($_SESSION['kurv'])) {
                foreach ($_SESSION['kurv'] as $id => $produktInfo) {
//allerede oprettet produkt
                    if ($id == $produktID) {
                        $_SESSION['kurv'][$produktID] = array("antal" => $produktInfo['antal'] + $antal, "navn" => $produktInfo['navn'], "stkPris" => $produktInfo['stkPris'], "samletPris" => $produktInfo['samletPris'] + ($pris * $antal), 'antalStk' => $produktInfo['antalStk'] + $antalStk);
                        $nytProdukt = false;
                    }
                }
            }
        }

//ny i kurven
        if ($nytProdukt) {
            $_SESSION['kurv'][$produktID] = array("antal" => $antal, "navn" => $navn, "stkPris" => $pris, "samletPris" => $pris * $antal, 'antalStk' => $antalStk);
        }
    }

    public function visKurv() {
        $res = '';
        if (!empty($_SESSION['kurv'])) {
            foreach ($_SESSION['kurv'] as $id => $produktInfo) {
                $res .= 'Produktnavn: ' . $produktInfo['navn'] . '<br /> Antal: ' . $produktInfo['antal'] .
                        '<form action="" method="POST"> '
                        . '<input type="hidden" name="id" value="' . $id . '" > '
                        . '<input type="submit" name="plus" value="+" /> - '
                        . '<input type="submit" name="minus" value="-" /> - '
                        . '<input type="submit" name="slet" value="Fjern Vare" /></form><br />Pris pr. stk: ' . $produktInfo['stkPris'] . '<br />Pris i alt: ' . $produktInfo['samletPris'] . '<br/><br/>';
            }
            return $res;
        } else {
            return 'Kurven er tom!';
        }
    }

    public function plusAntal() {
        $produktID = $_POST['id'];
        if ($_SESSION['kurv'][$produktID]['antalStk'] == $_SESSION['kurv'][$produktID]['antal'] || $_SESSION['kurv'][$produktID]['antalStk'] == 0) {
            echo '<p>Du har opn&aring;et max vare</p>';
        } else {
            $navn = $_SESSION['kurv'][$produktID]['navn'];
            $stkPris = $_SESSION['kurv'][$produktID]['stkPris'];
            $antalStk = $_SESSION['kurv'][$produktID]['antalStk'] - 1;
            $this->updateAntal($produktID, $antalStk);
            $this->putIKurv($produktID, 1, $navn, $stkPris, -1);
            return true;
        }
    }

    public function minusAntal() {
        $produktID = $_POST['id'];
        if ($_SESSION['kurv'][$produktID]['antal'] == 1) {
            echo '<p>Du skal max have 1 vare</p>';
        } else {
            $navn = $_SESSION['kurv'][$produktID]['navn'];
            $stkPris = $_SESSION['kurv'][$produktID]['stkPris'];
            $antalStk = $_SESSION['kurv'][$produktID]['antalStk'] + 1;
            $this->updateAntal($produktID, $antalStk);
            $this->putIKurv($produktID, -1, $navn, $stkPris, 1);
        }
    }

    public function fjernVare() {
        $produktID = $_POST['id'];
        unset($_SESSION['kurv'][$produktID]);
    }

    public function sletKurv() {
        unset($_SESSION['kurv']);
        session_destroy();
    }

    Public function test() {
        echo "<pre>";
        print_r($_SESSION['kurv']);
        echo "</pre>";
    }

    public function countVare() {
        $antalVare = '';
        if (!empty($_SESSION['kurv'])) {
            foreach ($_SESSION['kurv'] as $key => $antalvare) {
                $antalVare = $antalVare + $antalvare['antal'];
            }
            return $antalVare;
        } else {
            return 0;
        }
    }

    public function updateAntal($id, $value) {
        $sql = "UPDATE `products` SET `products_max`= $value WHERE products_id = $id ";
        $this->objCon->query($sql);
    }

    public function samletPris() {
        $samletpris = '';
        if (!empty($_SESSION['kurv'])) {
            foreach ($_SESSION['kurv'] as $key => $kurv) {
                $samletpris = $samletpris + $kurv['samletPris'];
            }
            return number_format($samletpris, 2, ',', '');
        } else {
            return 0;
        }
    }

    Public function tjeckUd() {
        
    }

}

//$kurv = new Kurv();
//
//$kurv->putIKurv(1, 1, 'Badesalt', 22.50);
//$kurv->putIKurv(2, 2, 'Flaskepost', 50.00);
//$kurv->putIKurv(9, 1, 'Negleklipper', 75.00);
//$kurv->putIKurv(10, 10, 'Posthorn', 222.10);
//$kurv->putIKurv(1, 10, 'Badesalt', 22.50);
//
//echo $kurv->visKurv();
//$kurv->test();
//$kurv->sletKurv();
