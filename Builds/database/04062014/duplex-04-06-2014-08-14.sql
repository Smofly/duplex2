-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Vært: 127.0.0.1
-- Genereringstid: 04. 06 2014 kl. 08:14:32
-- Serverversion: 5.6.16
-- PHP-version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `duplex`
--

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `event`
--

CREATE TABLE IF NOT EXISTS `event` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_name` varchar(45) DEFAULT NULL,
  `event_desc` varchar(45) DEFAULT NULL,
  `event_startdate` datetime DEFAULT NULL,
  `event_enddate` datetime DEFAULT NULL,
  `event tilmelding` int(11) DEFAULT NULL,
  `event_img` varchar(45) DEFAULT NULL,
  `event_events_category_cat_id` int(11) NOT NULL,
  `event_users_users_id` int(11) NOT NULL,
  PRIMARY KEY (`event_id`),
  KEY `fk_event_news_events_category1_idx` (`event_events_category_cat_id`),
  KEY `fk_event_users1_idx` (`event_users_users_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `event_signup`
--

CREATE TABLE IF NOT EXISTS `event_signup` (
  `signup_id` int(11) NOT NULL AUTO_INCREMENT,
  `users_users_id` int(11) NOT NULL,
  `event_event_id` int(11) NOT NULL,
  PRIMARY KEY (`signup_id`),
  KEY `fk_event_signup_users1_idx` (`users_users_id`),
  KEY `fk_event_signup_event1_idx` (`event_event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Stand-in-struktur for visning `fcat_has_subcat`
--
CREATE TABLE IF NOT EXISTS `fcat_has_subcat` (
`fcat_id` int(11)
,`fcat_name` varchar(45)
,`fsubcat_id` int(11)
,`fsubcat_name` varchar(45)
,`fsubcat_desc` varchar(45)
,`forum_category_fcat_id` int(11)
);
-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `forum`
--

CREATE TABLE IF NOT EXISTS `forum` (
  `forum_id` int(11) NOT NULL AUTO_INCREMENT,
  `forum_headline` varchar(45) DEFAULT NULL,
  `forum_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `forum_text` text,
  `forum_update_time` datetime DEFAULT NULL,
  `users_users_id` int(11) DEFAULT NULL,
  `forum_subcat_fsubcat_id` int(11) NOT NULL,
  PRIMARY KEY (`forum_id`),
  KEY `fk_forum_users1_idx` (`users_users_id`),
  KEY `fk_forum_forum_subcat1_idx` (`forum_subcat_fsubcat_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Data dump for tabellen `forum`
--

INSERT INTO `forum` (`forum_id`, `forum_headline`, `forum_time`, `forum_text`, `forum_update_time`, `users_users_id`, `forum_subcat_fsubcat_id`) VALUES
(1, 'Dette er noget lort', '2014-04-21 00:00:00', 'Dette kommer bare ikke til at ske!!', NULL, 4, 1),
(2, 'Dette er en test', '2014-05-13 00:00:00', 'bla bla bla blabla bla bla blabla bla bla blabla bla bla blabla bla bla blabla bla bla blabla bla bla blabla bla bla blabla bla bla blabla bla bla blabla bla bla blabla bla bla blabla bla bla blabla bla bla blabla bla bla blabla bla bla blabla bla bla blabla bla bla blabla bla bla bla', NULL, 2, 1),
(9, 'dsadsad', '2014-05-25 13:21:22', 'sadasdsd', NULL, 2, 1),
(10, 'fløde', '2014-05-25 13:22:41', 'bolle', NULL, 2, 1),
(11, 'fsdfds', '2014-05-25 13:29:20', 'sdfsdfd', NULL, 2, 1);

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `forum_category`
--

CREATE TABLE IF NOT EXISTS `forum_category` (
  `fcat_id` int(11) NOT NULL AUTO_INCREMENT,
  `fcat_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`fcat_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Data dump for tabellen `forum_category`
--

INSERT INTO `forum_category` (`fcat_id`, `fcat_name`) VALUES
(1, 'Community'),
(2, 'General Forum');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `forum_comments`
--

CREATE TABLE IF NOT EXISTS `forum_comments` (
  `fcom_id` int(11) NOT NULL AUTO_INCREMENT,
  `fcom_comment` text,
  `fcom_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `forum_forum_id` int(11) NOT NULL,
  `users_users_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`fcom_id`),
  KEY `fk_forum_comments_forum1_idx` (`forum_forum_id`),
  KEY `fk_forum_comments_users1_idx` (`users_users_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Data dump for tabellen `forum_comments`
--

INSERT INTO `forum_comments` (`fcom_id`, `fcom_comment`, `fcom_time`, `forum_forum_id`, `users_users_id`) VALUES
(1, 'hah haha ', '2014-04-20 22:00:00', 1, 2),
(9, 'lolololol', '2014-05-18 11:51:22', 1, 2),
(10, 'lll\r\n', '2014-05-18 12:15:07', 2, 2),
(11, 'vfasfsf', '2014-05-25 11:23:07', 9, 2);

-- --------------------------------------------------------

--
-- Stand-in-struktur for visning `forum_comments_has_users`
--
CREATE TABLE IF NOT EXISTS `forum_comments_has_users` (
`fcom_id` int(11)
,`fcom_comment` text
,`fcom_time` timestamp
,`forum_forum_id` int(11)
,`users_users_id` int(11)
,`users_id` int(11)
,`users_firstname` varchar(45)
,`users_lastname` varchar(45)
,`users_adress` varchar(150)
,`users_email` varchar(45)
,`users_signupdate` timestamp
,`users_username` varchar(45)
,`users_password` varchar(45)
,`users_active` int(11)
,`roles_roles_id` int(11)
);
-- --------------------------------------------------------

--
-- Stand-in-struktur for visning `forum_has_subcats_and_users`
--
CREATE TABLE IF NOT EXISTS `forum_has_subcats_and_users` (
`forum_id` int(11)
,`forum_headline` varchar(45)
,`forum_time` datetime
,`forum_text` text
,`forum_update_time` datetime
,`users_users_id` int(11)
,`forum_subcat_fsubcat_id` int(11)
,`fsubcat_id` int(11)
,`fsubcat_name` varchar(45)
,`fsubcat_desc` varchar(45)
,`forum_category_fcat_id` int(11)
,`users_id` int(11)
,`users_firstname` varchar(45)
,`users_lastname` varchar(45)
,`users_adress` varchar(150)
,`users_email` varchar(45)
,`users_signupdate` timestamp
,`users_username` varchar(45)
,`users_password` varchar(45)
,`users_active` int(11)
,`roles_roles_id` int(11)
);
-- --------------------------------------------------------

--
-- Stand-in-struktur for visning `forum_has_users`
--
CREATE TABLE IF NOT EXISTS `forum_has_users` (
`forum_id` int(11)
,`forum_headline` varchar(45)
,`forum_time` datetime
,`forum_text` text
,`forum_update_time` datetime
,`users_users_id` int(11)
,`forum_subcat_fsubcat_id` int(11)
,`users_id` int(11)
,`users_firstname` varchar(45)
,`users_lastname` varchar(45)
,`users_adress` varchar(150)
,`users_email` varchar(45)
,`users_signupdate` timestamp
,`users_username` varchar(45)
,`users_password` varchar(45)
,`users_active` int(11)
,`roles_roles_id` int(11)
);
-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `forum_subcat`
--

CREATE TABLE IF NOT EXISTS `forum_subcat` (
  `fsubcat_id` int(11) NOT NULL AUTO_INCREMENT,
  `fsubcat_name` varchar(45) DEFAULT NULL,
  `fsubcat_desc` varchar(45) DEFAULT NULL,
  `forum_category_fcat_id` int(11) NOT NULL,
  PRIMARY KEY (`fsubcat_id`),
  KEY `fk_forum_subcat_forum_category1_idx` (`forum_category_fcat_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Data dump for tabellen `forum_subcat`
--

INSERT INTO `forum_subcat` (`fsubcat_id`, `fsubcat_name`, `fsubcat_desc`, `forum_category_fcat_id`) VALUES
(1, 'Like a sir', 'pik!', 1),
(2, 'disco', 'pik!', 2);

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `gallery_category`
--

CREATE TABLE IF NOT EXISTS `gallery_category` (
  `gallery_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `gallery_category_name` varchar(45) DEFAULT NULL,
  `gallery_category_desc` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`gallery_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Stand-in-struktur for visning `gallery_has_img_and_cat`
--
CREATE TABLE IF NOT EXISTS `gallery_has_img_and_cat` (
`gallery_images_id` int(11)
,`gallery_images_name` varchar(256)
,`gallery_images_desc` varchar(256)
,`gallery_images_uploadDate` datetime
,`gallery_category_gallery_category_id` int(11)
,`gallery_category_id` int(11)
,`gallery_category_name` varchar(45)
,`gallery_category_desc` varchar(256)
);
-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `gallery_images`
--

CREATE TABLE IF NOT EXISTS `gallery_images` (
  `gallery_images_id` int(11) NOT NULL AUTO_INCREMENT,
  `gallery_images_name` varchar(256) DEFAULT NULL,
  `gallery_images_desc` varchar(256) DEFAULT NULL,
  `gallery_images_uploadDate` datetime DEFAULT CURRENT_TIMESTAMP,
  `gallery_category_gallery_category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`gallery_images_id`),
  KEY `fk_gallery_images_gallery_category1_idx` (`gallery_category_gallery_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `info`
--

CREATE TABLE IF NOT EXISTS `info` (
  `info_id` int(11) NOT NULL AUTO_INCREMENT,
  `info_adressline_one` varchar(200) DEFAULT NULL,
  `info_adressline_two` varchar(200) DEFAULT NULL,
  `info_phone_one` int(10) DEFAULT NULL,
  `info_phone_two` int(10) DEFAULT NULL,
  `info_email` varchar(45) DEFAULT NULL,
  `info_company` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`info_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Data dump for tabellen `info`
--

INSERT INTO `info` (`info_id`, `info_adressline_one`, `info_adressline_two`, `info_phone_one`, `info_phone_two`, `info_email`, `info_company`) VALUES
(1, 'Dette er adressefelt1', 'Dette er adressefelt2', 12345678, 12345678, 'Demo@Demo.dk', '12345678910');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `news_id` int(11) NOT NULL AUTO_INCREMENT,
  `news_headline` varchar(200) DEFAULT NULL,
  `news_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `news_text` longtext,
  `news_update_time` datetime DEFAULT NULL,
  `news_users_users_id` int(11) DEFAULT NULL,
  `news_events_category_cat_id` int(11) NOT NULL,
  PRIMARY KEY (`news_id`),
  KEY `fk_news_users1_idx` (`news_users_users_id`),
  KEY `fk_news_news_events_category1_idx` (`news_events_category_cat_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Data dump for tabellen `news`
--

INSERT INTO `news` (`news_id`, `news_headline`, `news_time`, `news_text`, `news_update_time`, `news_users_users_id`, `news_events_category_cat_id`) VALUES
(12, 'Montana / bang og olufsen arrangement i butikken i august', '2006-07-23 22:00:00', 'Vi arrangerer med B&O i Vingårdsgade et spændende arrangement, hvor vi præsenterer nyheder fra B&O samt Montana. \r\nIndretningsarkitekt fra Montana samt specialister fra B&O vil være tilstede. Vi er vært ved en let forfriskning. \r\nFor nærmere info. / dato samt tidspunkter venligst kontakt Jan Sørensen i butikken.\r\n', '2014-04-29 14:31:43', 4, 2),
(13, 'Lightyears udfordrer markedet for designede lamper', '2006-07-25 22:00:00', '<p>Ny lampevirksomhed bag ambiti&oslash;s lancering af dansk design. I 2005 introduceres 15 forskellige lampeserier alle initieret af nulevende f&oslash;rende designere. Lightyears A/S er navnet p&aring; den nye akt&oslash;r p&aring; markedet for boligbelysning i Skandinavien, som p&aring; Copenhagen International Furniture Fair i maj pr&aelig;senterer den mest ambiti&oslash;se produktlancering for designede belysningsprodukter i Danmark i mange &aring;r. Designede lamper af h&oslash;j kvalitet til en fair pris er den b&aelig;rende forretningsid&eacute;. "Visionen er at g&oslash;re designede lamper tilg&aelig;ngelige for en bred m&aring;lgruppe af design- og kvalitetsbevidste forbrugere," siger adm. dir. Lars &Oslash;stergaard Olsen.</p>', '2014-04-27 12:57:03', 2, 2),
(14, 'Innovativ duo bag ny møbelkollektion', '2006-08-04 22:00:00', 'Danskproducerede møbler til "den moderne design- og kvalitetsbeviste forbruger", som ikke vil købe de samme møbler som sine forældre.Det er nyeste idé fra den 28-årige møbelarkitekt \r\nRené Hovgaard og møbelmanden Jens Hornbak, 32. Sammen danner de firmaet dnmark, som lige nu er aktuel med en helt ny møbelkollektion, der sælges landet over.\r\nKollektionen indeholder bl.a. Daybed i læder fra 25.500 kr., stolen Pablo fra 1375 kr. og bordet Hornsleth Café skabt i samarbejde med kunstneren Kristian von Hornsleth fra 3300 kr. \r\n', '2006-08-05 00:00:00', 2, 2),
(15, 'Formland 2006 - Potten i gummi fik prisen', '2006-08-08 22:00:00', 'Pot for One Flower - vinder af Formland Prisen i foråret 2006. 10 farver, to stk. for 149 kr., Normann Copenhagen.\r\nEn ganske lille, men alligevel helt innovativ urtepotte i gummi løb med Formland Prisen, der uddeles hvert halve år på messen Formland i Herning. Med prisen følger 100.000 kr. til markedsføring. Pot for One Flower, som vinderen hedder, er designet af Boris Berlin og Poul Christiansen fra Komplot Design for Normann Copenhagen. Fra dommer-komiteen lød det, at Normann tidligere har anvendt gummi til bl.a. en vaskebalje, et dørslag og en tragt, og nu fuldendes serien med et produkt til bordet. \r\nDe to andre nominerede til Formland Prisen var et par solide børnemøbler fra firmaet Collect Furniture samt vasen Confetti fra Rosendahl. Møblerne er designet i skæve vinkler, der hindrer, at de vælter, og vasen er designet af Lin Utzon, der har dekoreret med forårsglade farver i nye sammensætninger. \r\n', '2006-08-09 00:00:00', 2, 2);

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `newsletter`
--

CREATE TABLE IF NOT EXISTS `newsletter` (
  `newsletter_id` int(11) NOT NULL AUTO_INCREMENT,
  `newsletter_fullname` varchar(45) DEFAULT NULL,
  `newsletter_email` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`newsletter_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Data dump for tabellen `newsletter`
--

INSERT INTO `newsletter` (`newsletter_id`, `newsletter_fullname`, `newsletter_email`) VALUES
(2, NULL, 'biffan2003@hotmail.com'),
(3, NULL, 'ost@ost.dk');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `news_comments`
--

CREATE TABLE IF NOT EXISTS `news_comments` (
  `com_id` int(11) NOT NULL AUTO_INCREMENT,
  `com_comment` text,
  `com_time` timestamp NULL DEFAULT NULL,
  `news_news_id` int(11) NOT NULL,
  `com_users_users_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`com_id`),
  KEY `fk_news_comments_news1_idx` (`news_news_id`),
  KEY `fk_news_comments_users1_idx` (`com_users_users_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

--
-- Data dump for tabellen `news_comments`
--

INSERT INTO `news_comments` (`com_id`, `com_comment`, `com_time`, `news_news_id`, `com_users_users_id`) VALUES
(9, 'Testetstetsst', '2014-05-07 22:00:00', 12, 4),
(11, 'gdsdgsdgsdfgshdfghsfhdhdf', '2014-05-07 22:00:00', 12, 2),
(12, 'adadaaa', '2014-05-04 22:00:00', 12, 7),
(16, 'dasdasd', '2014-04-14 22:00:00', 12, 7),
(17, 'bxdvzddzdzgd', '2014-04-14 22:00:00', 12, 2),
(18, 'flesh\r\n', '2014-04-14 22:00:00', 13, 2),
(19, 'sdffasfafsasasf', '2014-04-14 22:00:00', 12, 2),
(20, 'dasdasd', '2014-04-14 22:00:00', 13, 2),
(21, 'fsafasfsafsaf', '2014-04-14 22:00:00', 13, 2),
(22, 'dette er en test', '2014-04-14 22:00:00', 12, NULL);

-- --------------------------------------------------------

--
-- Stand-in-struktur for visning `news_comments_has_users`
--
CREATE TABLE IF NOT EXISTS `news_comments_has_users` (
`com_id` int(11)
,`com_comment` text
,`com_time` timestamp
,`news_news_id` int(11)
,`com_users_users_id` int(11)
,`users_id` int(11)
,`users_firstname` varchar(45)
,`users_lastname` varchar(45)
,`users_adress` varchar(150)
,`users_email` varchar(45)
,`users_signupdate` timestamp
,`users_username` varchar(45)
,`users_password` varchar(45)
,`users_active` int(11)
,`roles_roles_id` int(11)
);
-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `news_events_category`
--

CREATE TABLE IF NOT EXISTS `news_events_category` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '	',
  `cat_name` varchar(200) DEFAULT NULL,
  `cat_desc` text,
  `cat_event_news` int(11) DEFAULT NULL,
  `cat_small_img` varchar(45) DEFAULT NULL,
  `cat_large_img` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Data dump for tabellen `news_events_category`
--

INSERT INTO `news_events_category` (`cat_id`, `cat_name`, `cat_desc`, `cat_event_news`, `cat_small_img`, `cat_large_img`) VALUES
(1, 'News', 'Dette er news', 0, NULL, NULL),
(2, 'nyheder', 'bla bla', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Stand-in-struktur for visning `news_has_comments`
--
CREATE TABLE IF NOT EXISTS `news_has_comments` (
`news_id` int(11)
,`news_headline` varchar(200)
,`news_time` timestamp
,`news_text` longtext
,`news_update_time` datetime
,`news_users_users_id` int(11)
,`news_events_category_cat_id` int(11)
,`com_id` int(11)
,`com_comment` text
,`com_time` timestamp
,`news_news_id` int(11)
,`com_users_users_id` int(11)
);
-- --------------------------------------------------------

--
-- Stand-in-struktur for visning `news_has_comments_and_users`
--
CREATE TABLE IF NOT EXISTS `news_has_comments_and_users` (
`news_id` int(11)
,`news_headline` varchar(200)
,`news_time` timestamp
,`news_text` longtext
,`news_update_time` datetime
,`news_users_users_id` int(11)
,`news_events_category_cat_id` int(11)
,`com_id` int(11)
,`com_comment` text
,`com_time` timestamp
,`news_news_id` int(11)
,`com_users_users_id` int(11)
,`users_id` int(11)
,`users_firstname` varchar(45)
,`users_lastname` varchar(45)
,`users_adress` varchar(150)
,`users_email` varchar(45)
,`users_signupdate` timestamp
,`users_username` varchar(45)
,`users_password` varchar(45)
,`users_active` int(11)
,`roles_roles_id` int(11)
);
-- --------------------------------------------------------

--
-- Stand-in-struktur for visning `news_has_users`
--
CREATE TABLE IF NOT EXISTS `news_has_users` (
`news_id` int(11)
,`news_headline` varchar(200)
,`news_time` timestamp
,`news_text` longtext
,`news_update_time` datetime
,`news_users_users_id` int(11)
,`news_events_category_cat_id` int(11)
,`users_id` int(11)
,`users_firstname` varchar(45)
,`users_lastname` varchar(45)
,`users_adress` varchar(150)
,`users_email` varchar(45)
,`users_signupdate` timestamp
,`users_username` varchar(45)
,`users_password` varchar(45)
,`users_active` int(11)
,`roles_roles_id` int(11)
);
-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `products_id` int(11) NOT NULL AUTO_INCREMENT,
  `products_name` varchar(50) NOT NULL,
  `products_nr` int(20) DEFAULT NULL,
  `products_year` int(4) DEFAULT NULL,
  `products_price` int(20) DEFAULT NULL,
  `products_desc` text,
  `products_design` varchar(40) DEFAULT NULL,
  `products_max` int(11) NOT NULL,
  `products_category_products_cat_id` int(11) NOT NULL,
  PRIMARY KEY (`products_id`),
  KEY `fk_products_products_category1_idx` (`products_category_products_cat_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=60 ;

--
-- Data dump for tabellen `products`
--

INSERT INTO `products` (`products_id`, `products_name`, `products_nr`, `products_year`, `products_price`, `products_desc`, `products_design`, `products_max`, `products_category_products_cat_id`) VALUES
(59, 'Dette er en sofa', 25252525, 2014, 500, '<p>dette er en sofa lort</p>', 'kurt larsen', 150, 1);

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `products_category`
--

CREATE TABLE IF NOT EXISTS `products_category` (
  `products_cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `products_cat_name` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`products_cat_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Data dump for tabellen `products_category`
--

INSERT INTO `products_category` (`products_cat_id`, `products_cat_name`) VALUES
(1, 'Sofa'),
(2, 'Sofabord'),
(3, 'Spisestol'),
(4, 'Spisebord');

-- --------------------------------------------------------

--
-- Stand-in-struktur for visning `products_has_cat_and_img`
--
CREATE TABLE IF NOT EXISTS `products_has_cat_and_img` (
`products_id` int(11)
,`products_name` varchar(50)
,`products_nr` int(20)
,`products_year` int(4)
,`products_price` int(20)
,`products_desc` text
,`products_design` varchar(40)
,`products_max` int(11)
,`products_category_products_cat_id` int(11)
,`products_cat_id` int(11)
,`products_cat_name` varchar(40)
,`products_image_id` int(11)
,`products_image_name` varchar(60)
,`products_products_id` int(11)
);
-- --------------------------------------------------------

--
-- Stand-in-struktur for visning `products_has_images`
--
CREATE TABLE IF NOT EXISTS `products_has_images` (
`products_id` int(11)
,`products_name` varchar(50)
,`products_nr` int(20)
,`products_year` int(4)
,`products_price` int(20)
,`products_desc` text
,`products_design` varchar(40)
,`products_max` int(11)
,`products_category_products_cat_id` int(11)
,`products_image_id` int(11)
,`products_image_name` varchar(60)
,`products_products_id` int(11)
);
-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `products_image`
--

CREATE TABLE IF NOT EXISTS `products_image` (
  `products_image_id` int(11) NOT NULL AUTO_INCREMENT,
  `products_image_name` varchar(60) DEFAULT NULL,
  `products_products_id` int(11) NOT NULL,
  PRIMARY KEY (`products_image_id`),
  KEY `fk_products_image_products1_idx` (`products_products_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Data dump for tabellen `products_image`
--

INSERT INTO `products_image` (`products_image_id`, `products_image_name`, `products_products_id`) VALUES
(4, '1401816956_578130_10151806340390022_1585717892_n.jpg', 59);

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `roles_id` int(11) NOT NULL AUTO_INCREMENT,
  `roles_rank` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`roles_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Data dump for tabellen `roles`
--

INSERT INTO `roles` (`roles_id`, `roles_rank`) VALUES
(1, 'Guest'),
(2, 'User'),
(3, 'Admin'),
(4, 'Superadmin');

-- --------------------------------------------------------

--
-- Struktur-dump for tabellen `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `users_id` int(11) NOT NULL AUTO_INCREMENT,
  `users_firstname` varchar(45) DEFAULT NULL,
  `users_lastname` varchar(45) DEFAULT NULL,
  `users_adress` varchar(150) DEFAULT NULL,
  `users_email` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `users_signupdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `users_username` varchar(45) DEFAULT NULL,
  `users_password` varchar(45) DEFAULT NULL,
  `users_active` int(11) DEFAULT NULL,
  `roles_roles_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`users_id`),
  KEY `fk_users_roles_idx` (`roles_roles_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Data dump for tabellen `users`
--

INSERT INTO `users` (`users_id`, `users_firstname`, `users_lastname`, `users_adress`, `users_email`, `users_signupdate`, `users_username`, `users_password`, `users_active`, `roles_roles_id`) VALUES
(2, 'admin', 'admin', 'admin 118', 'admin@admin.dk', '2014-04-14 22:00:00', 'admin', 'f6fdffe48c908deb0f4c3bd36c032e72', 1, 4),
(4, 'users', 'users', 'users', 'users@users.dk', '2014-04-15 22:00:00', 'users', '3a9feeb8bc3c07b4c88d1367b757b617', 1, 2),
(7, 'Dorte', 'andersen', 'dd', 'dorte@dorte.dk', '2014-05-07 22:00:00', 'Dorte', '3b14cd2ba9b357a89176db760b504d55', 1, 2);

-- --------------------------------------------------------

--
-- Stand-in-struktur for visning `users_has_roles`
--
CREATE TABLE IF NOT EXISTS `users_has_roles` (
`users_id` int(11)
,`users_firstname` varchar(45)
,`users_lastname` varchar(45)
,`users_adress` varchar(150)
,`users_email` varchar(45)
,`users_signupdate` timestamp
,`users_username` varchar(45)
,`users_password` varchar(45)
,`users_active` int(11)
,`roles_roles_id` int(11)
,`roles_id` int(11)
,`roles_rank` varchar(45)
);
-- --------------------------------------------------------

--
-- Struktur for visning `fcat_has_subcat`
--
DROP TABLE IF EXISTS `fcat_has_subcat`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `fcat_has_subcat` AS select `forum_category`.`fcat_id` AS `fcat_id`,`forum_category`.`fcat_name` AS `fcat_name`,`forum_subcat`.`fsubcat_id` AS `fsubcat_id`,`forum_subcat`.`fsubcat_name` AS `fsubcat_name`,`forum_subcat`.`fsubcat_desc` AS `fsubcat_desc`,`forum_subcat`.`forum_category_fcat_id` AS `forum_category_fcat_id` from (`forum_category` join `forum_subcat` on((`forum_subcat`.`forum_category_fcat_id` = `forum_category`.`fcat_id`)));

-- --------------------------------------------------------

--
-- Struktur for visning `forum_comments_has_users`
--
DROP TABLE IF EXISTS `forum_comments_has_users`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `forum_comments_has_users` AS select `forum_comments`.`fcom_id` AS `fcom_id`,`forum_comments`.`fcom_comment` AS `fcom_comment`,`forum_comments`.`fcom_time` AS `fcom_time`,`forum_comments`.`forum_forum_id` AS `forum_forum_id`,`forum_comments`.`users_users_id` AS `users_users_id`,`users`.`users_id` AS `users_id`,`users`.`users_firstname` AS `users_firstname`,`users`.`users_lastname` AS `users_lastname`,`users`.`users_adress` AS `users_adress`,`users`.`users_email` AS `users_email`,`users`.`users_signupdate` AS `users_signupdate`,`users`.`users_username` AS `users_username`,`users`.`users_password` AS `users_password`,`users`.`users_active` AS `users_active`,`users`.`roles_roles_id` AS `roles_roles_id` from (`forum_comments` left join `users` on((`users`.`users_id` = `forum_comments`.`users_users_id`)));

-- --------------------------------------------------------

--
-- Struktur for visning `forum_has_subcats_and_users`
--
DROP TABLE IF EXISTS `forum_has_subcats_and_users`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `forum_has_subcats_and_users` AS select `forum`.`forum_id` AS `forum_id`,`forum`.`forum_headline` AS `forum_headline`,`forum`.`forum_time` AS `forum_time`,`forum`.`forum_text` AS `forum_text`,`forum`.`forum_update_time` AS `forum_update_time`,`forum`.`users_users_id` AS `users_users_id`,`forum`.`forum_subcat_fsubcat_id` AS `forum_subcat_fsubcat_id`,`forum_subcat`.`fsubcat_id` AS `fsubcat_id`,`forum_subcat`.`fsubcat_name` AS `fsubcat_name`,`forum_subcat`.`fsubcat_desc` AS `fsubcat_desc`,`forum_subcat`.`forum_category_fcat_id` AS `forum_category_fcat_id`,`users`.`users_id` AS `users_id`,`users`.`users_firstname` AS `users_firstname`,`users`.`users_lastname` AS `users_lastname`,`users`.`users_adress` AS `users_adress`,`users`.`users_email` AS `users_email`,`users`.`users_signupdate` AS `users_signupdate`,`users`.`users_username` AS `users_username`,`users`.`users_password` AS `users_password`,`users`.`users_active` AS `users_active`,`users`.`roles_roles_id` AS `roles_roles_id` from ((`forum` left join `forum_subcat` on((`forum`.`forum_subcat_fsubcat_id` = `forum_subcat`.`fsubcat_id`))) left join `users` on((`forum`.`users_users_id` = `users`.`users_id`)));

-- --------------------------------------------------------

--
-- Struktur for visning `forum_has_users`
--
DROP TABLE IF EXISTS `forum_has_users`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `forum_has_users` AS select `forum`.`forum_id` AS `forum_id`,`forum`.`forum_headline` AS `forum_headline`,`forum`.`forum_time` AS `forum_time`,`forum`.`forum_text` AS `forum_text`,`forum`.`forum_update_time` AS `forum_update_time`,`forum`.`users_users_id` AS `users_users_id`,`forum`.`forum_subcat_fsubcat_id` AS `forum_subcat_fsubcat_id`,`users`.`users_id` AS `users_id`,`users`.`users_firstname` AS `users_firstname`,`users`.`users_lastname` AS `users_lastname`,`users`.`users_adress` AS `users_adress`,`users`.`users_email` AS `users_email`,`users`.`users_signupdate` AS `users_signupdate`,`users`.`users_username` AS `users_username`,`users`.`users_password` AS `users_password`,`users`.`users_active` AS `users_active`,`users`.`roles_roles_id` AS `roles_roles_id` from (`forum` left join `users` on((`forum`.`users_users_id` = `users`.`users_id`)));

-- --------------------------------------------------------

--
-- Struktur for visning `gallery_has_img_and_cat`
--
DROP TABLE IF EXISTS `gallery_has_img_and_cat`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `gallery_has_img_and_cat` AS select `gallery_images`.`gallery_images_id` AS `gallery_images_id`,`gallery_images`.`gallery_images_name` AS `gallery_images_name`,`gallery_images`.`gallery_images_desc` AS `gallery_images_desc`,`gallery_images`.`gallery_images_uploadDate` AS `gallery_images_uploadDate`,`gallery_images`.`gallery_category_gallery_category_id` AS `gallery_category_gallery_category_id`,`gallery_category`.`gallery_category_id` AS `gallery_category_id`,`gallery_category`.`gallery_category_name` AS `gallery_category_name`,`gallery_category`.`gallery_category_desc` AS `gallery_category_desc` from (`gallery_images` left join `gallery_category` on((`gallery_images`.`gallery_category_gallery_category_id` = `gallery_category`.`gallery_category_id`)));

-- --------------------------------------------------------

--
-- Struktur for visning `news_comments_has_users`
--
DROP TABLE IF EXISTS `news_comments_has_users`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `news_comments_has_users` AS select `news_comments`.`com_id` AS `com_id`,`news_comments`.`com_comment` AS `com_comment`,`news_comments`.`com_time` AS `com_time`,`news_comments`.`news_news_id` AS `news_news_id`,`news_comments`.`com_users_users_id` AS `com_users_users_id`,`users`.`users_id` AS `users_id`,`users`.`users_firstname` AS `users_firstname`,`users`.`users_lastname` AS `users_lastname`,`users`.`users_adress` AS `users_adress`,`users`.`users_email` AS `users_email`,`users`.`users_signupdate` AS `users_signupdate`,`users`.`users_username` AS `users_username`,`users`.`users_password` AS `users_password`,`users`.`users_active` AS `users_active`,`users`.`roles_roles_id` AS `roles_roles_id` from (`news_comments` left join `users` on((`news_comments`.`com_users_users_id` = `users`.`users_id`)));

-- --------------------------------------------------------

--
-- Struktur for visning `news_has_comments`
--
DROP TABLE IF EXISTS `news_has_comments`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `news_has_comments` AS select `news`.`news_id` AS `news_id`,`news`.`news_headline` AS `news_headline`,`news`.`news_time` AS `news_time`,`news`.`news_text` AS `news_text`,`news`.`news_update_time` AS `news_update_time`,`news`.`news_users_users_id` AS `news_users_users_id`,`news`.`news_events_category_cat_id` AS `news_events_category_cat_id`,`news_comments`.`com_id` AS `com_id`,`news_comments`.`com_comment` AS `com_comment`,`news_comments`.`com_time` AS `com_time`,`news_comments`.`news_news_id` AS `news_news_id`,`news_comments`.`com_users_users_id` AS `com_users_users_id` from (`news` join `news_comments` on((`news_comments`.`news_news_id` = `news`.`news_id`)));

-- --------------------------------------------------------

--
-- Struktur for visning `news_has_comments_and_users`
--
DROP TABLE IF EXISTS `news_has_comments_and_users`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `news_has_comments_and_users` AS select `news`.`news_id` AS `news_id`,`news`.`news_headline` AS `news_headline`,`news`.`news_time` AS `news_time`,`news`.`news_text` AS `news_text`,`news`.`news_update_time` AS `news_update_time`,`news`.`news_users_users_id` AS `news_users_users_id`,`news`.`news_events_category_cat_id` AS `news_events_category_cat_id`,`news_comments`.`com_id` AS `com_id`,`news_comments`.`com_comment` AS `com_comment`,`news_comments`.`com_time` AS `com_time`,`news_comments`.`news_news_id` AS `news_news_id`,`news_comments`.`com_users_users_id` AS `com_users_users_id`,`users`.`users_id` AS `users_id`,`users`.`users_firstname` AS `users_firstname`,`users`.`users_lastname` AS `users_lastname`,`users`.`users_adress` AS `users_adress`,`users`.`users_email` AS `users_email`,`users`.`users_signupdate` AS `users_signupdate`,`users`.`users_username` AS `users_username`,`users`.`users_password` AS `users_password`,`users`.`users_active` AS `users_active`,`users`.`roles_roles_id` AS `roles_roles_id` from ((`news` join `news_comments` on((`news_comments`.`news_news_id` = `news`.`news_id`))) join `users` on((`news`.`news_users_users_id` = `users`.`users_id`)));

-- --------------------------------------------------------

--
-- Struktur for visning `news_has_users`
--
DROP TABLE IF EXISTS `news_has_users`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `news_has_users` AS select `news`.`news_id` AS `news_id`,`news`.`news_headline` AS `news_headline`,`news`.`news_time` AS `news_time`,`news`.`news_text` AS `news_text`,`news`.`news_update_time` AS `news_update_time`,`news`.`news_users_users_id` AS `news_users_users_id`,`news`.`news_events_category_cat_id` AS `news_events_category_cat_id`,`users`.`users_id` AS `users_id`,`users`.`users_firstname` AS `users_firstname`,`users`.`users_lastname` AS `users_lastname`,`users`.`users_adress` AS `users_adress`,`users`.`users_email` AS `users_email`,`users`.`users_signupdate` AS `users_signupdate`,`users`.`users_username` AS `users_username`,`users`.`users_password` AS `users_password`,`users`.`users_active` AS `users_active`,`users`.`roles_roles_id` AS `roles_roles_id` from (`news` join `users` on((`users`.`users_id` = `news`.`news_users_users_id`)));

-- --------------------------------------------------------

--
-- Struktur for visning `products_has_cat_and_img`
--
DROP TABLE IF EXISTS `products_has_cat_and_img`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `products_has_cat_and_img` AS select `products`.`products_id` AS `products_id`,`products`.`products_name` AS `products_name`,`products`.`products_nr` AS `products_nr`,`products`.`products_year` AS `products_year`,`products`.`products_price` AS `products_price`,`products`.`products_desc` AS `products_desc`,`products`.`products_design` AS `products_design`,`products`.`products_max` AS `products_max`,`products`.`products_category_products_cat_id` AS `products_category_products_cat_id`,`products_category`.`products_cat_id` AS `products_cat_id`,`products_category`.`products_cat_name` AS `products_cat_name`,`products_image`.`products_image_id` AS `products_image_id`,`products_image`.`products_image_name` AS `products_image_name`,`products_image`.`products_products_id` AS `products_products_id` from ((`products` left join `products_category` on((`products`.`products_category_products_cat_id` = `products_category`.`products_cat_id`))) left join `products_image` on((`products`.`products_id` = `products_image`.`products_products_id`)));

-- --------------------------------------------------------

--
-- Struktur for visning `products_has_images`
--
DROP TABLE IF EXISTS `products_has_images`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `products_has_images` AS select `products`.`products_id` AS `products_id`,`products`.`products_name` AS `products_name`,`products`.`products_nr` AS `products_nr`,`products`.`products_year` AS `products_year`,`products`.`products_price` AS `products_price`,`products`.`products_desc` AS `products_desc`,`products`.`products_design` AS `products_design`,`products`.`products_max` AS `products_max`,`products`.`products_category_products_cat_id` AS `products_category_products_cat_id`,`products_image`.`products_image_id` AS `products_image_id`,`products_image`.`products_image_name` AS `products_image_name`,`products_image`.`products_products_id` AS `products_products_id` from (`products` left join `products_image` on((`products`.`products_id` = `products_image`.`products_products_id`)));

-- --------------------------------------------------------

--
-- Struktur for visning `users_has_roles`
--
DROP TABLE IF EXISTS `users_has_roles`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `users_has_roles` AS select `users`.`users_id` AS `users_id`,`users`.`users_firstname` AS `users_firstname`,`users`.`users_lastname` AS `users_lastname`,`users`.`users_adress` AS `users_adress`,`users`.`users_email` AS `users_email`,`users`.`users_signupdate` AS `users_signupdate`,`users`.`users_username` AS `users_username`,`users`.`users_password` AS `users_password`,`users`.`users_active` AS `users_active`,`users`.`roles_roles_id` AS `roles_roles_id`,`roles`.`roles_id` AS `roles_id`,`roles`.`roles_rank` AS `roles_rank` from (`users` left join `roles` on((`roles`.`roles_id` = `users`.`roles_roles_id`)));

--
-- Begrænsninger for dumpede tabeller
--

--
-- Begrænsninger for tabel `event`
--
ALTER TABLE `event`
  ADD CONSTRAINT `fk_event_news_events_category1` FOREIGN KEY (`event_events_category_cat_id`) REFERENCES `news_events_category` (`cat_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_event_users1` FOREIGN KEY (`event_users_users_id`) REFERENCES `users` (`users_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Begrænsninger for tabel `event_signup`
--
ALTER TABLE `event_signup`
  ADD CONSTRAINT `fk_event_signup_event1` FOREIGN KEY (`event_event_id`) REFERENCES `event` (`event_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_event_signup_users1` FOREIGN KEY (`users_users_id`) REFERENCES `users` (`users_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Begrænsninger for tabel `forum`
--
ALTER TABLE `forum`
  ADD CONSTRAINT `fk_forum_users1` FOREIGN KEY (`users_users_id`) REFERENCES `users` (`users_id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `fk_forum_forum_subcat1` FOREIGN KEY (`forum_subcat_fsubcat_id`) REFERENCES `forum_subcat` (`fsubcat_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Begrænsninger for tabel `forum_comments`
--
ALTER TABLE `forum_comments`
  ADD CONSTRAINT `fk_forum_comments_users1` FOREIGN KEY (`users_users_id`) REFERENCES `users` (`users_id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `fk_forum_comments_forum1` FOREIGN KEY (`forum_forum_id`) REFERENCES `forum` (`forum_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Begrænsninger for tabel `forum_subcat`
--
ALTER TABLE `forum_subcat`
  ADD CONSTRAINT `fk_forum_subcat_forum_category1` FOREIGN KEY (`forum_category_fcat_id`) REFERENCES `forum_category` (`fcat_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Begrænsninger for tabel `gallery_images`
--
ALTER TABLE `gallery_images`
  ADD CONSTRAINT `fk_gallery_images_gallery_category1` FOREIGN KEY (`gallery_category_gallery_category_id`) REFERENCES `gallery_category` (`gallery_category_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Begrænsninger for tabel `news`
--
ALTER TABLE `news`
  ADD CONSTRAINT `fk_news_users1` FOREIGN KEY (`news_users_users_id`) REFERENCES `users` (`users_id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `fk_news_news_events_category1` FOREIGN KEY (`news_events_category_cat_id`) REFERENCES `news_events_category` (`cat_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Begrænsninger for tabel `news_comments`
--
ALTER TABLE `news_comments`
  ADD CONSTRAINT `fk_news_comments_users1` FOREIGN KEY (`com_users_users_id`) REFERENCES `users` (`users_id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `fk_news_comments_news1` FOREIGN KEY (`news_news_id`) REFERENCES `news` (`news_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Begrænsninger for tabel `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `fk_products_products_category1` FOREIGN KEY (`products_category_products_cat_id`) REFERENCES `products_category` (`products_cat_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Begrænsninger for tabel `products_image`
--
ALTER TABLE `products_image`
  ADD CONSTRAINT `fk_products_image_products1` FOREIGN KEY (`products_products_id`) REFERENCES `products` (`products_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Begrænsninger for tabel `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_users_roles` FOREIGN KEY (`roles_roles_id`) REFERENCES `roles` (`roles_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
