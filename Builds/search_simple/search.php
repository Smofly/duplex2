<?php
include_once 'connect.php';
?>
<div class="row">
    <div class="col-md-12">
        <form action="" method="post">
            <div class="form-group">
                <div class="col-md-9 pull-left">
                    <input class="form-control" type="text" name="search_felt" placeholder="Søg" />
                </div>
            </div>
            <div class="col-md-3 pull-right">
                <button class="btn btn-info" type ="submit" name="searchSubmitted" value="Søg">Søg</button>
            </div
        </form>
        <br /><br />
        <?php

        if(isset($_POST['searchSubmitted']))
        {
            $variabelSearch = $_POST['search_felt'];
            $searchSQL = "SELECT *
                          FROM news
                          WHERE news_headline LIKE  '%$variabelSearch%'";
            $result = mysqli_query($mysqli, $searchSQL);

            while($row = mysqli_fetch_assoc($result))
            {
                echo '<div class="well well-sm">';
                echo '<a href="?frontend_page=front_news_one&&news_id='.$row['news_id'].'">'.$row['news_headline'] . "</a><br />";
                echo '</div>';
            }
        }
        ?>
    </div>
    <div class="col-md-6"></div>
</div>