<!-- Carousel

    skal snart admintisizes

    ================================================== -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
        <div class="item active">
            <img src="http://placehold.it/1070x400" alt="First slide">
            <div class="container">
                <div class="carousel-caption">
                    <h1>Eksempel på overskrift</h1>
                    <p><a class="btn btn-lg btn-success" href="#" role="button">Tilmeld dig i dag</a></p>
                </div>
            </div>
        </div>
        <div class="item">
            <img src="http://placehold.it/1070x400" alt="Second slide">
            <div class="container">
                <div class="carousel-caption">
                    <h1>Eksempel på overskrift</h1>
                    <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>

                </div>
            </div>
        </div>
        <div class="item">
            <img src="http://placehold.it/1070x400" alt="Third slide">
            <div class="container">
                <div class="carousel-caption">
                    <h1>Eksempel på overskrift</h1>
                    <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>

                </div>
            </div>
        </div>
    </div>
    <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
</div><!-- /.carousel -->
