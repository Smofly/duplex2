<div class="header">
    <nav class="navbar navbar-default" role="navigation">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="index.php">Forside</a></li>                    
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Moduler <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="?frontend_page=front_news">Nyheder</a></li>
                            <li><a href="?frontend_page=front_galleri">Galleri</a></li>
                            <li><a href="?frontend_page=front_forum">Forum</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Webshop <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="?frontend_page=front_webshop">Webshop</a></li>
                            <li><a href="?frontend_page=front_search">Søg i produkter</a></li>
                        </ul>
                    </li> 
                    <li><a href="?frontend_page=front_contact">Kontakt</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <?php if (in_array('Guest', $users_rank->getRolesRank())) { ?>
                        <!-- Buttons to trigger modal -->
                        <li><a href="?frontend_page=front_login"> Log ind</a></li>
                        <li><a href="?frontend_page=front_signup"> Registrer bruger</a></li>
                    <?php } ?>
                </ul>
            </div><!-- /.navbar-collapse -->
    </nav>
</div><!-- header end -->