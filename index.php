<?php
// Start Session for sessions
session_start();
// Loader all classes
include_once 'autoloader.php';
include_once 'includes/switch.php';
include_once 'includes/functions.php';
// Instancier classes
$kurv = new Kurv();
if (isset($_SESSION['users_rank'])) {
    $users_rank = new Roles('roles', $_SESSION['users_rank']);
} else {
    $users_rank = new Roles('roles');
}
?>

<!DOCTYPE html>
<html lang="da">
    <head>
        <!-- Website Title & Description for Search Engine purposes -->
        <title></title>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html">

        <!-- Mobile viewport optimized -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <!-- Bootstrap CSS -->
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/theme.css" rel="stylesheet">

        <!-- TinyMCE til at redigere i nyheder mm. -->
        <script type="text/javascript" src="includes/tinymce/js/tinymce/tinymce.min.js"></script>
        <script type="text/javascript">
            tinyMCE.init({
                mode : "textareas",
                editor_selector : "mceEditor"
            });
        </script>
    </head>
    <body>

        <?php
        if (!in_array('Guest', $users_rank->getRolesRank())) {
            // Check if user is logged in

            include 'includes/user-menu.php';
        }
        ?>

        <div class="container">
            <!-- Projektets logo eller navn-->
            <div class="brand-or-logo">Duplex PHP framework</div>

            <?php include 'header.php' ?>



            <?php
            include ( "$frontend_pages" );
            ?>


            <?php include 'footer.php'; ?>

        </div><!-- finish container -->

