            <head>
                <meta charset="utf-8">
                <title>Dashboard :: Administrator</title>

                <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
                <meta name="apple-mobile-web-app-capable" content="yes">    

                <link href="./css/bootstrap.min.css" rel="stylesheet">
                <link href="./css/bootstrap-responsive.min.css" rel="stylesheet">
                <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
                <link href="./css/font-awesome.min.css" rel="stylesheet">
                <link href="./css/ui-lightness/jquery-ui-1.10.0.custom.min.css" rel="stylesheet">
                <link href="./js/plugins/msgGrowl/css/msgGrowl.css" rel="stylesheet">
                <link href="./js/plugins/lightbox/themes/evolution-dark/jquery.lightbox.css" rel="stylesheet">	
                <link href="./js/plugins/msgbox/jquery.msgbox.css" rel="stylesheet">

                <link href="./css/base-admin-3.css" rel="stylesheet">
                <link href="./css/base-admin-3-responsive.css" rel="stylesheet">
                <link href="./css/pages/dashboard.css" rel="stylesheet">
                <link href="./css/custom.css" rel="stylesheet">

                <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
                <!--[if lt IE 9]>
                  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
                <![endif]-->

                <!-- TinyMCE til at redigere i nyheder mm. -->
                <script type="text/javascript" src="../includes/tinymce/js/tinymce/tinymce.min.js"></script>
                <script type="text/javascript">
                    tinymce.init({
                        selector: "textarea"
                    });
                </script>

            </head>