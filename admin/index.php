<?php
// Start Session for sessions
session_start();
// Loader all classes
include_once '../autoloader.php';
include_once '../includes/switch.php';
include 'includes/functions.php';

// if logout is choosen
if (isset($_POST['logout'])) {
    $logout = new Users('users');
    $logout->logout();
    $logout->redirect('../index.php');
}
// Set classes
$users_rank = new Roles('roles', $_SESSION['users_rank']);
?>

<?php
if (in_array('Guest', $users_rank->getRolesRank())) {
    // header
    header('Location: ../index.php');
} else {
    // Hvis der ikke er en bruger i session, så gives der adgang til siden
    if (!in_array('User', $users_rank->getRolesRank())) {
        ?>

        <!DOCTYPE html>
        <html lang="da">

            <?php include 'header.php'; ?>

            <body>

                <?php include 'includes/top_nav.php' ?>
                <?php include 'includes/main_nav.php' ?>

                <?php if (!isset($_GET['page'])): ?>
                    <div class="main">

                                <div class="container">
                                    <div class="row">
                                        <!-- boks med velkomst hilsen -->
                                        <?php include 'includes/welcome.php' ?>
                                        <!-- genvej -->
                                        <?php include 'includes/shortcuts.php' ?>

                                    </div><!-- row -->
                                    <!-- accordians -->
                                    <?php //include 'includes/accordians.php' ?>
                                    <!-- tre bokse med mulighed for at liste data -->
                                    <?php //include 'includes/threelistboxes.php' ?>

                                    <?php
                                else:
                                    include( "$pages" );
                                endif;
                                ?>

                            </div> <!-- /row -->


                        </div> <!-- /container -->

                    </div> <!-- /main -->

                    <?php include'footer.php'; ?>
                    <!-- Le javascript
                    ================================================== -->
                    <!-- Placed at the end of the document so the pages load faster -->
                    <!-- Placed at the end of the document so the pages load faster -->
                    <script src="./js/libs/jquery-1.9.1.min.js"></script>
                    <script src="./js/libs/jquery-ui-1.10.0.custom.min.js"></script>
                    <script src="./js/libs/bootstrap.min.js"></script>

                    <script src="./js/plugins/msgGrowl/js/msgGrowl.js"></script>
                    <script src="./js/plugins/lightbox/jquery.lightbox.min.js"></script>
                    <script src="./js/plugins/msgbox/jquery.msgbox.min.js"></script>

                    <script src="./js/Application.js"></script>

                    <script src="./js/demo/notifications.js"></script>

                    <!-- Custom jscript -->
                    <script src="../js/custom.js"></script>

                    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
                    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.1.47/jquery.form-validator.min.js"></script>
                    <script> $.validate();</script>
            </body>
        </html>
        <?php
    } else {
        // warning that show the visitor that he do not have permissions for this site
        echo "Du har ikke adgang til denne side!";
        header('Location: ../index.php');
    }
}
?>