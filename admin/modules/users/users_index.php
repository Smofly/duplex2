<?php
if (isset($_GET['id'])) {
    $delete_users = new Users('users');
    $delete_users->setCondition_field('users_id');
    $delete_users->setCondition_operator('=');
    $delete_users->setCondition_value($_GET['id']);
    if ($delete_users->delete_user() == true) {
        $delete_users->redirect('?page=admin_users&delete=succes');
    } else {
        $delete_users->redirect('?page=admin_users&delete=deny');
    }
}
?>
<div class="container">
    <div class="widget stacked">
        <div class="widget-header">
            <i class="icon-bookmark"></i>
            <h3>Brugere</h3>
        </div> <!-- /widget-header -->
        <div class="widget-content">
            <div class="col-md-12">
                <?php
                if (isset($_GET['delete']) && $_GET['delete'] == 'succes') {
                    echo '<div class="alert alert-success">Brugeren er nu Slettet</div>';
                }
                if (isset($_GET['delete']) && $_GET['delete'] == 'deny') {
                    echo '<div class="alert alert-danger">Der skete en fejl, brugeren er ikke blevet slettet</div>';
                }
                ?>

                <div class="btn-toolbar">
                    <a href="?page=create_users" class="btn btn-primary">Opret Bruger</a>
                </div>
                <br />
                <div class="well">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Fornavn</th>
                                <th>Efternavn</th>
                                <th>Brugernavn</th>
                                <th>Rank</th>
                                <th style="width: 36px;"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $admin_listuser = new Users('users_has_roles');
                            $admin_listuser->setCondition_order($order = array('roles_id', 'DESC'));
                            $result_admin_listuser = $admin_listuser->select_user();
                            while ($row_admin_listuser = $result_admin_listuser->fetch_object()) {
                                ?>
                                <tr>
                                    <td><?php echo $row_admin_listuser->users_firstname ?></td>
                                    <td><?php echo $row_admin_listuser->users_lastname ?></td>
                                    <td><?php echo $row_admin_listuser->users_username ?></td>
                                    <td><?php echo $row_admin_listuser->roles_rank ?></td>

                                    <?php
                                    if ($row_admin_listuser->roles_rank == 'Superadmin' && in_array('Admin', $users_rank->getRolesRank())) {
                                        
                                    } else {
                                        if ($row_admin_listuser->roles_rank == 'Admin' && in_array('Admin', $users_rank->getRolesRank())) {
                                            
                                        } else {
                                            ?>
                                            <td>
                                                <a href="?page=update_users&id=<?php echo $row_admin_listuser->users_id ?>"><i class="icon-pencil"></i></a>
                                                <a class="confirm" href="?page=admin_users&id=<?php echo $row_admin_listuser->users_id ?>"><i class="icon-remove"></i></a>
                                            </td>
                                            <?php
                                        }
                                    }
                                    ?>

                                </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div><!-- end container -->
    </div>
</div>    