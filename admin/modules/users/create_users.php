<?php
if (isset($_POST['submit_bruger'])) {
    $insert_users = new Users('users');

    $email = $_POST["users_email"];
    $username = $_POST['users_username'];
    $insert_users->setCondition_parameter($parameter = array(
        array('WHERE', 'users_email', '=', "'$email'"),
        array('OR', 'users_username', '=', "'$username'")
    ));
    $result_insertUsers = $insert_users->select_user();
    $count_users = mysqli_num_rows($result_insertUsers);

    if (!$count_users == 0) {
        $insert_users->redirect('?page=create_users&oprettet=deny');
    } else {
        $validering = new Validering();

        // Check if Rank and Active is a number. if not make a error
        $validering->checknumeric($_POST['users_roles']);
        $validering->checknumeric($_POST['users_aktiv']);
        // check Firstname and lastname
        $validering->checkName($_POST['users_firstname']);
        $validering->checkName($_POST['users_lastname']);
        // Check adress and email
        $validering->checkAdresse($_POST['users_adress']);
        $validering->checkEmail($_POST['users_email']);
        // Check username and password
        $validering->checkName($_POST['users_username']);
        $validering->checkMinLength($_POST['users_username'], 2);

        $validering->checkMinLength($_POST['users_password'], 6);
        $validering->checkPassword($_POST['users_password']);

        if ($fejl = $validering->getFejl() == false) {

            $insert_users->setRoles_roles_id($_POST['users_roles']);
            $insert_users->setUsers_active($_POST['users_aktiv']);
            $insert_users->setUsers_firstname($_POST['users_firstname']);
            $insert_users->setUsers_lastname($_POST['users_lastname']);
            $insert_users->setUsers_adress($_POST['users_adress']);
            $insert_users->setUsers_email($_POST['users_email']);
            $insert_users->setUsers_username($_POST['users_username']);
            $insert_users->setUsers_password($_POST['users_password']);
            $insert_users->tableoption(1);

            if (!$insert_users->insert_user() == false) {
                $insert_users->redirect('?page=create_users&oprettet=succes');
            }
        } else {
            $insert_users->redirect('?page=create_users&vali=deny');
        }
    }
}
?>

<div class="main container">
    <div class="widget stacked">
        <div class="widget-header">
            <i class="icon-bookmark"></i>
            <h3>Opret Bruger</h3>
        </div> <!-- /widget-header -->
        <div class="widget-content">
            <div class="col-md-12">
                <?php
                if (isset($_GET['oprettet']) && $_GET['oprettet'] == 'succes') {
                    echo '<div class="alert alert-success">Brugeren er nu oprettet</div>';
                }
                if (isset($_GET['oprettet']) && $_GET['oprettet'] == 'deny') {
                    echo '<div class="alert alert-danger">Der findes allerede en Bruger med valgte Brugernavn/Email</div>';
                }
                if (isset($_GET['vali']) && $_GET['vali'] == 'deny') {
                    echo '<div class="alert alert-danger">En eller flere felter var ikke udfyldt korekt</div>';
                }
                ?>
                <form action="" method="POST">
                    <?php
                    $user_rank = new Users('roles');
                    $result_rank = $user_rank->select_user();
                    ?>
                    <div class="form-group">
                        <select class="form-control" name="users_roles" data-validation="required" data-validation-error-msg="Du skal vælge en bruger rank">
                            <option value="">Vælg Rank</option>
                            <?php
                            if (in_array('Superadmin', $users_rank->getRolesRank())) {
                                while ($row_rank = $result_rank->fetch_object()) {
                                    ?>
                                    <option value="<?php echo $row_rank->roles_id ?>"><?php echo $row_rank->roles_rank ?></option>
                                    <?php
                                }
                            } else {
                                ?>
                                <option value="2">User</option>
                                <?php
                            }
                            ?>

                        </select>
                    </div>
                    <div class="form-group">
                        <select class="form-control" name="users_aktiv" data-validation="required" data-validation-error-msg="Du skal vælge om brugeren profil er aktiveret">
                            <option value="">Vælge Status</option>
                            <option value="1">Aktiveret</option>
                            <option value="0">Deaktivert</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="text" name="users_firstname" placeholder="Indtast navn" data-validation="alphanumeric" data-validation="length" data-validation-length="2-100" data-validation-error-msg="Navnet skal Indholde bogstaver fra a-z og minimum værre 2 max 100 tegn"  />
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="text" name="users_lastname" placeholder="Indtast efternavn" data-validation="alphanumeric" data-validation="length" data-validation-length="2-100" data-validation-error-msg="Efternavnet skal Indholde bogstaver fra a-z og minimum værre 2 max 100 tegn" />
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="text" name="users_adress" placeholder="Indtast Adresse" data-validation="custom" data-validation-regexp="^((.){1,}(\d){1,}(.){0,})$" data-validation-error-msg="Adressen skal indholde et husnummer F.eks Adresse 14" />
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="text" name="users_email" placeholder="Indtast Email"  data-validation="email" data-validation-error-msg="Du har ikke angivet en gyldig email."/>
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="text" name="users_username" placeholder="Indtast Brugernavn" data-validation="length" data-validation-length="2-100" data-validation="alphanumeric" data-validation-allowing="-_"   data-validation-error-msg="Dit brugernavn må minimum værre 2 max 100 tegn"/>
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="text" name="users_password" placeholder="Indtast Password"  data-validation="length" data-validation-length="6-100"  data-validation-error-msg="Dit password skal minimum værre 6 max 100 tegn"/>
                    </div>
                    <div class="form-group">
                        <input class="btn btn-primary" type="submit" name="submit_bruger" value="Opret Bruger" />
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>    