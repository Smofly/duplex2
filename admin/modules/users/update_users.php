<?php
$update_users = new Users('users');
$update_users->setCondition_parameter($parameter = array(
    array('WHERE', 'users_id', '=', $_GET['id'])
));
$result_users = $update_users->select_user();
$row_users = $result_users->fetch_object();
?>
<?php
if (isset($_POST['update_bruger'])) {


    $validering = new Validering();

    // Check if Rank and Active is a number. if not make a error
    $validering->checknumeric($_POST['users_roles']);
    $validering->checknumeric($_POST['users_aktiv']);
    // check Firstname and lastname
    $validering->checkName($_POST['users_firstname']);
    $validering->checkName($_POST['users_lastname']);
    // Check adress and email
    $validering->checkAdresse($_POST['users_adress']);
    $validering->checkEmail($_POST['users_email']);
    // Check username and password
    $validering->checkName($_POST['users_username']);
    $validering->checkMinLength($_POST['users_username'], 2);

    if ($fejl = $validering->getFejl() == false) {

        $update_users->setRoles_roles_id($_POST['users_roles']);
        $update_users->setUsers_active($_POST['users_aktiv']);
        $update_users->setUsers_firstname($_POST['users_firstname']);
        $update_users->setUsers_lastname($_POST['users_lastname']);
        $update_users->setUsers_adress($_POST['users_adress']);
        $update_users->setUsers_email($_POST['users_email']);
        $update_users->setUsers_username($_POST['users_username']);
        $update_users->tableoption(2);
        $update_users->setCondition_field('users_id');
        $update_users->setCondition_operator('=');
        $update_users->setCondition_value($row_users->users_id);
        if (!$update_users->update_user() == false) {
            $update_users->redirect('?page=update_users&id=' . $row_users->users_id . '&update=succes');
        }
    } else {
        $update_users->redirect('?page=update_users&id=' . $row_users->users_id . '&vali=deny');
    }
}
?>
<div class="container">
    <div class="widget stacked">
        <div class="widget-header">
            <i class="icon-bookmark"></i>
            <h3>Opdater Bruger</h3>
        </div> <!-- /widget-header -->
        <div class="widget-content">
            <div class="col-md-12">
                <?php
                if (isset($_GET['update']) && $_GET['update'] == 'succes') {
                    echo '<div class="alert alert-success">Brugeren er nu Opdateret</div>';
                }
                if (isset($_GET['update']) && $_GET['update'] == 'deny') {
                    echo '<div class="alert alert-danger">Der findes allerede en Bruger med valgte Brugernavn/Email</div>';
                }
                if (isset($_GET['vali']) && $_GET['vali'] == 'deny') {
                    echo '<div class="alert alert-danger">En eller flere felter var ikke udfyldt korekt</div>';
                }
                ?>
                <form action="" method="POST">
                    <?php
                    if (in_array('Superadmin', $users_rank->getRolesRank())) {
                        $disable = '';
                    } else {
                        $disable = 'disabled';
                    }
                    $users_rank = new Users('roles');
                    $result_rank = $users_rank->select_user();
                    ?>
                    <div class="form-group">
                        <select class="form-control" name="users_roles" data-validation="required" data-validation-error-msg="Du skal vælge en bruger rank" <?php echo $disable ?> >
                            <option value="">Vælg Rank</option>
                            <?php
                            while ($row_rank = $result_rank->fetch_object()) {
                                if ($row_rank->roles_id == $row_users->roles_roles_id) {
                                    $selected = 'selected';
                                } else {
                                    $selected = '';
                                }
                                ?>
                                <option <?php echo $selected ?> value="<?php echo $row_rank->roles_id ?>"><?php echo $row_rank->roles_rank ?></option>
                                <?php
                            }
                            ?>

                        </select>

                    </div>
                    <div class="form-group">
                        <select class="form-control" name="users_aktiv" data-validation="required" data-validation-error-msg="Du skal vælge om brugeren profil er aktiveret">
                            <?php
                            if ($row_users->users_active == 1) {
                                ?>
                                <option selected value="1">Aktiveret</option>
                                <option value="0">Deaktivert</option>
                                <?php
                            } else {
                                ?>
                                <option value="1">Aktiveret</option>
                                <option selected value="0">Deaktivert</option>
                                <?php
                            }
                            ?>

                        </select>
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="text" name="users_firstname" value="<?php echo $row_users->users_firstname ?>" placeholder="Indtast navn" data-validation="alphanumeric" data-validation="length" data-validation-length="2-100" data-validation-error-msg="Navnet skal Indholde bogstaver fra a-z og minimum værre 2 max 100 tegn"  />
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="text" name="users_lastname" value="<?php echo $row_users->users_lastname ?>" placeholder="Indtast efternavn" data-validation="alphanumeric" data-validation="length" data-validation-length="2-100" data-validation-error-msg="Efternavnet skal Indholde bogstaver fra a-z og minimum værre 2 max 100 tegn" />
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="text" name="users_adress" value="<?php echo $row_users->users_adress ?>" placeholder="Indtast Adresse" data-validation="custom" data-validation-regexp="^((.){1,}(\d){1,}(.){0,})$" data-validation-error-msg="Adressen skal indholde et husnummer F.eks Adresse 14" />
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="text" name="users_email" value="<?php echo $row_users->users_email ?>"  placeholder="Indtast Email"  data-validation="email" data-validation-error-msg="Du har ikke angivet en gyldig email."/>
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="text" name="users_username" value="<?php echo $row_users->users_username ?>" placeholder="Indtast Brugernavn" data-validation="length" data-validation-length="2-100" data-validation="alphanumeric" data-validation-allowing="-_"   data-validation-error-msg="Dit brugernavn må minimum værre 2 max 100 tegn"/>
                    </div>
                    <div class="form-group">
                        <input class="btn btn-primary" type="submit" name="update_bruger" value="Opdatere Bruger" />
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
