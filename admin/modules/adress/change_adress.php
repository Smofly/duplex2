<?php
$info_update = new Adress('info');

// resultat fra select
$result_info_update = $info_update->select_info();
$info_update->setCondition_parameter($parameter = array(array('WHERE', 'info_id', '=', '1')));
$row_info_update = $result_info_update->fetch_object();
// nedenfor updaten

if (isset($_POST['submit'])) {
    // Lad os validere
    $validering = new Validering();
    // valideringschecks
    $validering->checkMinLength($_POST['info_adressline_one'], 5);
    $validering->checkMinLength($_POST['info_adressline_two'], 5);
    $validering->checkMinLength($_POST['info_phone_one'], 8);
    $validering->checkMinLength($_POST['info_phone_two'], 8);
    $validering->checkEmail($_POST['info_email']);
    $validering->checkMinLength($_POST['info_company'], 5);

    if ($fejl = $validering->getFejl() == false) {

        $info_update->setInfoAdresslineOne($_POST['info_adressline_one']);
        $info_update->setInfoAdresslineTwo($_POST['info_adressline_two']);
        $info_update->setInfoPhoneOne($_POST['info_phone_one']);
        $info_update->setInfoPhoneTwo($_POST['info_phone_two']);
        $info_update->setInfoEmail($_POST['info_email']);
        $info_update->setInfoCompany($_POST['info_company']);

        $info_update->setCondition_field('info_id');
        $info_update->setCondition_operator('=');
        $info_update->setCondition_value(1);

        $info_update->tableoption(1);
        if ($info_update->update_info() == true) {
            userRedirect('?page=admin_change_adress&update=succes');
        } else {
            userRedirect('?page=admin_change_adress&update=deny');
        }
    } else {
        userRedirect('?page=admin_change_adress&validate=deny');
    }
}
?>
<div class="container">
    <div class="widget stacked">
        <?php
        if (isset($_GET['update']) && $_GET['update'] == 'succes') {
            echo '<div class="alert alert-success text-center">Adressen er opdateret</div>';
        } else if (isset($_GET['update']) && $_GET['update'] == 'deny') {
            echo '<div class="alert alert-danger">Noget gik galt!</div>';
        } else if (isset($_GET['validate']) && $_GET['validate'] == 'deny') {
            echo '<div class="alert alert-danger">Du har udfyldt formularen forkert, prøv igen.</div>';
        }
        ?>
        <div class="widget-header">
            <i class="icon-bookmark"></i>
            <h3>Rediger adresse</h3>
        </div> <!-- /widget-header -->
        <div class="widget-content">
            <div class="col-md-12">
            <form class="form-horizontal" role="form" method="POST" action="">
                <div class="form-group">
                    <label for="info_adressline_one">Adressefelt</label>
                    <input class="form-control" data-validation="length" data-validation-length="min5" data-validation-error-msg="Der skal være mindst 5 cifre" name="info_adressline_one" value="<?php echo $row_info_update->info_adressline_one; ?>" type="Text" size="50" maxlength="50">
                </div>
                <div class="form-group">
                    <label for="info_adressline_two">Adressefelt 2</label>
                    <input class="form-control" data-validation="length" data-validation-length="min5" data-validation-error-msg="Der skal være mindst 5 cifre" name="info_adressline_two" value="<?php echo $row_info_update->info_adressline_two; ?>" type="Text" size="50" maxlength="50">
                </div>
                <div class="form-group">
                    <label for="info_phone_one">Telefon 1</label>
                    <input data-validation="length" data-validation-length="min8" data-validation-error-msg="Der skal være mindst 8 cifre" class="form-control" name="info_phone_one" value="<?php echo $row_info_update->info_phone_one; ?>" type="Text" size="50" maxlength="50">
                </div>
                <div class="form-group">
                    <label for="info_phone_two">Telefon 2</label>
                    <input data-validation="length" data-validation-length="min8" data-validation-error-msg="Der skal være mindst 8 cifre" class="form-control" name="info_phone_two" value="<?php echo $row_info_update->info_phone_two; ?>" type="Text" size="50" maxlength="50">
                </div>
                <div class="form-group">
                    <label for="info_email">Email</label>
                    <input data-validation="email" data-validation-error-msg="Du skal skrive en email" class="form-control" name="info_email" value="<?php echo $row_info_update->info_email; ?>" type="Text" size="50" maxlength="50">
                </div>
                <div class="form-group">
                    <label for="info_company">Virksomhed/CVR</label>
                    <input data-validation="length" data-validation-length="min5" data-validation-error-msg="Der skal være mindst 5 cifre" class="form-control" name="info_company" value="<?php echo $row_info_update->info_company; ?>" type="Text" size="50" maxlength="50">
                </div>
                <div class="btn-toolbar">
                    <input type="submit" name="submit" class="btn btn-primary" value="Opdater"/>
                </div>
            </form>
        </div>
            </div>
    </div>
</div>