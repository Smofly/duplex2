<div class="container">
    <div class="widget stacked">
        <div class="widget-header">
            <i class="icon-bookmark"></i>
            <h3>Alle nyheder</h3>
        </div> <!-- /widget-header -->
        <div class="widget-content">

            <?php
            if (isset($_GET['delete']) && $_GET['delete'] == 'succes') {
                echo '<div class="alert alert-success">Nyheden er slettet</div>';
            }
            if (isset($_GET['delete']) && $_GET['delete'] == 'deny') {
                echo '<div class="alert alert-danger">Noget gik galt!</div>';
            }
            ?>


            <div class="btn-toolbar">
                <a href="?page=admin_create_news"><button class="btn btn-primary">Nyt indlæg</button></a>
            </div>
            <br />
            <div class="well">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Overskrift</th>
                            <th>Udgivet</th>
                            <th style="width: 36px;"></th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                        $admin_listnews = new News('news');


                        $result_admin_listnews = $admin_listnews->select_news();
                        while ($row_admin_listnews = $result_admin_listnews->fetch_object()) {
                            $newsTime = new Datetime($row_admin_listnews->news_time);
                            ?>
                            <tr>
                                <td><?php echo $row_admin_listnews->news_id ?></td>
                                <td><?php echo $row_admin_listnews->news_headline ?></td>
                                <td><?php echo $newsTime->format('d-m-Y H:i') ?></td>
                                <td>
                                    <a href="?page=update_news&newsid=<?php echo $row_admin_listnews->news_id; ?>"><i class="icon-pencil"></i></a>
                                    <a class="confirm" href="?page=delete_news&newsid=<?php echo $row_admin_listnews->news_id; ?>" role="button" data-toggle="modal"><i class="icon-remove"></i></a>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div><!-- end container -->
</div>
