<div class="container">
    <div class="widget stacked">
        <div class="widget-header">
            <i class="icon-bookmark"></i>
            <h3>Opret en nyhed</h3>
        </div> <!-- /widget-header -->
        <div class="widget-content">
            <?php
            // instanciater class
            $news = new News('news');
            if (isset($_POST['submit'])) {
                $validering = new Validering();
                // valideringschecks
                $validering->checkMinLength($_POST['news_headline'], 5);
                if ($fejl = $validering->getFejl() == false) {

                    $news->setNews_headline($_POST['news_headline']);
                    $news->setNews_text($_POST['news_text']);
                    $news->setNews_update_time('2014-04-15 00:00:00');
                    $news->setNews_users_id($_SESSION['users_id']);
                    $news->setNews_category_cat_id(2);
                    // tableoption sender data til classen
                    $news->tableoption(1);
                    $fileupload = new upload('../images/news/');

                    foreach ($_FILES["files"]["size"] as $value) {
                        $filesize = $value;
                    }
                    $lastinsertid = $news->insert_news();
                    if ($filesize != 0) {
                        $pic = $fileupload->save($_FILES, $_POST);
                        foreach ($pic as $value) {
                            $images_upload = new News('news_images');

                            $fileupload->lavProportionalThumb($value, 150, 200);
                            $images_upload->setNews_images_name($value);
                            $images_upload->setImages_news_id($lastinsertid);
                            $images_upload->tableoption(3);
                            $succes = $images_upload->insert_news();
                        }
                    } else {
                        $succes = true;
                    }
                    if (!$succes == false) {
                        // Bruger script til at "refresh"
                        userRedirect("?page=admin_create_news&insert=succes");
                    } else {
                        // Bruger script til at "refresh"
                     userRedirect("?page=admin_create_news&insert=deny");
                    }
                } else {
                    // Bruger script til at "refresh"
                    userRedirect("?page=admin_create_news&insert=deny");
                }
            }
            ?>
            <div class="col-md-12">
                <?php
                if (isset($_GET['insert']) && $_GET['insert'] == 'succes') {
                    echo '<div class="alert alert-success">Indsat i databassen</div>';
                }
                if (isset($_GET['insert']) && $_GET['insert'] == 'deny') {
                    echo '<div class="alert alert-danger">noget gik galt, prøv igen!</div>';
                }
                ?>
                <form role="form" method="POST" action="" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="news_headline">Overskrift:</label>
                        <input data-validation="length" data-validation-length="min5" data-validation-error-msg="Der skal være mindst 5 cifre i din overskrift" class="form-control" name="news_headline" id="news_headline" type="Text">
                    </div>
                    <div class="form-group">
                        <label for="file">Billede:</label>
                    <input type="file"  id="file" name="files[]" multiple="multiple"  >
                    </div>
                    
                    <div class="news_text">
                        <label for="news_text">Dit indhold:</label>
                    <textarea id="news_text" name="news_text" cols="50" rows="5"></textarea><br />
                    </div>
                    <div class="btn-toolbar">
                        <input type="submit" name="submit" class="btn btn-primary" value="Udgiv"/>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>