<div class="container"><div class="row">
        <?php
        $newsid = $_GET['newsid'];

        $select_news_images = new News('news_images');

        $select_news_images->setCondition_parameter($parameter = array(
            array('WHERE', 'news_news_id', '=', $newsid)
        ));
        $result_news_images = $select_news_images->select_news();

        $count = mysqli_num_rows($result_news_images);

        if (!$count == 0) {
            foreach ($result_news_images as $news_images) {
                $fileupload = new upload('../images/news/');
                $fileupload->deleteImg('thumb_' . $news_images['news_images_name']);
            }
        }

        $delete_news = new News('news');

        $delete_news->setCondition_field('news_id');
        $delete_news->setCondition_operator('=');
        $delete_news->setCondition_value($newsid);

        if ($delete_news->delete_news() == true) {
            // Bruger script til at "refresh"
            userRedirect("?page=admin_news&delete=succes");
        } else {            
            // Bruger script til at "refresh"
           userRedirect("?page=admin_news&delete=deny");
        }
        ?>
    </div></div>
