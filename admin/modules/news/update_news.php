<div class="row">
    <div class="container">
        <div class="widget stacked">
            <div class="widget-header">
                <i class="icon-bookmark"></i>
                <h3>Redigere nyheden</h3>
            </div> <!-- /widget-header -->
            <div class="widget-content">
                <?php
                // getter id fra url
                $newsid = $_GET['newsid'];
                // instancier class
                $news_update = new News('news');
                // sætter select/where sætningen select * news where news_id=(get)newsid
                $news_update->setCondition_parameter($parameter = array(
                    array('WHERE', 'news_id', '=', $newsid)
                ));
                // resultat fra select
                $result_news_update = $news_update->select_news();
                $row_news_update = $result_news_update->fetch_object();
                // nedenfor updaten
                if (isset($_POST['submit'])) {
                    $validering = new Validering();
                    // valideringschecks
                    $validering->checkMinLength($_POST['news_headline'], 5);
                    if ($fejl = $validering->getFejl() == false) {

                        $newsupdatetime = date('Y-m-d H:i:s');
                        $news_update->setNews_headline($_POST['news_headline']);
                        $news_update->setNews_text($_POST['news_text']);
                        $news_update->setNews_update_time($newsupdatetime);
                        $news_update->setNews_users_id($row_news_update->news_users_id);
                        $news_update->setNews_category_cat_id(2);
                        $upload = new Upload('../images/news/');
                        $imagesDelete = new News('news_images');
                        if (isset($_POST['imagesId']) == '') {
                            
                        } else {
                            foreach ($_POST['imagesId'] as $value) {

                                $imagesDelete->setCondition_field('news_images_id');
                                $imagesDelete->setCondition_operator('=');
                                $imagesDelete->setCondition_value($value);

                                $imagesDelete->setCondition_parameter($parameter = array(
                                    array('WHERE', 'news_images_id', '=', $value)
                                ));
                                $result_deleteimages = $imagesDelete->select_news();
                                $row_deleteimages = $result_deleteimages->fetch_object();

                                $upload->deleteImg('thumb_' . $row_deleteimages->news_images_name);
                                $imagesDelete->delete_news();
                            }
                        }

                        foreach ($_FILES["files"]["size"] as $value) {
                            $filesize = $value;
                        }
                        if ($filesize != 0) {
                            $pic = $upload->save($_FILES, $_POST);
                            foreach ($pic as $images_name) {
                                $upload->lavProportionalThumb($images_name, 150, 200);

                                $imagesDelete->setNews_images_name($images_name);
                                $imagesDelete->setImages_news_id($_GET['newsid']);
                                $imagesDelete->tableoption(3);

                                $imagesDelete->insert_news();
                            }
                        }

                        $news_update->setCondition_field('news_id');
                        $news_update->setCondition_operator('=');
                        $news_update->setCondition_value($newsid);
                        $news_update->tableoption(1);
                        if ($news_update->update_news() == true) {
                            echo '<div class="alert alert-success">Datan er updatet</div>';
                            // Bruger script til at "refresh"
                            userRedirectOnTime("?page=admin_news", 3000);
                        } else {
                            echo '<div class="alert alert-danger">Noget gik galt!</div>';
                            // Bruger script til at "refresh"
                            userRedirectOnTime("?page=admin_news", 3000);
                        }
                    } else {
                        echo '<div class="alert alert-danger">Du har udfyldt formularen forkert, husk der skal være mindt 5 cifre i overskriften!</div>';
                        // Bruger script til at "refresh"
                        userRedirectOnTime("?page=admin_news", 3000);
                    }
                }
                ?>
                <div class="col-md-12">
                    <form class="form-horizontal" role="form" method="POST" action=""  enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="news_headline" class="col-md-2">Overskrift</label>
                            <div class="col-sm-12">
                                <input data-validation="length" data-validation-length="min5" data-validation-error-msg="Der skal være mindst 5 cifre i din overskrift" class="form-control" name="news_headline" value="<?php echo $row_news_update->news_headline; ?>" type="Text" size="50" maxlength="50">
                            </div>
                        </div>
                        <!-- billede skal gemmes i to størrelser 300x255 og 120x90 -->
                        <strong>Produkte Billede:</strong>
                        <input type="file"  id="file" name="files[]" multiple="multiple"  >
                        <br />
                        <br />
                        <textarea name="news_text" cols="50" rows="5"><?php echo $row_news_update->news_text; ?></textarea><br />

                        <?php
                        $news_images = new News('news_images');
                        $news_images->setCondition_parameter($parameter = array(
                            array('WHERE', 'images_news_id', '=', $_GET['newsid'])
                        ));
                        $result_images = $news_images->select_news();
                        while ($row_images = $result_images->fetch_object()) {
                            ?>
                            <input value="<?php echo $row_images->news_images_id ?>" type="checkbox" name="imagesId[]" />
                            <img src="../images/news/thumb_<?php echo $row_images->news_images_name; ?>" alt="<?php echo $row_images->news_images_name ?>" />
                            <?php
                        }
                        ?>
                        <br />
                        <br />
                        <div class="btn-toolbar">
                            <input type="submit" name="submit" class="btn btn-primary" value="Opdater"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<br />