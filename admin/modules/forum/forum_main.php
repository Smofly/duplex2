<?php
if (isset($_POST['submit_cat'])) {
    if (!$_POST['fcat_name'] == '') {
        if (isset($_POST['subcat']) && is_numeric($_POST['subcat'])) {
            $insert_kategori = new Forum_category('forum_subcat');
            $insert_kategori->setFsubcat_name($_POST['fcat_name']);
            $insert_kategori->setFsubcat_desc('Standart beskrivelse');
            $insert_kategori->setForum_category_fcat_id($_POST['subcat']);
            $insert_kategori->tableoption(2);
        } else {
            $insert_kategori = new Forum_category('forum_category');
            $insert_kategori->setFcat_name($_POST['fcat_name']);
            $insert_kategori->tableoption(1);
        }
        if (!$insert_kategori->insert_forum() == false) {
            $insert_kategori->redirect('?page=admin_view_forum_main&insert=succes');
        }
    } else {
        userRedirect('?page=admin_view_forum_main&insert=deny');
    }
}

if (isset($_GET['action']) && $_GET['action'] == 'edit_kategori') {

    if (isset($_GET['categori_id'])) {
        $insert_kategori = new Forum_category('forum_category');

        $insert_kategori->setCondition_parameter($parameter = array(
            array('WHERE', 'fcat_id', '=', $_GET['categori_id'])
        ));
        $result_category = $insert_kategori->select_forum();
        $row_category = $result_category->fetch_assoc();

        $cat_sub_name = $row_category['fcat_name'];
        $submit = 'submit_kategori';

        if (isset($_POST['submit_kategori'])) {
            $insert_kategori->setFcat_name($_POST['fcat_name']);
            $insert_kategori->setCondition_field('fcat_id');
            $insert_kategori->setCondition_operator('=');
            $insert_kategori->setCondition_value($_GET['categori_id']);
            $insert_kategori->tableoption(1);
            if ($insert_kategori->update_forum() == true) {
                $insert_kategori->redirect('?page=admin_view_forum_main&update=succes');
            }
        }
    } else if (isset($_GET['fsubcat_id'])) {
        $insert_kategori = new Forum_category('forum_subcat');

        $insert_kategori->setCondition_parameter($parameter = array(
            array('WHERE', 'fsubcat_id', '=', $_GET['fsubcat_id'])
        ));
        $result_category = $insert_kategori->select_forum();
        $row_category = $result_category->fetch_assoc();
        $cat_sub_name = $row_category['fsubcat_name'];
        $submit = 'submit_sub_kategori';

        if (isset($_POST['submit_sub_kategori'])) {
            $insert_kategori->setFsubcat_name($_POST['fcat_name']);
            $insert_kategori->setFsubcat_desc('Updatet desc');
            $insert_kategori->setForum_category_fcat_id($row_category['forum_category_fcat_id']);

            $insert_kategori->setCondition_field('fsubcat_id');
            $insert_kategori->setCondition_operator('=');
            $insert_kategori->setCondition_value($_GET['fsubcat_id']);
            $insert_kategori->tableoption(2);
            if ($insert_kategori->update_forum() == true) {
                $insert_kategori->redirect('?page=admin_view_forum_main&update=succes');
            }
        }
    } else {
        echo 'Du har gjort noget galt';
    }
}

if (isset($_GET['action']) && $_GET['action'] == 'delete_kategori') {
    if ($_GET['categori_id']) {

        $delete_cat = new Forum_category('forum_category');
        $delete_cat->setCondition_field('fcat_id');
        $delete_cat->setCondition_operator('=');
        $delete_cat->setCondition_value($_GET['categori_id']);
        if ($delete_cat->delete_forun() == true) {
            $delete_cat->redirect('?page=admin_view_forum_main&delete=succes');
        }
    } else if ($_GET['fsubcat_id']) {
        $delete_cat = new Forum_category('forum_subcat');
        $delete_cat->setCondition_field('fsubcat_id');
        $delete_cat->setCondition_operator('=');
        $delete_cat->setCondition_value($_GET['fsubcat_id']);
        if ($delete_cat->delete_forun() == true) {
            $delete_cat->redirect('?page=admin_view_forum_main&delete=succes');
        }
    } else {
        echo 'Du har gjort noget galt';
    }
}
?>


<div class="container">
    <div class="widget stacked">
        <div class="widget-header">
            <i class="icon-bookmark"></i>
            <h3>Kategorier i forum</h3>
        </div> <!-- /widget-header -->
        <div class="widget-content">
            <?php
            if (isset($_GET['update']) && $_GET['update'] == 'succes') {
                echo '<div class="alert alert-success">Kategorien er opdateret</div>';
            }
            if (isset($_GET['delete']) && $_GET['delete'] == 'succes') {
                echo '<div class="alert alert-success">Kategorien er Slettet</div>';
            }
            if (isset($_GET['insert']) && $_GET['insert'] == 'succes') {
                echo '<div class="alert alert-success">Kategorien er Oprettet</div>';
            }
            if (isset($_GET['insert']) && $_GET['insert'] == 'deny') {
                echo '<div class="alert alert-danger">Der skete en fejl ved oprettelse af kategori!</div>';
            }
            ?>            
            <form class="form-horizontal" role="form" method="POST" action="">
                <div class="col-md-6">
                    <div class="form-group">
                        <h2>Ny kategori</h2>
                        <hr/>
                        <label for="fcat_name">Navn på kategori (Husk at vælge hovedkategori, hvis du ønsker en underkategori):</label>
                        <input class="form-control" name="fcat_name"
                        <?php
                        if (isset($_GET['action']) && $_GET['action'] == 'edit_kategori') {
                            echo 'value="' . $cat_sub_name . '"';
                        }
                        ?>
                               type="text" size="10" maxlength="30">
                    </div>
                    <?php
                    if (isset($_GET['action']) && $_GET['action'] == 'edit_kategori') {
                        ?>
                        <div class="form-group">
                            <input class="btn btn-default" type="submit" name="<?php echo $submit ?>" value="Redigere Kategori"/>
                        </div>
                    <?php } else {
                        ?>
                        <div class="form-group">
                            <input class="btn btn-default" type="submit" name="submit_cat" value="Opret Kategori"/>
                        </div>
                    <?php }
                    ?>
                </div> 
                <div class="col-md-1"></div>
                <div class="col-md-5">
                    <div class="form-group"> 
                        <h2>vælg kategori</h2>
                        <hr/>
                        <div class="alert alert-info" role="alert">
                        <?php
                        $forum_catgory = new Forum_category('forum_category');
                        $result_forum_category = $forum_catgory->select_forum();

                        $forum_subcat = new Forum_category('forum_subcat');
                        foreach ($result_forum_category as $forum_category) {
                            ?>
                            <div class="checkbox">
                                <label>
                                    <h4><input name="subcat" value="<?php echo $forum_category['fcat_id'] ?>" type="radio" <?php
                                        if (isset($_GET['action']) && $_GET['action'] == 'edit_kategori') {
                                            echo 'disabled';
                                        }
                                        ?> > <?php echo $forum_category['fcat_name'] ?> <br><a href="?page=admin_view_forum_main&action=edit_kategori&categori_id=<?php echo $forum_category['fcat_id'] ?>">Rediger</a>/<a href="?page=admin_view_forum_main&action=delete_kategori&categori_id=<?php echo $forum_category['fcat_id'] ?>">Slet</a></h4> 
                                </label>
                            </div>
                            <?php
                            $forum_subcat->setCondition_parameter($parameter = array(array('WHERE', 'forum_category_fcat_id', '=', $forum_category['fcat_id'])));
                            $result_forum_subcat = $forum_subcat->select_forum();

                            foreach ($result_forum_subcat AS $fsubcats) {
                                ?>
                                
                                <ul>
                                    <li><?php echo $fsubcats['fsubcat_name'] ?> <a href="?page=admin_view_forum_main&action=edit_kategori&fsubcat_id=<?php echo $fsubcats['fsubcat_id'] ?>">Rediger</a>/<a href="?page=admin_view_forum_main&action=delete_kategori&fsubcat_id=<?php echo $fsubcats['fsubcat_id'] ?>">Slet</a></li>    
                                </ul>
                                  
                                <?php
                            }
                            echo '<hr/>';
                        }
                        ?>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div><!-- end container -->