<div class="container">
        <?php
        $catid = $_GET['gallery_category_id'];

        $select_gallery = new Gallery('gallery_has_img_and_cat');
        $remove_img = new Upload('../images/gallery/');
        $select_gallery->setCondition_parameter($parameter = array(
            array('WHERE', 'gallery_category_id', '=', $catid)
        ));
        $result_gallery = $select_gallery->select_gallery();

        while ($row_gallery = $result_gallery->fetch_assoc()) {

            $imgname[] = $row_gallery['gallery_images_name'];

            foreach ($imgname as $key => $value) {
                $remove_img->deleteImg('thumb_' . $value);
            }
        }

        $delete_forum = new Gallery('gallery_category');

        $delete_forum->setCondition_field('gallery_category_id');
        $delete_forum->setCondition_operator('=');
        $delete_forum->setCondition_value($catid);

        if ($delete_forum->delete_gallery() == true) {
            echo '<div class="alert alert-success">Kategorien er slettet</div>';
            // Bruger script til at "refresh"
            userRedirectOnTime("?page=admin_list_gallery", 3000);
        } else {
            echo '<div class="alert alert-danger">Noget gik galt, har du slettet alle indlæg i denne kategori først?</div>';
            // Bruger script til at "refresh"
            userRedirectOnTime("?page=admin_list_gallery", 3000);
        }
        ?>
</div>