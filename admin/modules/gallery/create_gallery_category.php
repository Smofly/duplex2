<?php
$view_category = new Gallery('gallery_category');

$result_view_category = $view_category->select_gallery();
$row_view_category = $result_view_category->fetch_object();

// update/set
if (isset($_POST['submit_gallery_category'])) {
    $view_category->setGallery_category_name($_POST['gallery_category_name']);
    $view_category->setGallery_images_desc($_POST['gallery_images_desc']);
    $view_category->tableoption(1);
    if ($view_category->insert_gallery() == true) {
        echo '<div class="container"><div class="alert alert-success text-center">Adressen er opdateret</div></div>';
        // Bruger script til at "refresh"
        userRedirectOnTime("?page=admin_list_gallery", 3000);
    } else {
        echo '<div class="alert alert-danger">Noget gik galt!</div>';
    }
}
?>
<div class="container">
    <div class="widget stacked">
        <div class="widget-header">
            <i class="icon-bookmark"></i>
            <h3>Opret en kategori til dit galleri</h3>
        </div> <!-- /widget-header -->
        <div class="widget-content">
            <div class="col-md-12">
                <form action="" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="gallery_category_name">Navn:</label>
                        <input class="form-control" type="text" name="gallery_category_name" placeholder="Indtast navn" />
                    </div>
                    <div class="form-group">
                    <label for="gallery_images_desc">Beskrivelse:</label>
                    <textarea  type="text" name="gallery_images_desc"></textarea>
                    </div>
                    <input class="btn btn-primary" type="submit" name="submit_gallery_category" value="Gem kategori" />
                </form>
            </div>
        </div>
    </div>
</div>
<br />
