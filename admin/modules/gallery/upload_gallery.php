<?php
if (isset($_POST['submit_gallery'])) {
    $images_save = new Gallery('gallery_images');
    $fileupload = new Upload('../images/gallery/');
    $pic = $fileupload->save($_FILES, $_POST);
    print_r($pic);
    foreach ($pic as $value) {
        $fileupload->lavProportionalThumb($value, '237.5', '260');

        $images_save->setGallery_images_name($value);
        $images_save->setGallery_images_desc($_POST['gallery_desc']);
        $images_save->setGallery_category_gallery_category_id($_POST['category']);

        $images_save->tableoption(2);
        $images_save->insert_gallery();
    }
}
?>
<div class="container">
    <div class="widget stacked">
        <div class="widget-header">
            <i class="icon-bookmark"></i>
            <h3>Opret et galleri</h3>
        </div> <!-- /widget-header -->
        <div class="widget-content">
            <div class="col-md-12">
                <form action="" method="POST" enctype="multipart/form-data">

                    <div class="form-group">
                        <select class="form-control" name="category">
                            <option value="">Vælg Kategori</option>
                            <?php
                            $dropdown_kategori = new Gallery('gallery_category');
                            $result_kategori = $dropdown_kategori->select_gallery();
                            while ($row_kategori = $result_kategori->fetch_object()) {
                                ?>
                                <option value="<?php echo $row_kategori->gallery_category_id ?>"><?php echo $row_kategori->gallery_category_name ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <strong>Billede:</strong>
                    <input type="file" id="file" name="files[]" multiple />
                    <br />
                    <br />
                    <strong>Beskrivelse</strong> <textarea  type="text" name="gallery_desc"></textarea>
                    <br /><br />
                    <input class="btn btn-primary" type="submit" name="submit_gallery" value="Gem galleriet" />
                </form>
            </div>
        </div>
    </div>
</div>
<br />
