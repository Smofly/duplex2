<div class="container">
    <div class="widget stacked">
        <div class="widget-header">
            <i class="icon-bookmark"></i>
            <h3>Galleri kategori list</h3>
        </div> <!-- /widget-header -->
        <div class="widget-content">
            <div class="btn-toolbar">
                <a href="?page=admin_upload_gallery"><button class="btn btn-primary">Opret et galleri</button></a>
            </div>
            <br />
            <div class="well">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Kategori</th>
                            <th>Beskrivelse</th>
                            <th>Slet</th>
                            <th style="width: 36px;"></th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                        $admin_listgalcat = new Gallery('gallery_category');
                        $result_admin_listgalcat = $admin_listgalcat->select_gallery();
                        while ($row_admin_listgalcat = $result_admin_listgalcat->fetch_object()) {
                            ?>
                            <tr>
                                <td><?php echo $row_admin_listgalcat->gallery_category_name ?></td>
                                <td><?php echo $row_admin_listgalcat->gallery_category_desc ?></td>
                                <td>
                                    <a class="confirm" href="?page=admin_delete_galcat&gallery_category_id=<?php echo $row_admin_listgalcat->gallery_category_id; ?>" role="button" data-toggle="modal"><i class="icon-remove"></i></a>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div><!-- end container -->

