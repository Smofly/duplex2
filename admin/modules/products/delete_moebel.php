<div class="container">
    <?php
    $productid = $_GET['products_id'];

    $select_products = new Products('products_has_cat_and_img');
    $delete_img = new Products_images('products_image');

    $select_products->setCondition_parameter($parameter = array(
        array('WHERE', 'products_id', '=', $productid)
    ));
    $result_products = $select_products->select_products();

    $count = mysqli_num_rows($result_products);

    if (!$count == 0) {
        foreach ($result_products as $products) {
            $fileupload = new upload('../images/' . $products['products_cat_id'] . '/');
            $fileupload->deleteImg('thumb_' . $products['products_image_name']);
        }
    }
    $delete_products = new Products('products');
    $delete_products->setCondition_field('products_id');
    $delete_products->setCondition_operator('=');
    $delete_products->setCondition_value($productid);

    if ($delete_products->delete_products() == true) {
        echo '<div class="alert alert-success">Productet  er slettet</div>';
        // Bruger script til at "refresh"
        userRedirectOnTime("?page=admin_list_moebel", 3000);
    } else {
        echo '<div class="alert alert-danger">Noget gik galt!</div>';
        // Bruger script til at "refresh"
        userRedirectOnTime("?page=admin_list_moebel", 3000);
    }
    ?>
</div>
