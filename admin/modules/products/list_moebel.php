<div class="container">
        <div class="widget stacked">
            <div class="widget-header">
                <i class="icon-bookmark"></i>
                <h3>Opret et produkt</h3>
            </div> <!-- /widget-header -->
            <div class="widget-content">
                <div class="btn-toolbar">
                    <a href="?page=admin_create_moebel"><button class="btn btn-primary">Nyt produkt</button></a>
                </div>
                <br />
                <div class="well">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Kategori/type</th>
                                <th>varenummer</th>
                                <th>designer</th>
                                <th style="width: 36px;"></th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php
                            $admin_listmoebler = new Products('products');
                            $result_admin_listmoebler = $admin_listmoebler->select_products();
                            while ($row_admin_listmoebler = $result_admin_listmoebler->fetch_object()) {
                                ?>
                                <tr>
                                    <td><?php echo $row_admin_listmoebler->products_name ?></td>
                                    <td><?php echo $row_admin_listmoebler->products_nr ?></td>
                                    <td><?php echo $row_admin_listmoebler->products_design ?></td>
                                    <td>
                                        <a href="?page=update_products&products_id=<?php echo $row_admin_listmoebler->products_id; ?>"><i class="icon-pencil"></i></a>
                                        <a class="confirm" href="?page=delete_moebel&products_id=<?php echo $row_admin_listmoebler->products_id; ?>" role="button" data-toggle="modal"><i class="icon-remove"></i></a>
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
</div><!--end row -->
</div><!-- end container -->
