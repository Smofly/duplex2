<div class="container">
    <div class="widget stacked">
        <div class="widget-header">
            <i class="icon-bookmark"></i>
            <h3>Update et produkt</h3>
        </div> <!-- /widget-header -->
        <div class="widget-content">
            <?php
            $update_products = new Products('products_has_cat_and_img');
            // updatere de rigtig table i databassen ud fra id
            $update_products->setCondition_field('products_id');
            $update_products->setCondition_operator('=');
            $update_products->setCondition_value($_GET['products_id']);
            // bruges til at selecte de rigtig data i databassen til at vise i formen
            $update_products->setCondition_parameter($parameter = array(
                array('WHERE', 'products_id', '=', $_GET['products_id'])
            ));
            $result_products = $update_products->select_products();
            $row_products = $result_products->fetch_object();

            $upload = new Upload('../images/' . $row_products->products_cat_id . '/');

            if (isset($_POST['update_products'])) {
                $update_products->setProductsName($_POST['products_name']);
                $update_products->setProductsNr($_POST['products_nr']);
                $update_products->setProductsDesign($_POST['designer']);
                $update_products->setProductsYear($_POST['productsyear']);
                $update_products->setProductsPrice($_POST['price']);
                $update_products->setProductsDesc($_POST['desc']);
                $update_products->setProducts_max($_POST['products_max']);
                $update_products->setProductsProductsCategoryProductsCatId($row_products->products_category_products_cat_id);

                $imagesDelete = new Products_images('products_image');
                if (isset($_POST['imagesId']) == '') {
                    
                } else {
                    foreach ($_POST['imagesId'] as $value) {
                        $imagesDelete->setCondition_parameter($parameter = array(
                            array('WHERE', 'products_image_id', '=', $value)
                        ));
                        $result_deleteimages = $imagesDelete->select_products_images();
                        $row_deleteimages = $result_deleteimages->fetch_object();

                        $imagesDelete->setCondition_field('products_image_id');
                        $imagesDelete->setCondition_operator('=');
                        $imagesDelete->setCondition_value($value);

                        $upload->deleteImg('thumb_' . $row_deleteimages->products_image_name);
                        $imagesDelete->delete_products_images();
                    }
                }

                foreach ($_FILES["files"]["size"] as $value) {
                    $filesize = $value;
                }
                if ($filesize != 0) {
                    $pic = $upload->save($_FILES, $_POST);
                    foreach ($pic as $images_name) {
                        $upload->lavProportionalThumb($images_name, 150, 200);

                        $imagesDelete->setProductsImageName($images_name);
                        $imagesDelete->setProductsProductsId($_GET['products_id']);
                        $imagesDelete->tableoption(1);
                        $imagesDelete->insert_products_images();
                    }
                }

                $update_products->tableoption(1);
                if ($update_products->update_products() == true) {
                    userRedirect('?page=update_products&products_id=' . $_GET['products_id'] . '&updatet=succes');
                }
            }

// Udskriver Succes besked når produktet er opdateret
            if (isset($_GET['updatet']) && $_GET['updatet'] == 'succes') {
                echo '<div class="alert alert-success">Produktet er blevet opdateret</div>';
            }
            if (isset($_GET['delete']) && $_GET['delete'] == 'succes') {
                echo '<div class="alert alert-success">Produktet billederne er blevet slettet</div>';
            }
            ?>
            <div class="col-md-12">
                <form action="" method="POST" enctype="multipart/form-data">

                    <div class="form-group">
                        <label for="products_category">produkte Kategori</label>
                        <select class="form-control" name="category">
                            <option value="">Vælg Kategori</option>
                            <?php
                            $dropdown_kategori = new Products_category('products_category');
                            $result_kategori = $dropdown_kategori->select_products_category();
                            while ($row_kategori = $result_kategori->fetch_object()) {
                                if ($row_products->products_cat_id == $row_kategori->products_cat_id) {
                                    $selected = 'selected';
                                } else {
                                    $selected = '';
                                }
                                ?>
                                <option <?php echo $selected ?> value="<?php echo $row_kategori->products_cat_id ?>"><?php echo $row_kategori->products_cat_name ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="products_name">Produkte Navn</label>
                        <input class="form-control" id="products_name" type="text" name="products_name" placeholder="Indtast navn" value="<?php echo $row_products->products_name; ?>" />
                    </div>

                    <div class="form-group">
                        <label for="products_nr">Produkte Nummer</label>
                        <input class="form-control" id="products_nr" type="text" name="products_nr" placeholder="Indtast varenummer" value="<?php echo $row_products->products_nr; ?>" />
                    </div>


                    <div class="form-group">
                        <label for="products_designer">Produkte Designer</label>
                        <input class="form-control" id="products_designer" type="text" name="designer" placeholder="Designer" value="<?php echo $row_products->products_design; ?>" />
                    </div>

                    <div class="form-group">
                        <label for="products_year">Produkte År</label>
                        <input class="form-control" id="products_year" type="text" name="productsyear" placeholder="År" value="<?php echo $row_products->products_year; ?>" />
                    </div>

                    <div class="form-group">
                        <label for="products_price">Produkte Pris</label>
                        <input class="form-control" id="products_price" type="text" name="price" placeholder="Pris" value="<?php echo $row_products->products_price; ?>" />
                    </div>
                    <div class="form-group">
                        <label for="products_max">Produkte max</label>
                        <input class="form-control" id="products_max" type="text" name="products_max" placeholder="Max" value="<?php echo $row_products->products_max; ?>" />
                    </div>
                    <!-- billede skal gemmes i to størrelser 300x255 og 120x90 -->
                    <strong>Produkte Billede:</strong>
                    <input type="file"  id="file" name="files[]" multiple="multiple"  >
                    <br />
                    <br />
                    <strong>Produkte Beskrivelse</strong> <textarea  type="text" name="desc"><?php echo $row_products->products_desc; ?></textarea>

                    <?php
                    $produckt_images = new Products_images('products_image');
                    $produckt_images->setCondition_field('products_products_id');
                    $produckt_images->setCondition_operator('=');
                    $produckt_images->setCondition_value($_GET['products_id']);
                    $result_images = $produckt_images->select_products_images();
                    while ($row_images = $result_images->fetch_object()) {
                        ?>
                        <input value="<?php echo $row_images->products_image_id ?>" type="checkbox" name="imagesId[]" />
                        <img src="../images/<?php echo $row_products->products_cat_id . '/thumb_' . $row_images->products_image_name ?>" alt="" />
                        <?php
                    }
                    ?>

                    <br /><br />
                    <input class="btn btn-primary" type="submit" name="update_products" value="Update produkt" />
                </form>
            </div>
        </div>
    </div>
</div>
