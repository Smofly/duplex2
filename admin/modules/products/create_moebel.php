<div class="container">
    <div class="widget stacked">
        <div class="widget-header">
            <i class="icon-bookmark"></i>
            <h3>Opret et produkt</h3>
        </div> <!-- /widget-header -->
        <div class="widget-content">
            <?php
            // instanciater class
            $products = new Products('products');
            if (isset($_POST['submit_moebel'])) {
                if (!$_POST['category'] == '') {
                    $products->setProductsName($_POST['products_name']);
                    $products->setProductsNr($_POST['products_nr']);
                    $products->setProductsProductsCategoryProductsCatId($_POST['category']);
                    $products->setProductsDesign($_POST['designer']);
                    $products->setProductsYear($_POST['productsyear']);
                    $products->setProductsPrice($_POST['price']);
                    $products->setProductsDesc($_POST['desc']);
                    $products->setProducts_max($_POST['max_vare']);
                    // tableoption sender data til classen
                    $products->tableoption(1);
                    //$productsimage->tableoption();


                    $fileupload = new upload('../images/' . $_POST['category'] . '/');

                    $pic = $fileupload->save($_FILES, $_POST);
                    $lastinsertid = $products->insert_products();

                    foreach ($pic as $value) {
                        $images_upload = new Products_images('products_image');

                        $fileupload->lavProportionalThumb($value, 150, 200);
                        $images_upload->setProductsImageName($value);
                        $images_upload->setProductsProductsId($lastinsertid);
                        $images_upload->tableoption(1);
                        $succes = $images_upload->insert_products_images();
                    }
                } else {
                    
                }
                if (!$succes == false) {
                    echo '<div class="alert alert-success">Indsat i databassen</div>';
                    // Bruger script til at "refresh"
                    //userRedirectOnTime("?page=admin_list_moebel", 3000);
                } else {
                    echo '<div class="alert alert-danger">noget gik galt!</div>';
                    // Bruger script til at "refresh"
                    //userRedirectOnTime("?page=admin_list_moebel", 3000);
                }
            }
            ?>
            <div class="col-md-12">
                <form action="" method="POST" enctype="multipart/form-data">

                    <div class="form-group">
                        <select class="form-control" name="category">
                            <option value="">Vælg Kategori</option>
                            <?php
                            $dropdown_kategori = new Products_category('products_category');
                            $result_kategori = $dropdown_kategori->select_products_category();
                            while ($row_kategori = $result_kategori->fetch_object()) {
                                ?>
                                <option value="<?php echo $row_kategori->products_cat_id ?>"><?php echo $row_kategori->products_cat_name ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <input class="form-control" type="text" name="products_name" placeholder="Indtast navn" />
                    </div>

                    <div class="form-group">
                        <input class="form-control" type="text" name="products_nr" placeholder="Indtast varenummer" />
                    </div>


                    <div class="form-group">
                        <input class="form-control" type="text" name="designer" placeholder="Designer" />
                    </div>

                    <div class="form-group">
                        <input class="form-control" type="text" name="productsyear" placeholder="År" />
                    </div>

                    <div class="form-group">
                        <input class="form-control" type="text" name="price" placeholder="Pris" />
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="text" name="max_vare" placeholder="max antal" />
                    </div>
                    <!-- billede skal gemmes i to størrelser 300x255 og 120x90 -->
                    <strong>Billede:</strong>
                    <input type="file"  id="file" name="files[]" multiple="multiple"  >
                    <br />
                    <br />
                    <strong>Beskrivelse</strong> <textarea  type="text" name="desc"></textarea>
                    <br /><br />
                    <input class="btn btn-primary" type="submit" name="submit_moebel" value="Gem produkt" />
                </form>
            </div>
        </div>
    </div>
</div>