<div class="row">

    <div class="col-md-4">

        <div class="widget stacked widget-table">

            <div class="widget-header">
                <span class="icon-list-alt"></span>
                <h3>Top Referrers</h3>
            </div> <!-- .widget-header -->

            <div class="widget-content">
                <table class="table table-bordered table-striped">

                    <thead><tr>								
                            <th>Referrer</th>
                            <th>Uniques</th>								
                        </tr></thead>

                    <tbody><tr>
                            <td class="description"><a href="http://google.com">http://google.com</a></td>
                            <td class="value"><span>1123</span></td>
                        </tr>
                        <tr>
                            <td class="description"><a href="http://yahoo.com">http://yahoo.com</a></td>
                            <td class="value"><span>927</span></td>
                        </tr>
                        <tr>
                            <td class="description"><a href="http://themeforest.net">http://themeforest.net</a></td>
                            <td class="value"><span>834</span></td>
                        </tr>
                        <tr>
                            <td class="description"><a href="http://codecanyon.net">codecanyon.net</a></td>
                            <td class="value"><span>625</span></td>
                        </tr>
                        <tr>
                            <td class="description"><a href="http://graphicriver.net">http://graphicriver.net</a></td>
                            <td class="value"><span>593</span></td>
                        </tr>

                        <tr>
                            <td class="description"><a href="http://bing.com">http://bing.com</a></td>
                            <td class="value"><span>324</span></td>
                        </tr>


                    </tbody></table>

            </div> <!-- .widget-content -->

        </div> <!-- /widget -->	

    </div> <!-- /span4 -->



    <div class="col-md-4">

        <div class="widget stacked widget-table">

            <div class="widget-header">
                <span class="icon-file"></span>
                <h3>Most Visited Pages</h3>
            </div> <!-- .widget-header -->

            <div class="widget-content">
                <table class="table table-bordered table-striped">

                    <thead><tr>								
                            <th>Page</th>
                            <th>Visits</th>								
                        </tr></thead>

                    <tbody><tr>
                            <td class="description"><a href="javascript:;">Homepage</a></td>
                            <td class="value"><span>1123</span></td>
                        </tr>
                        <tr>
                            <td class="description"><a href="javascript:;">Portfolio</a></td>
                            <td class="value"><span>927</span></td>
                        </tr>
                        <tr>
                            <td class="description"><a href="javascript:;">Services</a></td>
                            <td class="value"><span>834</span></td>
                        </tr>
                        <tr>
                            <td class="description"><a href="javascript:;">Contact Us</a></td>
                            <td class="value"><span>625</span></td>
                        </tr>
                        <tr>
                            <td class="description"><a href="javascript:;">Testimonials</a></td>
                            <td class="value"><span>593</span></td>
                        </tr>

                        <tr>
                            <td class="description"><a href="javascript:;">Signup</a></td>
                            <td class="value"><span>456</span></td>
                        </tr>


                    </tbody></table>

            </div> <!-- .widget-content -->

        </div>

    </div> <!-- /span4 -->



    <div class="col-md-4">

        <div class="widget stacked widget-table">

            <div class="widget-header">
                <span class="icon-external-link"></span>
                <h3>Browsers</h3>
            </div> <!-- .widget-header -->

            <div class="widget-content">
                <table class="table table-bordered table-striped">

                    <thead><tr>								
                            <th>Browser</th>
                            <th>Visits</th>								
                        </tr></thead>

                    <tbody><tr>
                            <td class="description">Firefox</td>
                            <td class="value"><span>1123</span></td>
                        </tr>
                        <tr>
                            <td class="description">Chrome</td>
                            <td class="value"><span>927</span></td>
                        </tr>
                        <tr>
                            <td class="description">Internet Explorer</td>
                            <td class="value"><span>834</span></td>
                        </tr>
                        <tr>
                            <td class="description">Safari</td>
                            <td class="value"><span>625</span></td>
                        </tr>
                        <tr>
                            <td class="description">Opera</td>
                            <td class="value"><span>593</span></td>
                        </tr>

                        <tr>
                            <td class="description">Netscape</td>
                            <td class="value"><span>123</span></td>
                        </tr>


                    </tbody></table>

            </div> <!-- .widget-content -->

        </div>

    </div> <!-- /span4 -->

</div> <!-- /row -->