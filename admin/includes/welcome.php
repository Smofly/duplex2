<div class="col-md-6">

    <div class="widget stacked">

        <div class="widget-header">
            <i class="icon-bookmark"></i>
            <h3>Velkommen til duplex</h3>
        </div> <!-- /widget-header -->

        <div class="widget-content">
            <h2>Du er admin på duplex</h2>
            <p>
                Her kan du administrere:
            </p>
            <ul>
                <li>Nyheder</li>
                <li>Gallerier</li>
                <li>Produkter</li>
                <li>Forum</li>
                <li>Bruger</li>
            </ul>
            <p>Og meget mere..</p>
            <p>Du er velkommen til at læse vores <a href="#">vejledning</a> eller <a href="#">kontakte</a> os for spørgsmål</p>
        </div>
    </div>
</div>