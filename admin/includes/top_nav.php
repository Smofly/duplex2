                <nav class="navbar navbar-inverse" role="navigation">

                    <div class="container">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <i class="icon-cog"></i>
                            </button>
                            <a class="navbar-brand" href="./index.php">Administrator</a>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse navbar-ex1-collapse">
                            <ul class="nav navbar-nav navbar-right">

                                <li><a href="../index.php">Til frontend</a></li>

                                <li class="dropdown">

                                    <a href="javscript:;" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="icon-user"></i>
                                        Brugernavn
                                        <b class="caret"></b>
                                    </a>

                                    <ul class="dropdown-menu">
                                        <li><a href="javascript:;">Min Profil</a></li>
                                        <li><a href="javascript:;">Tomt menupunkt</a></li>
                                        <li class="divider"></li>
                                        <form action="" method="post"><button style="margin-left:16px; margin-bottom:5px;" class="btn btn-danger btn-xs" type="submit" name="logout"> Log ud</button></form>
                                    </ul>

                                </li>
                            </ul>

                            <form class="navbar-form navbar-right" role="search">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm search-query" placeholder="Search">
                                </div>
                            </form>
                        </div><!-- /.navbar-collapse -->
                    </div> <!-- /.container -->
                </nav>