<div class="subnavbar">

                    <div class="subnavbar-inner">

                        <div class="container">

                            <a href="javascript:;" class="subnav-toggle" data-toggle="collapse" data-target=".subnav-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <i class="icon-reorder"></i>

                            </a>

                            <div class="collapse subnav-collapse">
                                <ul class="mainnav">

                                    <li class="active">
                                        <a href="./index.php">
                                            <i class="icon-home"></i>
                                            <span>Hjem</span>
                                        </a>	    				
                                    </li>

                                    <li class="dropdown">					
                                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                                            <i class="icon-user"></i>
                                            <span>Brugere</span>
                                            <b class="caret"></b>
                                        </a>	    

                                        <ul class="dropdown-menu">
                                            <li><a href="?page=create_users">Opret Bruger</a></li>
                                            <li><a href="?page=admin_users">Brugerliste</a></li>
                                        </ul> 				
                                    </li>
                                    
                                    <li class="dropdown">					
                                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                                            <i class="icon-th"></i>
                                            <span>Nyheder</span>
                                            <b class="caret"></b>
                                        </a>	    

                                        <ul class="dropdown-menu">
                                            <li><a href="?page=admin_create_news">Opret nyhed</a></li>
                                            <li><a href="?page=admin_news">Nyhedsliste</a></li>
                                        </ul> 				
                                    </li>

                                    <li class="dropdown">
                                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                                            <i class="icon-copy"></i>
                                            <span>Produkter</span>
                                            <b class="caret"></b>
                                        </a>

                                        <ul class="dropdown-menu">                                       
                                            <li><a href="?page=admin_create_moebel">Opret produkt</a></li>  
                                            <li><a href="?page=admin_list_moebel">Produktliste</a></li>
                                        </ul>
                                    </li>

                                    <li class="dropdown">					
                                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                                            <i class="icon-copy"></i>
                                            <span>Gallerier</span>
                                            <b class="caret"></b>
                                        </a>	    

                                        <ul class="dropdown-menu">
                                            <li><a href="?page=admin_list_gallery">Galleri Kategorier</a></li>
                                            <li><a href="?page=admin_create_galcat">Opret kategori</a></li>
                                            <li><a href="?page=admin_upload_gallery">Opret galleri</a></li>
                                        </ul> 				
                                    </li>
                                    <li class="dropdown">					
                                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                                            <i class="icon-external-link"></i>
                                            <span>Forum</span>
                                            <b class="caret"></b>
                                        </a>	

                                        <ul class="dropdown-menu">
                                            <li><a href="?page=admin_view_forum_main">Kategorier</a></li>
                                        </ul>    				
                                    </li>

                                    <li class="dropdown">					
                                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                                            <i class="icon-external-link"></i>
                                            <span>Andet</span>
                                            <b class="caret"></b>
                                        </a>	

                                        <ul class="dropdown-menu">
                                            <li><a href="?page=admin_change_adress">Ændre adresse</a></li>
                                        </ul>    				
                                    </li>

                                </ul>
                            </div> <!-- /.subnav-collapse -->

                        </div> <!-- /container -->

                    </div> <!-- /subnavbar-inner -->

                </div> <!-- /subnavbar -->