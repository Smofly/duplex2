<div class="extra">

    <div class="container">

        <div class="row">

            <div class="col-md-3">

                <h4>Socialt</h4>

                <ul>
                    <li><a href="http://hiweb.dk/om-os/">Om os</a></li>
                    <li><a href="https://www.facebook.com/pages/Hiwebdk/320471654654874">Facebook</a></li>
                </ul>

            </div> <!-- /span3 -->

            <div class="col-md-3">

                <h4>Support</h4>

                <ul>
                    <li><a href="http://hiweb.dk/kontakt/">Kontakt os med spørgsmål</a></li>
                </ul>

            </div> <!-- /span3 -->

            <div class="col-md-3">

                <h4>Rettigheder</h4>

                <ul>
                    <li><a href="https://www.gnu.org/copyleft/gpl.html">Licens</a></li>
                </ul>

            </div> <!-- /span3 -->

            <div class="col-md-3">

                <h4>Andet</h4>

                <ul>
                    <li><a href="#">Tomt</a></li>
                </ul>

            </div> <!-- /span3 -->

        </div> <!-- /row -->

    </div> <!-- /container -->

</div> <!-- /extra -->
