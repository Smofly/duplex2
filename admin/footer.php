<?php 
//include 'includes/footer_extra.php'; ?>
<div class="footer">
    <div class="container">
        <div class="row">
            <div id="footer-copyright" class="col-md-6">
                &copy; 2014 <a href="http://Smofly.dk">Dan Iversen SDE</a>
            </div> <!-- /span6 -->
            <div id="footer-terms" class="col-md-6">
                I Samarbejde med <a href="http://HiWEB.dk" target="_blank">Hiweb</a>
            </div> <!-- /.span6 -->
        </div> <!-- /row -->
    </div> <!-- /container -->
</div> <!-- /footer -->